package net.ihe.gazelle.communicationgateway.connector;

import net.ihe.gazelle.communicationgateway.connector.dto.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Set;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class CommunicationToolConnectorTest {

    @Mock
    CommunicationToolConnectorVerifier communicationToolConnectorVerifier;

    @InjectMocks
    CommunicationToolConnectorStub communicationToolConnectorStub;

    @Test
    void joinTeam() {
        Team team = mock(Team.class);
        Set<Member> members = getMembers();
        Set<Member> realMembers = getRealMembers();

        doNothing().when(communicationToolConnectorVerifier).doJoinTeam(team, realMembers);
        communicationToolConnectorStub.joinTeam(team, members);

        verifyNoMoreInteractions(communicationToolConnectorVerifier);
    }

    @Test
    void setTeamMembers() {
        Team team = mock(Team.class);
        Set<Member> members = getMembers();
        Set<Member> realMembers = getRealMembers();

        doNothing().when(communicationToolConnectorVerifier).doSetTeamMembers(team, realMembers);
        communicationToolConnectorStub.setTeamMembers(team, members);

        verifyNoMoreInteractions(communicationToolConnectorVerifier);
    }

    @Test
    void joinChannel() {
        Channel channel = mock(Channel.class);
        Set<Member> members = getMembers();
        Set<Member> realMembers = getRealMembers();

        doNothing().when(communicationToolConnectorVerifier).doJoinChannel(channel, realMembers);
        communicationToolConnectorStub.joinChannel(channel, members);

        verifyNoMoreInteractions(communicationToolConnectorVerifier);
    }

    @Test
    void setChannelMembers() {
        Channel channel = mock(Channel.class);
        Set<Member> members = getMembers();
        Set<Member> realMembers = getRealMembers();

        doNothing().when(communicationToolConnectorVerifier).doSetChannelMembers(channel, realMembers);
        communicationToolConnectorStub.setChannelMembers(channel, members);

        verifyNoMoreInteractions(communicationToolConnectorVerifier);
    }

    private Set<Member> getMembers() {
        return Set.of(
                new Member(new User("a", "b", "c", "d"), Role.USER),
                new Member(new User("a", "b", "c", "d"), Role.ADMIN),
                new Member(new User("e", "f", "g", "h"), Role.USER)
        );
    }

    private Set<Member> getRealMembers() {
        return Set.of(
                new Member(new User("a", "b", "c", "d"), Role.ADMIN),
                new Member(new User("e", "f", "g", "h"), Role.USER)
        );
    }

}
