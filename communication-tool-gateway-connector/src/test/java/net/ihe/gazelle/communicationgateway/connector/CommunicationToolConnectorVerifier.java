package net.ihe.gazelle.communicationgateway.connector;

import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;

import java.util.Set;

public interface CommunicationToolConnectorVerifier {
    void doJoinTeam(Team team, Set<Member> members);

    void doSetTeamMembers(Team team, Set<Member> members);

    void doJoinChannel(Channel channel, Set<Member> members);

    void doSetChannelMembers(Channel channel, Set<Member> members);
}
