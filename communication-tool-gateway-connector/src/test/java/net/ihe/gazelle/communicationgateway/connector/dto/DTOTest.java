package net.ihe.gazelle.communicationgateway.connector.dto;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class DTOTest {

    @Test
    void channel() {
        EqualsVerifier.simple()
                .forClass(Channel.class)
                .withIgnoredFields("team", "name", "topic", "privacyType")
                .verify();

        assertNotEquals(new Channel(), new Object());

        assertEqualsAndHashCode(new Channel(), new Channel(), true);
        assertEqualsAndHashCode(
                new Channel("a", new Team(), "b", "c", PrivacyType.PUBLIC),
                new Channel("a", null, null, null, null),
                false);
    }

    @Test
    void team() {
        EqualsVerifier.simple()
                .forClass(Team.class)
                .withIgnoredFields("name", "topic", "privacyType")
                .verify();

        assertNotEquals(new Team(), new Object());

        assertEqualsAndHashCode(new Team(), new Team(), true);
        assertEqualsAndHashCode(
                new Team("a", "b", "c", PrivacyType.PUBLIC),
                new Team("a", null, null, null),
                false
        );
    }

    @Test
    void user() {
        EqualsVerifier.simple()
                .forClass(User.class)
                .withIgnoredFields("firstname", "lastname", "email")
                .verify();

        assertNotEquals(new User(), new Object());

        assertEqualsAndHashCode(new User(), new User(), true);
        assertEqualsAndHashCode(
                new User("a", "b", "c", "d"),
                new User("a", null, null, null),
                false
        );
    }

    @Test
    void member() {
        EqualsVerifier.simple()
                .forClass(Member.class)
                .verify();

        assertNotEquals(new Member(), new Object());

        assertEqualsAndHashCode(new Member(), new Member(), true);
        assertEqualsAndHashCode(
                new Member(new User("a", "b", "c", "d"), Role.ADMIN),
                new Member(new User("a", "b", "c", "d"), Role.ADMIN),
                true
        );
    }

    private void assertEqualsAndHashCode(Object a, Object b, boolean toStringEquals) {
        assertEquals(a, b);
        assertEquals(a.hashCode(), b.hashCode());
        if (toStringEquals) {
            assertEquals(a.toString(), b.toString());
        } else {
            assertNotEquals(a.toString(), b.toString());
        }
    }

}
