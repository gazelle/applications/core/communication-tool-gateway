package net.ihe.gazelle.communicationgateway.connector;

import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.connector.dto.User;

import java.util.Set;

public class CommunicationToolConnectorStub implements CommunicationToolConnector {
    CommunicationToolConnectorVerifier communicationToolConnectorVerifier;

    @Override
    public String getTarget() {
        return null;
    }

    @Override
    public void healthCheck() {

    }

    @Override
    public void doJoinTeam(Team team, Set<Member> members) {
        communicationToolConnectorVerifier.doJoinTeam(team, members);
    }

    @Override
    public void doSetTeamMembers(Team team, Set<Member> members) {
        communicationToolConnectorVerifier.doSetTeamMembers(team, members);
    }

    @Override
    public void setTeamAdmins(Team team, Set<User> admins) {
    }

    @Override
    public Set<Channel> getChannels(Team team) {
        return null;
    }

    @Override
    public void checkChannels(Set<Channel> channels) {

    }

    @Override
    public void checkUsers(Set<User> users) {

    }

    @Override
    public void doJoinChannel(Channel channel, Set<Member> members) {
        communicationToolConnectorVerifier.doJoinChannel(channel, members);

    }

    @Override
    public void doSetChannelMembers(Channel channel, Set<Member> members) {
        communicationToolConnectorVerifier.doSetChannelMembers(channel, members);
    }

    @Override
    public void setChannelAdmins(Channel channel, Set<User> admins) {

    }

    @Override
    public void leaveChannel(Channel channel, Set<User> users) {

    }

    @Override
    public String getInvitationLink(Channel channel) {
        return null;
    }

    @Override
    public void archiveChannel(Channel channel) {

    }

    @Override
    public void unarchiveChannel(Channel channel) {

    }

    @Override
    public void sendMessage(Channel channel, String message) {

    }
}
