package net.ihe.gazelle.communicationgateway.connector.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Channel {
    String id;
    @EqualsAndHashCode.Exclude
    Team team;
    @EqualsAndHashCode.Exclude
    String name;
    @EqualsAndHashCode.Exclude
    String topic;
    @EqualsAndHashCode.Exclude
    PrivacyType privacyType;
}