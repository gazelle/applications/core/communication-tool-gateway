package net.ihe.gazelle.communicationgateway.connector.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Team {
    String id;
    @EqualsAndHashCode.Exclude
    String name;
    @EqualsAndHashCode.Exclude
    String topic;
    @EqualsAndHashCode.Exclude
    PrivacyType privacyType;
}
