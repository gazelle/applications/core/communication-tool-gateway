package net.ihe.gazelle.communicationgateway.connector.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class User {
    String username;
    @EqualsAndHashCode.Exclude
    String firstname;
    @EqualsAndHashCode.Exclude
    String lastname;
    @EqualsAndHashCode.Exclude
    String email;
}
