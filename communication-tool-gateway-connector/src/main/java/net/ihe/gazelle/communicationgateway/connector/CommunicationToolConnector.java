package net.ihe.gazelle.communicationgateway.connector;

import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.connector.dto.User;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

public interface CommunicationToolConnector {

    Comparator<Member> MEMBER_ROLE_COMPARATOR = Comparator.comparing(Member::getRole);

    String getTarget();

    void healthCheck();

    default void joinTeam(Team team, Set<Member> members) {
        doJoinTeam(team, mergeMembers(members));
    }

    void doJoinTeam(Team team, Set<Member> members);

    default void setTeamMembers(Team team, Set<Member> members) {
        doSetTeamMembers(team, mergeMembers(members));
    }

    void doSetTeamMembers(Team team, Set<Member> members);

    void setTeamAdmins(Team team, Set<User> admins);

    Set<Channel> getChannels(Team team);

    void checkChannels(Set<Channel> channels);

    void checkUsers(Set<User> users);

    default void joinChannel(Channel channel, Set<Member> members) {
        doJoinChannel(channel, mergeMembers(members));
    }

    void doJoinChannel(Channel channel, Set<Member> members);

    default void setChannelMembers(Channel channel, Set<Member> members) {
        doSetChannelMembers(channel, mergeMembers(members));
    }

    void doSetChannelMembers(Channel channel, Set<Member> members);

    void setChannelAdmins(Channel channel, Set<User> admins);

    void leaveChannel(Channel channel, Set<User> users);

    String getInvitationLink(Channel channel);

    void archiveChannel(Channel channel);

    void unarchiveChannel(Channel channel);

    void sendMessage(Channel channel, String message);

    default Set<Member> mergeMembers(Collection<Member> members) {
        // if same user is added to channel/team with different roles
        return members.stream()
                .collect(Collectors.groupingBy(Member::getUser))
                .values().stream()
                .map(memberList -> {
                    if (memberList.size() > 1) {
                        // use first role in enum Role
                        memberList.sort(MEMBER_ROLE_COMPARATOR);
                    }
                    return memberList.get(0);
                })
                .collect(Collectors.toSet());
    }

}
