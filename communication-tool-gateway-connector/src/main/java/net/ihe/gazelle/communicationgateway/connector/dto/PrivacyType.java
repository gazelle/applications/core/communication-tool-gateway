package net.ihe.gazelle.communicationgateway.connector.dto;

public enum PrivacyType {
    PUBLIC,

    PRIVATE;
}
