package net.ihe.gazelle.communicationgateway.connector.dto;

public enum Role {
    // by order of priority
    ADMIN,
    USER
}
