package net.ihe.gazelle.communicationgateway.rocketchat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatException;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.utility.DockerImageName;

@Slf4j
public class RocketChatContainer {

    private final ObjectMapper objectMapper = new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private GenericContainer<?> rocketChatContainer;

    @SneakyThrows
    public void start() {
        Network network = Network.newNetwork();

        MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:4.0"))
                .withNetwork(network)
                .withNetworkAliases("mongo")
                .withCommand("mongod --smallfiles --oplogSize 128 --replSet rs0 --storageEngine=mmapv1")
                .withClasspathResourceMapping("/users.json", "/tmp/users.json", BindMode.READ_ONLY);

        mongoDBContainer.start();
        mongoDBContainer.execInContainer("mongo", "--eval", "\"printjson(rs.initiate())\"");
        mongoDBContainer.execInContainer(
                "mongoimport", "--uri=\"mongodb://localhost:27017/rocketchat?retryWrites=false\"",
                "--collection=\"users\"", "--mode=upsert", "/tmp/users.json"
        );

        rocketChatContainer = new GenericContainer<>(DockerImageName.parse("rocket.chat:3.18.5"))
                .withNetwork(network)
                .withExposedPorts(3000)
                .withEnv("MONGO_URL", "mongodb://mongo:27017/rocketchat")
                .withEnv("MONGO_OPLOG_URL", "mongodb://mongo:27017/local")
                .waitingFor(Wait.forHttp("/"));
        rocketChatContainer.start();

        log.info("started");
    }

    public String getRocketChatUrl() {
        return "http://127.0.0.1:" + rocketChatContainer.getMappedPort(3000);
    }

    @SneakyThrows
    public RocketChatApiClient getRocketChatApi() {
        String rocketChatUrl = getRocketChatUrl();
        return new RocketChatApiClient(rocketChatUrl, rocketChatUrl, "gazelle", "gazelle");
    }

    protected String toJson(Object body) {
        try {
            return objectMapper.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            log.error("Failed to serialize {}", body);
            throw new RocketChatException("Failed to serialize body", e);
        }
    }

}
