package net.ihe.gazelle.communicationgateway.rocketchat.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupInfoResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class GroupByNameInfoRequest implements Request<GroupInfoResult> {

    String roomName;

    @Override
    public Class<GroupInfoResult> getResultClass() {
        return GroupInfoResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

    @Override
    public String getSuffix() {
        return "groups.info";
    }
}
