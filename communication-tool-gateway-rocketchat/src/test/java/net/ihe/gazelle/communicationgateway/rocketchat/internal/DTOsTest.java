package net.ihe.gazelle.communicationgateway.rocketchat.internal;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

class DTOsTest {

    @Test
    void beans() {
        EqualsVerifier.simple()
                .forPackage("net.ihe.gazelle.communicationgateway.rocketchat.internal", true)
                .verify();
    }
}
