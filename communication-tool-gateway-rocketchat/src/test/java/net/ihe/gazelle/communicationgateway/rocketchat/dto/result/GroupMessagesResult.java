package net.ihe.gazelle.communicationgateway.rocketchat.dto.result;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class GroupMessagesResult extends Result {
    List<GroupMessage> messages;
}
