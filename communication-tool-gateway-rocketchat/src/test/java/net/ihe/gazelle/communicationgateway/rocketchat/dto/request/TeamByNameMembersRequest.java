package net.ihe.gazelle.communicationgateway.rocketchat.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.result.TeamMembersResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class TeamByNameMembersRequest implements Request<TeamMembersResult> {

    String teamName;

    String username;

    Integer count;

    @Override
    public Class<TeamMembersResult> getResultClass() {
        return TeamMembersResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

    @Override
    public String getSuffix() {
        return "teams.members";
    }
}
