package net.ihe.gazelle.communicationgateway.rocketchat;

import net.ihe.gazelle.communicationgateway.connector.dto.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatException;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.chat.request.PostMessageRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.Room;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.RcTeam;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcMember;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;
import net.ihe.gazelle.communicationgateway.rocketchat.internal.ChannelRcTeam;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class RocketChatConnectorTest {

    @Mock
    RocketChatApiClient rocketChatApiClient;

    @Mock
    RoomService roomService;

    @Mock
    UserService userService;

    @Mock
    TeamService teamService;

    @InjectMocks
    RocketChatConnector rocketChatConnector;

    static int i = 0;

    @Test
    void getTarget() {
        assertEquals("rocketchat", rocketChatConnector.getTarget());
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void healthCheck() {
        doNothing().when(rocketChatApiClient).healthCheck();
        rocketChatConnector.healthCheck();
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void checkUsers() {
        User u1 = mock(User.class);
        User u2 = mock(User.class);
        User u3 = mock(User.class);
        Set<User> users = Set.of(u1, u2, u3);
        when(userService.getUsers(users)).thenReturn(Set.of());
        rocketChatConnector.checkUsers(users);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void joinTeam() {
        User u1 = getUser();
        User u2 = getUser();
        User u3 = getUser();
        Member m1 = new Member(u1, Role.USER);
        Member m2 = new Member(u2, Role.USER);
        Member m3 = new Member(u3, Role.USER);
        Team t = mock(Team.class);

        Set<User> users = Set.of(u1, u2, u3);
        Set<RcUser> rcUsers = Set.of(asRcUser(u1), asRcUser(u2), asRcUser(u3));
        Set<RcMember> rcMembers = rcUsers.stream().map(rcUser -> new RcMember(rcUser, Set.of(RoomRole.MEMBER))).collect(Collectors.toSet());
        when(userService.getUsers(users)).thenReturn(rcUsers);
        RcTeam rcTeam = mock(RcTeam.class);
        when(teamService.getTeam(t)).thenReturn(rcTeam);
        doNothing().when(teamService).joinTeam(rcTeam, rcMembers);
        rocketChatConnector.joinTeam(t, Set.of(m1, m2, m3));
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void setTeamMembers() {
        User u1 = getUser();
        User u2 = getUser();
        User u3 = getUser();
        Member m1 = new Member(u1, Role.USER);
        Member m2 = new Member(u2, Role.USER);
        Member m3 = new Member(u3, Role.USER);
        Team t = mock(Team.class);

        Set<User> users = Set.of(u1, u2, u3);
        Set<RcUser> rcUsers = Set.of(asRcUser(u1), asRcUser(u2), asRcUser(u3));
        Set<RcMember> rcMembers = rcUsers.stream().map(rcUser -> new RcMember(rcUser, Set.of(RoomRole.MEMBER))).collect(Collectors.toSet());
        when(userService.getUsers(users)).thenReturn(rcUsers);
        RcTeam rcTeam = mock(RcTeam.class);
        when(teamService.getTeam(t)).thenReturn(rcTeam);
        doNothing().when(teamService).setTeamMembers(rcTeam, rcMembers);
        rocketChatConnector.setTeamMembers(t, Set.of(m1, m2, m3));
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void setTeamAdmins() {
        User u1 = getUser();
        User u2 = getUser();
        User u3 = getUser();
        Member m1 = new Member(u1, Role.USER);
        Member m2 = new Member(u2, Role.USER);
        Member m3 = new Member(u3, Role.USER);
        Team t = mock(Team.class);

        Set<User> users = Set.of(u1, u2, u3);
        Set<RcUser> rcUsers = Set.of(asRcUser(u1), asRcUser(u2), asRcUser(u3));
        when(userService.getUsers(users)).thenReturn(rcUsers);
        RcTeam rcTeam = mock(RcTeam.class);
        when(teamService.getTeam(t)).thenReturn(rcTeam);
        doNothing().when(teamService).setTeamAdmins(rcTeam, rcUsers);
        rocketChatConnector.setTeamAdmins(t, Set.of(u1, u2, u3));
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void getChannels() {
        Team team = mock(Team.class);
        RcTeam rcTeam = mock(RcTeam.class);
        when(teamService.getTeam(team)).thenReturn(rcTeam);

        Channel c1 = mock(Channel.class);
        Channel c2 = mock(Channel.class);
        Channel c3 = mock(Channel.class);
        Set<Channel> channels = Set.of(c1, c2, c3);
        when(roomService.getRooms(team, rcTeam)).thenReturn(channels);
        assertEquals(channels, rocketChatConnector.getChannels(team));

        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void checkChannels() {

        Channel c1 = mock(Channel.class);
        Channel c2 = mock(Channel.class);
        Channel c3 = mock(Channel.class);
        Set<Channel> channels = Set.of(c1, c2, c3);
        doNothing().when(roomService).checkRooms(any(), anyBoolean());
        rocketChatConnector.checkChannels(channels);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void joinChannel() {
        User u1 = getUser();
        User u2 = getUser();
        User u3 = getUser();
        Member m1 = new Member(u1, Role.USER);
        Member m2 = new Member(u2, Role.ADMIN);
        Member m3 = new Member(u3, Role.USER);
        Set<User> users = Set.of(u1, u2, u3);
        Set<Member> members = Set.of(m1, m2, m3);

        Channel c = mock(Channel.class);

        Room room = mock(Room.class);
        when(roomService.getRoom(any())).thenReturn(room);

        Set<RcMember> rcMembers = members.stream()
                .map(this::getMember)
                .collect(Collectors.toSet());

        Set<RcUser> rcUsers = rcMembers.stream().map(RcMember::getUser).collect(Collectors.toSet());

        when(userService.getUsers(users)).thenReturn(rcUsers);

        doNothing().when(roomService).joinRoom(room, rcMembers);

        rocketChatConnector.joinChannel(c, members);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void joinChannel2() {
        User u1 = getUser();
        User u2 = getUser();
        User u3 = getUser();
        Member m1 = new Member(u1, Role.USER);
        Member m2 = new Member(u2, Role.ADMIN);
        Member m3 = new Member(u3, Role.USER);
        Set<User> users = Set.of(u1, u2, u3);
        Set<Member> members = Set.of(m1, m2, m3);

        Channel c = mock(Channel.class);

        Room room = mock(Room.class);
        when(roomService.getRoom(any())).thenReturn(room);

        Set<Member> members2 = Set.of(m1, m2);
        Set<RcMember> rcMembers = members2.stream()
                .map(this::getMember)
                .collect(Collectors.toSet());

        Set<RcUser> rcUsers = rcMembers.stream().map(RcMember::getUser).collect(Collectors.toSet());

        when(userService.getUsers(users)).thenReturn(rcUsers);

        doNothing().when(roomService).joinRoom(room, rcMembers);

        rocketChatConnector.joinChannel(c, members);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void setChannelMembers() {
        User u1 = getUser();
        User u2 = getUser();
        User u3 = getUser();
        Member m1 = new Member(u1, Role.USER);
        Member m2 = new Member(u2, Role.ADMIN);
        Member m3 = new Member(u3, Role.USER);
        Set<User> users = Set.of(u1, u2, u3);
        Set<Member> members = Set.of(m1, m2, m3);

        Channel c = mock(Channel.class);

        Room room = mock(Room.class);
        when(roomService.getRoom(any())).thenReturn(room);

        Set<RcMember> rcMembers = members.stream()
                .map(this::getMember)
                .collect(Collectors.toSet());

        Set<RcUser> rcUsers = rcMembers.stream().map(RcMember::getUser).collect(Collectors.toSet());

        when(userService.getUsers(users)).thenReturn(rcUsers);

        doNothing().when(roomService).setRoomMembers(room, rcMembers);

        rocketChatConnector.setChannelMembers(c, members);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void setChannelAdmins() {
        User u1 = getUser();
        User u2 = getUser();
        User u3 = getUser();
        Member m1 = new Member(u1, Role.USER);
        Member m2 = new Member(u2, Role.ADMIN);
        Member m3 = new Member(u3, Role.USER);
        Set<User> users = Set.of(u1, u2, u3);
        Set<Member> members = Set.of(m1, m2, m3);

        Channel c = mock(Channel.class);

        Room room = mock(Room.class);
        when(roomService.getRoom(any())).thenReturn(room);

        Set<RcMember> rcMembers = members.stream()
                .map(this::getMember)
                .collect(Collectors.toSet());

        Set<RcUser> rcUsers = rcMembers.stream().map(RcMember::getUser).collect(Collectors.toSet());

        when(userService.getUsers(users)).thenReturn(rcUsers);

        doNothing().when(roomService).setRoomAdmins(room, rcUsers);

        rocketChatConnector.setChannelAdmins(c, users);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void leaveChannel() {
        User u1 = getUser();
        User u2 = getUser();
        User u3 = getUser();
        Member m1 = new Member(u1, Role.USER);
        Member m2 = new Member(u2, Role.ADMIN);
        Member m3 = new Member(u3, Role.USER);
        Set<User> users = Set.of(u1, u2, u3);
        Set<Member> members = Set.of(m1, m2, m3);

        Channel c = mock(Channel.class);

        Room room = mock(Room.class);
        when(roomService.getRoom(any())).thenReturn(room);

        Set<RcMember> rcMembers = members.stream()
                .map(this::getMember)
                .collect(Collectors.toSet());

        Set<RcUser> rcUsers = rcMembers.stream().map(RcMember::getUser).collect(Collectors.toSet());

        when(userService.getUsers(users)).thenReturn(rcUsers);

        doNothing().when(roomService).leaveRoom(room, rcUsers);

        rocketChatConnector.leaveChannel(c, users);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    private User getUser() {
        String u = "user" + (i++);
        return new User(u, u, u, u);
    }

    private RcMember getMember(Member m) {
        RoomRole role;
        if (m.getRole() == Role.USER) {
            role = RoomRole.MEMBER;
        } else {
            role = RoomRole.MODERATOR;
        }
        User user = m.getUser();
        RcUser rcUser = asRcUser(user);
        return new RcMember(rcUser, Set.of(role));
    }

    @NotNull
    private RcUser asRcUser(User user) {
        RcUser rcUser = new RcUser();
        rcUser.setUsername(user.getUsername());
        rcUser.setId(user.getUsername());
        return rcUser;
    }

    @Test
    void getInvitationLink() {

        Channel c = mock(Channel.class);
        when(roomService.getInvitationLink(any())).thenReturn("link");
        String invitationLink = rocketChatConnector.getInvitationLink(c);
        assertEquals("link", invitationLink);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void archiveChannel() {

        Channel c = mock(Channel.class);
        doNothing().when(roomService).archive(any());
        rocketChatConnector.archiveChannel(c);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void archiveChannel2() {

        Channel c = mock(Channel.class);
        Team t = mock(Team.class);
        when(c.getTeam()).thenReturn(t);
        RcTeam rcTeam = mock(RcTeam.class);
        when(teamService.getTeam(t)).thenReturn(rcTeam);
        doNothing().when(roomService).archive(any());
        rocketChatConnector.archiveChannel(c);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void unarchiveChannel() {

        Channel c = mock(Channel.class);
        doNothing().when(roomService).unarchive(any(ChannelRcTeam.class));
        rocketChatConnector.unarchiveChannel(c);
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void sendMessage() {

        Channel c = mock(Channel.class);

        Room room = mock(Room.class);
        when(roomService.getRoom(any())).thenReturn(room);

        when(rocketChatApiClient.execute(any(PostMessageRequest.class))).thenReturn(null);

        rocketChatConnector.sendMessage(c, "message");
        verifyNoMoreInteractions(rocketChatApiClient, roomService, userService, teamService);
    }

    @Test
    void asRoomRole() {
        assertEquals(RoomRole.MEMBER, rocketChatConnector.asRoomRole(Role.USER));
        assertEquals(RoomRole.MODERATOR, rocketChatConnector.asRoomRole(Role.ADMIN));
        assertThrows(RocketChatException.class, () -> rocketChatConnector.asRoomRole(null));
    }

}