package net.ihe.gazelle.communicationgateway.rocketchat.client.dto;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.ChangeRoomTopicRequest;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.reflections.scanners.Scanners.SubTypes;

@Slf4j
class DTOsTest {

    @Test
    void beans() {
        EqualsVerifier.simple()
                .forPackage("net.ihe.gazelle.communicationgateway.rocketchat.client.dto", true)
                .verify();
    }

    @SneakyThrows
    @Test
    void requests() {
        Reflections reflections = new Reflections("net.ihe.gazelle.communicationgateway.rocketchat");

        Set<Class<?>> requestClasses = reflections.get(
                SubTypes
                        .of(Request.class)
                        .asClass()
                        .filter(input -> input != null && (input.getModifiers() & Modifier.ABSTRACT) == 0)
                        .filter(input -> input != null && (input.getModifiers() & Modifier.INTERFACE) == 0)
        );

        for (Class<?> requestClass : requestClasses) {
            Request<?> request = (Request<?>) requestClass.getDeclaredConstructor().newInstance();
            assertNotNull(request.getMethod());
            Class<?> resultClass = request.getResultClass();
            assertNotNull(resultClass);
            assertTrue(verifyResultType(requestClass, requestClass, resultClass));
            assertNotNull(request.getSuffix());
        }
    }

    private boolean verifyResultType(Class<?> baseRequestClass, Class<?> requestClass, Class<?> resultClass) {
        if (requestClass.equals(Object.class)) {
            return false;
        }
        Type[] genericInterfaces = requestClass.getGenericInterfaces();
        for (Type genericInterface : genericInterfaces) {
            if (genericInterface instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) genericInterface;
                if (parameterizedType.getRawType().equals(Request.class)) {
                    Type actualTypeArgument = parameterizedType.getActualTypeArguments()[0];
                    boolean result = actualTypeArgument.equals(resultClass);
                    if (!result) {
                        log.error("Wrong getResultClass in " + baseRequestClass);
                    }
                    return result;
                }
            }
        }
        return verifyResultType(baseRequestClass, requestClass.getSuperclass(), resultClass);
    }

    @Test
    void changeRoomTopicRequest() {
        ChangeRoomTopicRequest changeRoomTopicRequest = new ChangeRoomTopicRequest("rid", null);
        assertEquals("", changeRoomTopicRequest.getRoomTopic());
        changeRoomTopicRequest = new ChangeRoomTopicRequest("rid", "toto");
        assertEquals("toto", changeRoomTopicRequest.getRoomTopic());
    }
}
