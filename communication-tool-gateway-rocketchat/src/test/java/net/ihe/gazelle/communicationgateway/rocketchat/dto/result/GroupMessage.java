package net.ihe.gazelle.communicationgateway.rocketchat.dto.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GroupMessage {

    @JsonProperty("_id")
    String id;

    String msg;
}
