package net.ihe.gazelle.communicationgateway.rocketchat.client;

import lombok.Data;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;

@Data
public class RecursivePostRequest implements Request<Result> {
    RecursivePostRequest o;

    @Override
    public Class<Result> getResultClass() {
        return Result.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

    @Override
    public String getSuffix() {
        return "tata";
    }
}
