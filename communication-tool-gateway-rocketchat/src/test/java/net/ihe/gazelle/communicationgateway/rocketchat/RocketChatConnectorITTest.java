package net.ihe.gazelle.communicationgateway.rocketchat;

import lombok.SneakyThrows;
import net.ihe.gazelle.communicationgateway.connector.dto.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.ChannelCreateRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.ChannelMembersRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelCreateResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupCreateRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupInviteRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupKickRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupMembersRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupCreateResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.Room;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomInfoRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.MembersResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.RoomInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.RcTeam;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.result.TeamMembersResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcMember;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request.UserCreateRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request.UserInfoRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.result.UserInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.dto.request.*;
import net.ihe.gazelle.communicationgateway.rocketchat.dto.result.GroupMessage;
import net.ihe.gazelle.communicationgateway.rocketchat.dto.result.GroupMessagesResult;
import net.ihe.gazelle.communicationgateway.rocketchat.internal.ChannelRcTeam;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@Disabled
public class RocketChatConnectorITTest {

    // connector implementation
    private static RocketChatConnector rocketChatConnector;

    static int i = 0;
    private static RocketChatApiClient rocketChatApiClient;
    private static TeamService teamService;
    private static UserService userService;
    private static RoomService roomService;

    @BeforeAll
    public static void setUp() {
        RocketChatContainer rocketChatContainer = new RocketChatContainer();
        rocketChatContainer.start();

        rocketChatApiClient = rocketChatContainer.getRocketChatApi();

        String rocketChatUrl = rocketChatContainer.getRocketChatUrl();
        roomService = new RoomService(rocketChatUrl, rocketChatApiClient);

        teamService = new TeamService(rocketChatApiClient, roomService);
        userService = new UserService(rocketChatApiClient);

        rocketChatConnector = new RocketChatConnector(
                rocketChatApiClient,
                roomService,
                userService,
                teamService
        );
    }

    @Test
    void getTarget() {
        assertEquals("rocketchat", rocketChatConnector.getTarget());
    }

    @Test
    void healthCheck() {
        assertDoesNotThrow(() -> rocketChatConnector.healthCheck());
    }

    @Test
    void getInvitationLink() {

        Team team = getTeam();
        Channel channel = getChannel(team, "get_target", PrivacyType.PRIVATE);
        String invitationLink = rocketChatConnector.getInvitationLink(channel);
        assertNotNull(invitationLink);
        assertTrue(invitationLink.startsWith("http://127.0.0.1:"));
    }

    @Test
    void checkUser1() {

        User user = getNewUser();
        rocketChatConnector.checkUsers(Set.of(user));
        RcUser rcUser = getUser(user.getUsername());
        assertEquals(user.getUsername(), rcUser.getUsername());
    }

    @Test
    void checkUser2() {

        User user = getNewUser();
        User user2 = getNewUser();
        createUser(user2);
        User user3 = getNewUser();
        User user4 = getNewUser();
        createUser(user4);
        rocketChatConnector.checkUsers(Set.of(user, user2, user3, user4));
        RcUser rcUser = getUser(user.getUsername());
        assertEquals(user.getUsername(), rcUser.getUsername());
    }

    @Test
    void getTeamTest() {
        Team team = getTeam();
        RcTeam rcTeam = teamService.getTeam(team);
        Room room = getRoomById(rcTeam.getRoomId());
        assertNotNull(room);

        rcTeam = getRcTeam(team);
        assertEquals(team.getName(), rcTeam.getName());
        assertEquals(1, rcTeam.getType());
        room = getRoomById(rcTeam.getRoomId());
        assertEquals(team.getName(), room.getName());
        assertEquals("p", room.getType());

        team.setName("toto");
        teamService.getTeam(team);
        rcTeam = getRcTeam(team);
        assertEquals("toto", rcTeam.getName());
        assertEquals(1, rcTeam.getType());
        room = getRoomById(rcTeam.getRoomId());
        assertEquals("toto", room.getName());
        assertEquals("p", room.getType());

        team.setPrivacyType(PrivacyType.PUBLIC);
        teamService.getTeam(team);
        rcTeam = getRcTeam(team);
        assertEquals("toto", rcTeam.getName());
        assertEquals(0, rcTeam.getType());
        room = getRoomById(rcTeam.getRoomId());
        assertEquals("toto", room.getName());
        assertEquals("c", room.getType());

    }

    private Room getRoomById(String roomId) {
        return rocketChatApiClient.execute(new RoomInfoRequest(roomId)).getRoom();
    }

    @SneakyThrows
    @Test
    void archiveChannel1() {

        Team team = getTeam();
        Channel channel = createChannel(team, PrivacyType.PRIVATE);
        String channelName = channel.getName();

        rocketChatConnector.archiveChannel(channel);
        Room group = getRoom(channelName);
        assertTrue(group.getArchived());

        rocketChatConnector.unarchiveChannel(channel);
        group = getRoom(channelName);
        assertFalse(group.getArchived());

        rocketChatConnector.archiveChannel(channel);
        group = getRoom(channelName);
        assertTrue(group.getArchived());
    }

    @SneakyThrows
    @Test
    void archiveChannel2() {

        Channel channel = createChannel(null, PrivacyType.PUBLIC);
        String channelName = channel.getName();

        rocketChatConnector.archiveChannel(channel);
        Room group = getRoom(channelName);
        assertTrue(group.getArchived());

        rocketChatConnector.unarchiveChannel(channel);
        group = getRoom(channelName);
        assertFalse(group.getArchived());

        rocketChatConnector.archiveChannel(channel);
        group = getRoom(channelName);
        assertTrue(group.getArchived());
    }

    @SneakyThrows
    @Test
    void sendMessage() {

        Team team = getTeam();
        Channel channel = createChannel(team, PrivacyType.PRIVATE);
        String channelName = channel.getName();
        rocketChatConnector.sendMessage(channel, "hello");

        GroupMessagesResult messagesResponse = rocketChatApiClient.execute(new GroupMessagesRequest(channelName));

        List<GroupMessage> messages = messagesResponse.getMessages();
        assertEquals(2, messages.size());
        assertEquals("hello", messages.get(0).getMsg());
    }

    @SneakyThrows
    @Test
    void joinTeam() {

        Team team = getTeam();
        User user = getNewUser();
        User user2 = getNewUser();
        rocketChatConnector.joinTeam(team, Set.of(new Member(user2, Role.ADMIN)));
        rocketChatConnector.joinTeam(team, Set.of(new Member(user2, Role.USER)));
        User user3 = getNewUser();
        User user4 = getNewUser();
        TeamMembersResult teamInfoResult;
        rocketChatConnector.joinTeam(team, Set.of(
                new Member(user, Role.USER),
                new Member(user2, Role.ADMIN),
                new Member(user3, Role.USER),
                new Member(user4, Role.USER)
        ));

        teamInfoResult = rocketChatApiClient.execute(new TeamByNameMembersRequest(team.getName(), user.getUsername(), null));
        assertEquals(1, teamInfoResult.getCount());

        rocketChatConnector.joinTeam(team, Set.of(new Member(user, Role.USER)));

        teamInfoResult = rocketChatApiClient.execute(new TeamByNameMembersRequest(team.getName(), user.getUsername(), null));
        assertEquals(1, teamInfoResult.getCount());

        Room room = getRoom(team.getName());
        RcUser rcUser = getUser(user.getUsername());
        RcTeam rcTeam = getRcTeam(team);

        rocketChatApiClient.execute(new GroupKickRequest(room.getId(), rcUser.getId()));
        roomService.roomsCache.invalidateAll();

        teamInfoResult = rocketChatApiClient.execute(new TeamByNameMembersRequest(team.getName(), user.getUsername(), null));
        assertEquals(0, teamInfoResult.getCount());

        teamService.joinTeam(rcTeam, Set.of(new RcMember(rcUser, Set.of(RoomRole.MEMBER))));

        teamInfoResult = rocketChatApiClient.execute(new TeamByNameMembersRequest(team.getName(), user.getUsername(), null));
        assertEquals(1, teamInfoResult.getCount());
    }

//    @SneakyThrows
//    @Test
//    @Disabled
//    void joinTeamMultipleALot() {
//        Team team = getTeam();
//        List<User> users = IntStream.range(0, 2000).mapToObj(i -> this.getNewUser()).collect(Collectors.toList());
//        rocketChatConnector.joinTeam(team, users);
//    }

    @SneakyThrows
    @Test
    void checkChannelMultiple() {
        Team team = getTeam();
        RcTeam rcTeam = teamService.getTeam(team);

        Team team2 = getTeam();
        RcTeam rcTeam2 = teamService.getTeam(team2);

        Channel channel0 = createChannel(team, PrivacyType.PUBLIC);
        roomService.getRoom(new ChannelRcTeam(rcTeam, channel0.getId(), channel0.getName(), null, PrivacyType.PUBLIC));
        Channel channel1 = createChannel(team, PrivacyType.PUBLIC);
        roomService.getRoom(new ChannelRcTeam(rcTeam, channel1.getId(), channel1.getName(), null, PrivacyType.PRIVATE));
        Channel channel2 = createChannel(team, PrivacyType.PRIVATE);
        roomService.getRoom(new ChannelRcTeam(rcTeam2, channel2.getId(), channel2.getName(), null, PrivacyType.PRIVATE));
        Channel channel3 = createChannel(team, PrivacyType.PUBLIC);
        roomService.archive(new ChannelRcTeam(null, channel3.getId(), channel3.getName(), null, PrivacyType.PUBLIC));
        Channel channel4 = createChannel(team, PrivacyType.PRIVATE);
        roomService.archive(new ChannelRcTeam(null, channel4.getId(), channel4.getName(), null, PrivacyType.PRIVATE));

        Channel c5 = createChannel(team, PrivacyType.PUBLIC);
        Channel c6 = createChannel(team, PrivacyType.PRIVATE);
        Channel c7 = createChannel(team, PrivacyType.PUBLIC);
        Channel c8 = createChannel(team, PrivacyType.PRIVATE);
        Set<Channel> channels = Set.of(
                channel2,
                channel0,
                channel1,
                c5,
                c6,
                c7,
                c8,
                channel3,
                channel4
        );
        rocketChatConnector.checkChannels(
                channels
        );

        for (Channel channel : channels) {
            assertChannel(rcTeam, channel);
        }
    }

    private void assertChannel(RcTeam rcTeam, Channel c) {
        System.out.println(c.getName());
        ChannelRcTeam channelRcTeam = new ChannelRcTeam(rcTeam, c.getId(), c.getName(), null, c.getPrivacyType());
        Room room = getRoom(channelRcTeam.getName());
        assertEquals(c.getName(), room.getName());
        assertEquals(rcTeam.getId(), room.getTeamId());
        if (c.getPrivacyType().equals(PrivacyType.PUBLIC)) {
            assertEquals("c", room.getType());
        } else {
            assertEquals("p", room.getType());
        }
    }

    @SneakyThrows
    @Test
    void checkChannel1() {
        Team team = getTeam();
        String channelName = "checkChannel1";
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team,
                                channelName,
                                PrivacyType.PUBLIC
                        )
                )
        );
        Room room = getRoom(channelName);
        assertEquals("c", room.getType());
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(team, channelName, PrivacyType.PRIVATE)
                )
        );
        room = getRoom(channelName);
        assertEquals("p", room.getType());
    }

    @SneakyThrows
    @Test
    void checkChannelRename() {
        Team team = getTeam();
        String channelName = "checkChannelRename";
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team,
                                channelName,
                                PrivacyType.PUBLIC
                        )
                )
        );
        Room room = getRoom(channelName);
        assertEquals("c", room.getType());
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team,
                                channelName + "abcd",
                                PrivacyType.PUBLIC
                        )
                )
        );
        room = getRoom(channelName + "abcd");
        assertEquals("c", room.getType());
    }

    @SneakyThrows
    @Test
    void checkChannel1b() {
        Team team = getTeam();
        Team team2 = getTeam();
        Room room;
        String channelName = "checkChannel1b";
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team,
                                channelName,
                                PrivacyType.PRIVATE
                        )
                )
        );
        room = getRoom(channelName);
        assertEquals("p", room.getType());
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team2,
                                channelName,
                                PrivacyType.PRIVATE
                        )
                )
        );
        room = getRoom(channelName);
        assertEquals("p", room.getType());
    }

    @SneakyThrows
    @Test
    void checkChannel2() {

        Team team = getTeam();
        Team team2 = getTeam();
        Room room;
        String channelName = "checkChannel2";
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team,
                                channelName,
                                PrivacyType.PRIVATE
                        )
                )
        );
        room = getRoom(channelName);
        assertEquals("p", room.getType());
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team2,
                                channelName,
                                PrivacyType.PRIVATE
                        )
                )
        );
        room = getRoom(channelName);
        assertEquals("p", room.getType());
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                null,
                                channelName,
                                PrivacyType.PRIVATE
                        )
                )
        );
        room = getRoom(channelName);
        assertEquals("p", room.getType());
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                null,
                                channelName,
                                PrivacyType.PUBLIC
                        )
                )
        );
        room = getRoom(channelName);
        assertEquals("c", room.getType());
    }

    @SneakyThrows
    @Test
    void checkChannel3() {

        Team team = getTeam();
        teamService.getTeam(team);

        String channelName = "checkChannel3";
        teamService.teamsCache.invalidateAll();
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team,
                                channelName,
                                PrivacyType.PUBLIC
                        )
                )
        );

        Room room = getRoom(channelName);
        RcTeam rcTeam = getRcTeam(team);
        assertEquals(rcTeam.getId(), room.getTeamId());
        assertEquals("c", room.getType());
        assertEquals(channelName, room.getName());
        assertNull(room.getArchived());
    }

    @SneakyThrows
    @Test
    void checkChannel4() {

        Team team = getTeam();
        teamService.getTeam(team);

        String channelName = "checkChannel4";
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team,
                                channelName,
                                PrivacyType.PRIVATE
                        )
                )
        );

        Room room = getRoom(channelName);
        RcTeam rcTeam = getRcTeam(team);
        assertEquals(rcTeam.getId(), room.getTeamId());
        assertEquals("p", room.getType());
        assertEquals(channelName, room.getName());
        assertNull(room.getArchived());
    }

    @SneakyThrows
    @Test
    void checkChannel5() {

        Team team = getTeam();
        team.setPrivacyType(PrivacyType.PUBLIC);
        teamService.getTeam(team);

        String channelName = "checkChannel5";
        rocketChatConnector.checkChannels(
                Set.of(
                        getChannel(
                                team,
                                channelName,
                                PrivacyType.PRIVATE
                        )
                )
        );

        Room room = getRoom(channelName);
        RcTeam rcTeam = getRcTeam(team);
        assertEquals(rcTeam.getId(), room.getTeamId());
        assertEquals("p", room.getType());
        assertEquals(channelName, room.getName());
        assertNull(room.getArchived());
    }

    @SneakyThrows
    @Test
    void joinChannelMultiplePublic() {
        Team team = getTeam();
        String channelName = "joinChannelMultiplePublic";
        User user1 = getNewUser();
        User user2 = getNewUser();
        User user3 = getNewUser();
        User user4 = getNewUser();
        rocketChatConnector.joinChannel(
                getChannel(
                        team,
                        channelName,
                        PrivacyType.PUBLIC
                ),
                Set.of(
                        new Member(user2, Role.ADMIN),
                        new Member(user3, Role.USER)
                )
        );
        rocketChatConnector.joinChannel(
                getChannel(
                        team,
                        channelName,
                        PrivacyType.PUBLIC
                ),
                Set.of(
                        new Member(user1, Role.USER),
                        new Member(user2, Role.USER),
                        new Member(user3, Role.ADMIN),
                        new Member(user4, Role.USER)
                )
        );
        Room room = getRoom(channelName);
        assertTrue(isChannelMember(room.getId(), user1.getUsername()));
        assertTrue(isChannelMember(room.getId(), user2.getUsername()));
        assertTrue(isChannelMember(room.getId(), user3.getUsername()));
        assertTrue(isChannelMember(room.getId(), user4.getUsername()));
    }

    @SneakyThrows
    @Test
    void joinChannelMultiplePrivate() {
        Team team = getTeam();
        String channelName = "joinChannelMultiplePrivate";
        User user1 = getNewUser();
        User user2 = getNewUser();
        User user3 = getNewUser();
        User user4 = getNewUser();
        rocketChatConnector.joinChannel(
                getChannel(
                        team,
                        channelName,
                        PrivacyType.PRIVATE
                ),
                Set.of(
                        new Member(user2, Role.ADMIN),
                        new Member(user3, Role.USER)
                )
        );
        rocketChatConnector.joinChannel(
                getChannel(
                        team,
                        channelName,
                        PrivacyType.PRIVATE
                ),
                Set.of(
                        new Member(user1, Role.USER),
                        new Member(user2, Role.USER),
                        new Member(user3, Role.ADMIN),
                        new Member(user4, Role.USER)
                )
        );
        Room room = getRoom(channelName);
        assertTrue(isGroupMember(room.getId(), user1.getUsername()));
        assertTrue(isGroupMember(room.getId(), user2.getUsername()));
        assertTrue(isGroupMember(room.getId(), user3.getUsername()));
        assertTrue(isGroupMember(room.getId(), user4.getUsername()));
    }

//    @SneakyThrows
//    @Test
//    @Disabled
//    void joinChannelMultipleALot() {
//        Team team = getTeam();
//        String channelName = "joinChannelMultipleALot";
//        List<User> users = IntStream.range(0, 2000).mapToObj(i -> this.getNewUser()).collect(Collectors.toList());
//
//        rocketChatConnector.joinChannel(
//                new Channel(
//                        channelName,
//                        team,
//                        channelName,
//                        PrivacyType.PRIVATE
//                ),
//                users
//        );
//    }

    @SneakyThrows
    @Test
    void joinChannel1() {
        Team team = getTeam();
        String channelName = "joinChannel1";
        User user = getNewUser();
        rocketChatConnector.joinChannel(
                getChannel(
                        team,
                        channelName,
                        PrivacyType.PUBLIC
                ),
                Set.of(new Member(user, Role.USER))
        );
        Room room = getRoom(channelName);
        assertTrue(isChannelMember(room.getId(), user.getUsername()));
    }

    @SneakyThrows
    @Test
    void joinChannel1Private() {
        Team team = getTeam();
        String channelName = "joinChannel1Private";
        User user = getNewUser();
        rocketChatConnector.joinChannel(
                getChannel(
                        team,
                        channelName,
                        PrivacyType.PRIVATE
                ),
                Set.of(new Member(user, Role.USER))
        );
        Room room = getRoom(channelName);
        assertTrue(isGroupMember(room.getId(), user.getUsername()));
    }

    @SneakyThrows
    @Test
    void joinChannel2() {
        Team team = getTeam();
        Channel channel = createChannel(
                team,
                PrivacyType.PUBLIC
        );

        User user = getNewUser();

        ChannelCreateResult channelCreateResult = rocketChatApiClient.execute(
                new ChannelCreateRequest(channel.getName(), Map.of("id", channel.getId()))
        );
        String roomId = channelCreateResult.getChannel().getId();

        createUser(user);
        addUserToChannel(user, roomId);

        roomService.roomsCache.invalidateAll();
        rocketChatConnector.joinChannel(
                channel,
                Set.of(new Member(user, Role.USER))
        );

        assertTrue(isChannelMember(roomId, user.getUsername()));
    }

    @SneakyThrows
    @Test
    void joinChannel3() {
        Team team = getTeam();
        Channel channel = createChannel(
                team,
                PrivacyType.PUBLIC
        );

        User user = getNewUser();
        User user2 = getNewUser();

        ChannelCreateResult channelCreateResult = rocketChatApiClient.execute(
                new ChannelCreateRequest(channel.getName(), Map.of("id", channel.getId()))
        );
        String roomId = channelCreateResult.getChannel().getId();

        createUser(user2);
        addUserToChannel(user2, roomId);

        roomService.roomsCache.invalidateAll();
        rocketChatConnector.joinChannel(
                channel,
                Set.of(new Member(user, Role.USER))
        );

        assertTrue(isChannelMember(roomId, user.getUsername()));
    }

    @SneakyThrows
    @Test
    void joinChannel4() {
        Team team = getTeam();
        String channelName = "joinChannel4";
        User user = getNewUser();
        rocketChatConnector.joinChannel(
                getChannel(
                        team,
                        channelName,
                        PrivacyType.PRIVATE
                ),
                Set.of(new Member(user, Role.USER))
        );
        Room room = getRoom(channelName);
        assertTrue(isGroupMember(room.getId(), user.getUsername()));
        RcUser rcUser = getUser(user.getUsername());

        rocketChatApiClient.execute(new GroupKickRequest(room.getId(), rcUser.getId()));

        assertFalse(isGroupMember(room.getId(), user.getUsername()));

        roomService.joinRoom(room, Set.of(new RcMember(rcUser, Set.of(RoomRole.MEMBER))));

        assertTrue(isGroupMember(room.getId(), user.getUsername()));
    }

    private void addUserToChannel(User user, String roomId) {
        RcUser rcUser = getUser(user.getUsername());
        GroupInviteRequest groupInviteRequest = new GroupInviteRequest(roomId, List.of(rcUser.getId()));
        rocketChatApiClient.execute(groupInviteRequest);
    }

    private void createUser(User user) {
        UserCreateRequest userCreateRequest = new UserCreateRequest(
                user.getUsername(),
                "egrergergerg",
                user.getEmail(),
                user.getFirstname() + " " + user.getLastname(),
                true,
                true,
                false
        );
        rocketChatApiClient.execute(userCreateRequest);
    }

    @SneakyThrows
    @Test
    void joinChannel2Private() {
        Team team = getTeam();
        User user = getNewUser();
        Channel channel = createChannel(
                team,
                PrivacyType.PRIVATE
        );

        UserCreateRequest userCreateRequest = new UserCreateRequest(
                user.getUsername(),
                "egrergergerg",
                user.getEmail(),
                user.getFirstname() + " " + user.getLastname(),
                true,
                true,
                false
        );
        UserInfoResult userInfoResult = rocketChatApiClient.execute(userCreateRequest);
        GroupCreateResult groupCreateResult = rocketChatApiClient.execute(
                new GroupCreateRequest(channel.getName(), Map.of("id", channel.getId()))
        );
        String roomId = groupCreateResult.getGroup().getId();
        GroupInviteRequest groupInviteRequest = new GroupInviteRequest(roomId, List.of(userInfoResult.getUser().getId()));
        rocketChatApiClient.execute(groupInviteRequest);

        roomService.roomsCache.invalidateAll();
        rocketChatConnector.joinChannel(
                channel,
                Set.of(new Member(user, Role.USER))
        );

        assertTrue(isGroupMember(roomId, userCreateRequest.getUsername()));
    }

    private boolean isChannelMember(String roomId, String username) {
        MembersResult membersResult = rocketChatApiClient.execute(new ChannelMembersRequest(roomId, username, null));
        return membersResult.getCount() == 1;
    }

    private boolean isGroupMember(String roomId, String username) {
        MembersResult membersResult = rocketChatApiClient.execute(new GroupMembersRequest(roomId, username, null));
        return membersResult.getCount() == 1;
    }

    @SneakyThrows
    private RcUser getUser(String username) {

        UserInfoResult userInfoResult = rocketChatApiClient.execute(new UserInfoRequest(username));
        RcUser rcUser = userInfoResult.getUser();
        if (rcUser == null) {
            fail(username + " not found");
        }
        return rcUser;
    }

    private User getNewUser() {
        String username = "user" + (i++);
        return new User(
                username,
                username,
                username,
                username + "@gazelle.com"
        );
    }

    private Team getTeam() {
        String name = "team" + (i++);
        return new Team(name, name, name, PrivacyType.PRIVATE);
    }

    private Channel createChannel(Team team, PrivacyType privacyType) {
        String channelName = "createChannel" + (i++);
        return getChannel(
                team,
                channelName,
                privacyType
        );
    }

    private Room getRoom(String roomName) {
        RoomInfoResult roomInfoResult = rocketChatApiClient.execute(new RoomByNameInfoRequest(roomName));

        Room room = roomInfoResult.getRoom();

        if (room == null) {
            GroupInfoResult groupInfoResult = rocketChatApiClient.execute(new GroupByNameInfoRequest(roomName));
            room = groupInfoResult.getGroup();
        }

        if (room == null) {
            ChannelInfoResult channelInfoResult = rocketChatApiClient.execute(new ChannelByNameInfoRequest(roomName));
            room = channelInfoResult.getChannel();
        }
        if (room == null) {
            fail(roomName + " not found");
        }
        return room;
    }

    private RcTeam getRcTeam(Team team) {
        teamService.teamsCache.invalidateAll();
        roomService.roomsCache.invalidateAll();

        RcTeam rcTeam = teamService.getTeam(team);
        if (rcTeam == null) {
            fail(team + " not found");
        }
        return rcTeam;
    }

    private Channel getChannel(Team team, String channelName, PrivacyType privacyType) {
        return new Channel(
                "id_" + channelName,
                team,
                channelName,
                "topic_" + channelName,
                privacyType
        );
    }

}
