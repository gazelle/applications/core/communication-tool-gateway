package net.ihe.gazelle.communicationgateway.rocketchat.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.dto.result.GroupMessagesResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class GroupMessagesRequest implements Request<GroupMessagesResult> {

    String roomName;

    @Override
    public Class<GroupMessagesResult> getResultClass() {
        return GroupMessagesResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

    @Override
    public String getSuffix() {
        return "groups.messages";
    }
}
