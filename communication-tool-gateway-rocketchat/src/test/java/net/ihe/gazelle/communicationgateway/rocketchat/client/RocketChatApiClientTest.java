package net.ihe.gazelle.communicationgateway.rocketchat.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import lombok.SneakyThrows;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.ChannelCreateRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.login.result.Login;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.login.result.LoginResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request.UserInfoRequest;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.*;

@WireMockTest
class RocketChatApiClientTest {

    @SneakyThrows
    @Test
    void healthCheckOk(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        stubFor(get("/api/v1/users.info?username=gazelle").willReturn(okJson("{\"success\":true}")));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                "none",
                "gazelle",
                "gazelle"
        );
        assertDoesNotThrow(rocketChatApiClient::healthCheck);
    }

    @SneakyThrows
    @Test
    void healthCheckOk2(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        stubFor(get("/api/v1/users.info?username=gazelle").willReturn(okJson("{\"success\":true}").withStatus(400)));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                "none",
                "gazelle",
                "gazelle"
        );
        assertDoesNotThrow(rocketChatApiClient::healthCheck);
    }

    @NotNull
    private LoginResult getLoginResult() {
        Login login = new Login();
        login.setAuthToken("aaaa");
        login.setUserId("bbb");
        LoginResult loginResult = new LoginResult();
        loginResult.setData(login);
        loginResult.setSuccess(true);
        return loginResult;
    }

    @SneakyThrows
    @Test
    void healthCheckKo(WireMockRuntimeInfo wmRuntimeInfo) {
        stubFor(post("/api/v1/login").willReturn(okJson("{}")));
        stubFor(get("/api/v1/users.info?username=gazelle").willReturn(okJson("{\"success\":true}")));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        assertThrows(RocketChatException.class, rocketChatApiClient::healthCheck);
    }

    @SneakyThrows
    @Test
    void healthCheckKo2(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        stubFor(get("/api/v1/users.info?username=gazelle").willReturn(okJson("{\"success\":false}")));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        assertThrows(RocketChatException.class, rocketChatApiClient::healthCheck);
    }

    @SneakyThrows
    @Test
    void healthCheckKo3(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        stubFor(get("/api/v1/users.info?username=gazelle").willReturn(serverError()));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        assertThrows(RocketChatException.class, rocketChatApiClient::healthCheck);
    }

    @SneakyThrows
    @Test
    void healthCheckKo4(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        stubFor(get("/api/v1/users.info?username=gazelle").willReturn(aResponse().withStatus(1)));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        assertThrows(RocketChatException.class, rocketChatApiClient::healthCheck);
    }

    @SneakyThrows
    @Test
    void healthCheckOk5(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        stubFor(get("/api/v1/users.info?username=gazelle").willReturn(okJson("{\"success\":true}")));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                "none",
                "gazelle",
                "gazelle"
        );
        Thread.currentThread().interrupt();
        assertThrows(RocketChatException.class, rocketChatApiClient::healthCheck);
    }

    @SneakyThrows
    @Test
    void executeGet(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        stubFor(get("/api/v1/users.info?username=gazelle").willReturn(okJson("{\"success\":true}")));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        Result result = rocketChatApiClient.execute(new UserInfoRequest("gazelle"));
        assertTrue(result.isSuccess());
    }

    @SneakyThrows
    @Test
    void executeGetEmpty(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        stubFor(get("/api/v1/users.info?username=gazelle").willReturn(okJson("")));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        UserInfoRequest userInfoRequest = new UserInfoRequest("gazelle");
        assertThrows(RocketChatException.class, () -> rocketChatApiClient.execute(userInfoRequest));
    }

    @SneakyThrows
    @Test
    void executePost(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        stubFor(post("/api/v1/channels.create").willReturn(okJson("{\"success\":true}")));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        Result result = rocketChatApiClient.execute(new ChannelCreateRequest("hello", Map.of("a", "b")));
        assertTrue(result.isSuccess());
    }

    @SneakyThrows
    @Test
    void executePostFails(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        RecursivePostRequest recursiveRequest = new RecursivePostRequest();
        recursiveRequest.setO(recursiveRequest);
        assertThrows(RocketChatException.class, () -> rocketChatApiClient.execute(recursiveRequest));
    }

    @SneakyThrows
    @Test
    void executeGetFails(WireMockRuntimeInfo wmRuntimeInfo) {
        LoginResult loginResult = getLoginResult();
        String json = new ObjectMapper().writeValueAsString(loginResult);
        stubFor(post("/api/v1/login").willReturn(okJson(json)));
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        RecursiveGetRequest recursiveRequest = new RecursiveGetRequest();
        recursiveRequest.setO(recursiveRequest);
        assertThrows(RocketChatException.class, () -> rocketChatApiClient.execute(recursiveRequest));
    }

    @SneakyThrows
    @Test
    void readMapFails(WireMockRuntimeInfo wmRuntimeInfo) {
        RocketChatApiClient rocketChatApiClient = new RocketChatApiClient(
                wmRuntimeInfo.getHttpBaseUrl(),
                wmRuntimeInfo.getHttpBaseUrl(),
                "gazelle",
                "gazelle"
        );
        assertThrows(RocketChatException.class, () -> rocketChatApiClient.readMap("{zgfzgz"));
    }
}