package net.ihe.gazelle.communicationgateway.rocketchat;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.connector.dto.User;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request.UserCreateRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request.UserListRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.result.UserInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.result.UserListResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class UserServiceTest {

    @Mock
    RocketChatApiClient rocketChatApiClient;

    @InjectMocks
    UserService userService;

    static int i = 0;

    @Test
    void getUsersEmpty() {
        userService.usersCache.invalidateAll();
        assertTrue(userService.getUsers(Set.of()).isEmpty());
    }


    @Test
    void getUsersSingle() {
        userService.usersCache.invalidateAll();
        User user = getNewUser();
        UserInfoResult userInfoResult = new UserInfoResult();
        RcUser rcUser = asRcUser(user);
        userInfoResult.setUser(rcUser);

        UserListResult userListResult = new UserListResult();
        userListResult.setUsers(List.of(rcUser));
        when(rocketChatApiClient.execute(new UserListRequest(0))).thenReturn(userListResult);

        Set<RcUser> users = userService.getUsers(Set.of(user));
        assertEquals(1, users.size());
        assertTrue(users.contains(rcUser));

        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void getUsersExisting() {
        userService.usersCache.invalidateAll();
        User user1 = getNewUser();
        RcUser rcUser1 = asRcUser(user1);
        User user2 = getNewUser();
        RcUser rcUser2 = asRcUser(user2);
        User user3 = getNewUser();
        RcUser rcUser3 = asRcUser(user3);

        UserListResult userListResult = new UserListResult();
        userListResult.setUsers(List.of(rcUser1, rcUser2, rcUser3));
        when(rocketChatApiClient.execute(new UserListRequest(0))).thenReturn(userListResult);

        Set<RcUser> users = userService.getUsers(Set.of(user1, user2));
        assertEquals(2, users.size());
        assertTrue(users.contains(rcUser1));
        assertTrue(users.contains(rcUser2));

        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void getUsersMultiple() {
        userService.usersCache.invalidateAll();

        User user1 = getNewUser();
        User user2 = getNewUser();
        User user3 = getNewUser();
        User user4 = getNewUser();
        User user5 = getNewUser();
        User user6 = getNewUser();

        Set<User> users = Set.of(user1, user2, user3, user4, user5);

        UserListResult userListResult = new UserListResult();
        userListResult.setUsers(
                List.of(
                        asRcUser(user1),
                        asRcUser(user2),
                        asRcUser(user3),
                        asRcUser(user6)
                )
        );
        when(rocketChatApiClient.execute(new UserListRequest(0))).thenReturn(userListResult);

        UserInfoResult userInfoResultCreate4 = new UserInfoResult();
        userInfoResultCreate4.setUser(asRcUser(user4));
        UserInfoResult userInfoResultCreate5 = new UserInfoResult();
        userInfoResultCreate5.setUser(null);
        when(rocketChatApiClient.execute(any(UserCreateRequest.class))).thenReturn(userInfoResultCreate4, userInfoResultCreate5);

        Set<RcUser> rcUsers = userService.getUsers(users);

        assertEquals(4, rcUsers.size());

        verifyNoMoreInteractions(rocketChatApiClient);

    }

    @Test
    void getUserNew() {
        userService.usersCache.invalidateAll();

        User user = getNewUser();

        UserListResult userListResult = new UserListResult();
        userListResult.setUsers(List.of());
        when(rocketChatApiClient.execute(new UserListRequest(0))).thenReturn(userListResult);

        UserInfoResult userInfoResultCreate = new UserInfoResult();
        RcUser rcUser = asRcUser(user);
        userInfoResultCreate.setUser(rcUser);
        when(rocketChatApiClient.execute(any(UserCreateRequest.class))).thenReturn(userInfoResultCreate);

        Set<RcUser> result = userService.getUsers(Set.of(user));
        assertEquals(Set.of(rcUser), result);

        verifyNoMoreInteractions(rocketChatApiClient);
    }

    private User getNewUser() {
        String username = "user" + (i++);
        return new User(
                username,
                username,
                username,
                username + "@gazelle.com"
        );
    }

    private RcUser asRcUser(User user) {
        RcUser rcUser = new RcUser();
        rcUser.setId(user.getUsername());
        rcUser.setUsername(user.getUsername());
        return rcUser;
    }

}