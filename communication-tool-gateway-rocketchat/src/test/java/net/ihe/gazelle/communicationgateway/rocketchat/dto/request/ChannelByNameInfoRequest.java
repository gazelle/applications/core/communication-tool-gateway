package net.ihe.gazelle.communicationgateway.rocketchat.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelInfoResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class ChannelByNameInfoRequest implements Request<ChannelInfoResult> {

    String roomName;

    @Override
    public Class<ChannelInfoResult> getResultClass() {
        return ChannelInfoResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

    @Override
    public String getSuffix() {
        return "channels.info";
    }
}
