package net.ihe.gazelle.communicationgateway.rocketchat;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.connector.dto.PrivacyType;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.ChannelInfoRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.ChannelListRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.ChannelSetCustomFieldsRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelListResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupInfoRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupListRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupSetCustomFieldsRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupListResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.Room;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.ChangeRoomNameRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.ChangeRoomTopicRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomInfoRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.RoomInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.RcTeam;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.TeamMember;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.result.TeamCreateResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.result.TeamListResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.result.TeamMembersResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcMember;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class TeamServiceTest {

    @Mock
    RocketChatApiClient rocketChatApiClient;

    @Mock
    RoomService roomService;

    @InjectMocks
    TeamService teamService;

    static int i = 0;

    @Test
    void getTeamKnown() {
        Team team = getTeam();

        RcTeam rcTeam1 = asRcTeam(team);
        List<RcTeam> rcTeams = List.of(rcTeam1);

        TeamListResult teamListResult = new TeamListResult();
        teamListResult.setTeams(rcTeams);
        when(rocketChatApiClient.execute(new TeamListRequest(0))).thenReturn(teamListResult);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);
        GroupListResult groupListResult = new GroupListResult();
        Room teamRoom = new Room();
        teamRoom.setId(rcTeam1.getRoomId());
        teamRoom.setTeamId(rcTeam1.getId());
        teamRoom.setName(rcTeam1.getName());
        teamRoom.setCustomFields(Map.of("id", team.getId()));
        teamRoom.setTopic(team.getTopic());
        groupListResult.setGroups(List.of(teamRoom));
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        RcTeam rcTeam = teamService.getTeam(team);
        assertNotNull(rcTeam);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void getTeamKnownUpdate1() {
        getTeamKnownUpdateGeneric(true, true);
    }

    @Test
    void getTeamKnownUpdate2() {
        getTeamKnownUpdateGeneric(true, false);
    }

    @Test
    void getTeamKnownUpdate3() {
        getTeamKnownUpdateGeneric(false, true);
    }

    private void getTeamKnownUpdateGeneric(boolean wrongType, boolean wrongName) {
        Team team = getTeam();

        RcTeam rcTeam1 = asRcTeam(team);
        if (wrongType) {
            rcTeam1.setType(0);
        }
        if (wrongName) {
            rcTeam1.setName("regregregerg");
        }
        List<RcTeam> rcTeams = List.of(rcTeam1);

        TeamListResult teamListResult = new TeamListResult();
        teamListResult.setTeams(rcTeams);
        when(rocketChatApiClient.execute(new TeamListRequest(0))).thenReturn(teamListResult);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);
        GroupListResult groupListResult = new GroupListResult();
        Room teamRoom = new Room();
        teamRoom.setId(rcTeam1.getRoomId());
        teamRoom.setTeamId(rcTeam1.getId());
        teamRoom.setName(rcTeam1.getName() + " oh no");
        teamRoom.setCustomFields(Map.of("id", team.getId()));
        groupListResult.setGroups(List.of(teamRoom));
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        when(rocketChatApiClient.execute(any(UpdateTeamRequest.class))).thenReturn(null);
        when(rocketChatApiClient.execute(any(ChangeRoomNameRequest.class))).thenReturn(null);
        when(rocketChatApiClient.execute(any(ChangeRoomTopicRequest.class))).thenReturn(null);

        RcTeam rcTeam = teamService.getTeam(team);
        assertNotNull(rcTeam);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void getTeamUnknown1() {
        Team team = getTeam();

        RcTeam rcTeam = initCreate(team);

        RoomInfoResult roomInfoResult = new RoomInfoResult();
        roomInfoResult.setSuccess(true);
        Room room = new Room();
        room.setType("c");
        roomInfoResult.setRoom(room);
        when(rocketChatApiClient.execute(new RoomInfoRequest(rcTeam.getRoomId()))).thenReturn(roomInfoResult);

        when(rocketChatApiClient.execute(any(ChannelSetCustomFieldsRequest.class))).thenReturn(null);
        when(rocketChatApiClient.execute(any(ChangeRoomTopicRequest.class))).thenReturn(null);

        rcTeam = teamService.getTeam(team);
        assertNotNull(rcTeam);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void getTeamUnknown2() {
        Team team = getTeam();

        team.setPrivacyType(PrivacyType.PUBLIC);
        RcTeam rcTeam = initCreate(team);

        Room room = new Room();
        room.setType("p");

        RoomInfoResult roomInfoResult = new RoomInfoResult();
        roomInfoResult.setRoom(null);
        roomInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new RoomInfoRequest(rcTeam.getRoomId()))).thenReturn(roomInfoResult);
        GroupInfoResult groupInfoResult = new GroupInfoResult();
        groupInfoResult.setGroup(null);
        groupInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new GroupInfoRequest(rcTeam.getRoomId()))).thenReturn(groupInfoResult);
        ChannelInfoResult channelInfoResult = new ChannelInfoResult();
        channelInfoResult.setChannel(room);
        channelInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new ChannelInfoRequest(rcTeam.getRoomId()))).thenReturn(channelInfoResult);

        when(rocketChatApiClient.execute(any(GroupSetCustomFieldsRequest.class))).thenReturn(null);
        when(rocketChatApiClient.execute(any(ChangeRoomTopicRequest.class))).thenReturn(null);

        rcTeam = teamService.getTeam(team);
        assertNotNull(rcTeam);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void getTeamUnknown3() {
        Team team = getTeam();
        team.setTopic(null);

        team.setPrivacyType(PrivacyType.PUBLIC);
        RcTeam rcTeam = asRcTeam(team);
        rcTeam.setRoomCustomFieldsId(null);

        TeamListResult teamListResult = new TeamListResult();
        teamListResult.setTeams(List.of(rcTeam));
        when(rocketChatApiClient.execute(new TeamListRequest(0))).thenReturn(teamListResult);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);
        GroupListResult groupListResult = new GroupListResult();
        Room teamRoom = new Room();
        teamRoom.setId(rcTeam.getRoomId());
        teamRoom.setTeamId(rcTeam.getId());
        teamRoom.setName(rcTeam.getName());
        teamRoom.setCustomFields(null);
        groupListResult.setGroups(List.of(teamRoom));
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        TeamCreateResult teamCreateResult = new TeamCreateResult();
        teamCreateResult.setSuccess(true);
        teamCreateResult.setTeam(rcTeam);
        when(rocketChatApiClient.execute(any(TeamCreateRequest.class))).thenReturn(teamCreateResult);

        Room room = new Room();
        room.setType("p");
        RoomInfoResult roomInfoResult = new RoomInfoResult();
        roomInfoResult.setRoom(room);
        roomInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new RoomInfoRequest(rcTeam.getRoomId())))
                .thenReturn(roomInfoResult);

        when(rocketChatApiClient.execute(any(GroupSetCustomFieldsRequest.class))).thenReturn(null);

        rcTeam = teamService.getTeam(team);
        assertNotNull(rcTeam);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @NotNull
    private RcTeam initCreate(Team team) {
        TeamListResult teamListResult = new TeamListResult();
        teamListResult.setTeams(List.of());
        when(rocketChatApiClient.execute(new TeamListRequest(0))).thenReturn(teamListResult);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);
        GroupListResult groupListResult = new GroupListResult();
        groupListResult.setGroups(List.of());
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        TeamCreateResult teamCreateResult = new TeamCreateResult();
        teamCreateResult.setSuccess(true);
        RcTeam rcTeam = asRcTeam(team);
        teamCreateResult.setTeam(rcTeam);
        when(rocketChatApiClient.execute(any(TeamCreateRequest.class))).thenReturn(teamCreateResult);
        return rcTeam;
    }

    private RcTeam asRcTeam(Team team) {
        RcTeam rcTeam = new RcTeam();
        rcTeam.setId("team__" + team.getId());
        rcTeam.setName(team.getName());
        if (team.getPrivacyType() == PrivacyType.PRIVATE) {
            rcTeam.setType(1);
        } else {
            rcTeam.setType(0);
        }
        rcTeam.setRoomId("r" + team.getId());
        rcTeam.setRoomCustomFieldsId(team.getId());
        return rcTeam;
    }

    @Test
    void joinTeamEmpty() {

        Team team = getTeam();
        RcTeam rcTeam = asRcTeam(team);

        teamService.joinTeam(rcTeam, Set.of());

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void joinTeamSingleIn() {

        Team team = getTeam();
        RcTeam rcTeam = asRcTeam(team);

        RcUser rcUser = getRcUser();
        Set<RcMember> members = Set.of(new RcMember(rcUser, Set.of(RoomRole.MODERATOR)));

        TeamMembersResult teamMembersResult = new TeamMembersResult();
        teamMembersResult.setSuccess(true);
        teamMembersResult.setCount(1);
        TeamMember teamMember = new TeamMember();
        teamMember.setUser(rcUser);
        teamMember.setRoles(Set.of(RoomRole.MEMBER, RoomRole.LEADER));
        teamMembersResult.setMembers(List.of(teamMember));

        when(rocketChatApiClient.execute(new TeamMembersRequest(rcTeam.getId(), null, 0)))
                .thenReturn(teamMembersResult);

        Room room = mock(Room.class);
        RoomInfoResult roomInfoResult = new RoomInfoResult();
        roomInfoResult.setRoom(room);
        roomInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new RoomInfoRequest(rcTeam.getRoomId())))
                .thenReturn(roomInfoResult);

        doNothing().when(roomService).joinRoom(room, members);

        teamService.joinTeam(rcTeam, members);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void joinTeamSingleNot() {

        Team team = getTeam();
        RcTeam rcTeam = asRcTeam(team);

        RcUser rcUser = getRcUser();
        Set<RcMember> members = Set.of(new RcMember(rcUser, Set.of(RoomRole.MEMBER)));

        TeamMembersResult teamMembersResult = new TeamMembersResult();
        teamMembersResult.setSuccess(true);
        teamMembersResult.setCount(0);
        teamMembersResult.setMembers(List.of());
        when(rocketChatApiClient.execute(new TeamMembersRequest(rcTeam.getId(), null, 0)))
                .thenReturn(teamMembersResult);

        when(rocketChatApiClient.execute(new TeamAddMembersRequest(rcTeam.getId(), List.of(new RcTeamMember(rcUser.getId(), Set.of(RoomRole.MEMBER))))))
                .thenReturn(null);

        Room room = mock(Room.class);
        RoomInfoResult roomInfoResult = new RoomInfoResult();
        roomInfoResult.setRoom(room);
        roomInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new RoomInfoRequest(rcTeam.getRoomId())))
                .thenReturn(roomInfoResult);

        doNothing().when(roomService).joinRoom(room, members);

        teamService.joinTeam(rcTeam, members);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void joinTeamMultiple() {

        Team team = getTeam();
        RcTeam rcTeam = asRcTeam(team);

        RcUser rcUser1 = getRcUser();
        RcUser rcUser2 = getRcUser();

        List<RcUser> rcUsers = List.of(rcUser1, rcUser2);
        Set<RcMember> rcMembers = rcUsers.stream()
                .map(rcUser -> new RcMember(rcUser, Set.of(RoomRole.MEMBER, RoomRole.MODERATOR, RoomRole.OWNER, RoomRole.LEADER)))
                .collect(Collectors.toSet());

        TeamMembersResult teamMembersResult = new TeamMembersResult();
        teamMembersResult.setSuccess(true);
        teamMembersResult.setCount(1);
        TeamMember teamMember = new TeamMember();
        teamMember.setUser(rcUser1);
        teamMember.setRoles(Set.of());
        TeamMember teamMember2 = new TeamMember();
        teamMember2.setUser(rcUser2);
        teamMember2.setRoles(Set.of());
        teamMembersResult.setMembers(List.of(teamMember, teamMember2));
        when(rocketChatApiClient.execute(new TeamMembersRequest(rcTeam.getId(), null, 0)))
                .thenReturn(teamMembersResult);

        Room room = mock(Room.class);
        RoomInfoResult roomInfoResult = new RoomInfoResult();
        roomInfoResult.setRoom(room);
        roomInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new RoomInfoRequest(rcTeam.getRoomId())))
                .thenReturn(roomInfoResult);

        doNothing().when(roomService).joinRoom(room, rcMembers);

        teamService.joinTeam(rcTeam, rcMembers);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void setTeamAdmins() {
        Team team = getTeam();
        RcTeam rcTeam = asRcTeam(team);

        RcUser rcUser1 = getRcUser();
        RcUser rcUser2 = getRcUser();

        Room room = mock(Room.class);
        RoomInfoResult roomInfoResult = new RoomInfoResult();
        roomInfoResult.setRoom(room);
        roomInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new RoomInfoRequest(rcTeam.getRoomId())))
                .thenReturn(roomInfoResult);

        doNothing().when(roomService).setRoomAdmins(room, Set.of(rcUser1, rcUser2));

        teamService.setTeamAdmins(rcTeam, Set.of(rcUser1, rcUser2));

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void setTeamMembers() {

        Team team = getTeam();
        RcTeam rcTeam = asRcTeam(team);

        RcUser rcUser1 = getRcUser();
        RcUser rcUser2 = getRcUser();
        RcUser rcUser3 = getRcUser();
        RcUser rcUserGazelle = getRcUser();
        rcUserGazelle.setUsername("gazelle");

        List<RcUser> rcUsers = List.of(rcUser1, rcUser2);
        Set<RcMember> rcMembers = rcUsers.stream()
                .map(rcUser -> new RcMember(rcUser, Set.of(RoomRole.MEMBER, RoomRole.MODERATOR, RoomRole.OWNER, RoomRole.LEADER)))
                .collect(Collectors.toSet());

        TeamMembersResult teamMembersResult = new TeamMembersResult();
        teamMembersResult.setSuccess(true);
        teamMembersResult.setCount(1);
        TeamMember teamMember = new TeamMember();
        teamMember.setUser(rcUser1);
        teamMember.setRoles(Set.of());
        TeamMember teamMember2 = new TeamMember();
        teamMember2.setUser(rcUser2);
        teamMember2.setRoles(Set.of());
        TeamMember teamMember3 = new TeamMember();
        teamMember3.setUser(rcUser3);
        teamMember3.setRoles(Set.of());
        TeamMember teamMemberGazelle = new TeamMember();
        teamMemberGazelle.setUser(rcUserGazelle);
        teamMemberGazelle.setRoles(Set.of(RoomRole.OWNER));
        teamMembersResult.setMembers(List.of(teamMember, teamMember2, teamMember3, teamMemberGazelle));
        when(rocketChatApiClient.execute(new TeamMembersRequest(rcTeam.getId(), null, 0)))
                .thenReturn(teamMembersResult);

        Room room = mock(Room.class);
        RoomInfoResult roomInfoResult = new RoomInfoResult();
        roomInfoResult.setRoom(room);
        roomInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new RoomInfoRequest(rcTeam.getRoomId())))
                .thenReturn(roomInfoResult);

        when(rocketChatApiClient.execute(new TeamRemoveMemberRequest(rcTeam.getId(), rcUser3.getId())))
                .thenReturn(roomInfoResult);

        doNothing().when(roomService).setRoomMembers(room, rcMembers);

        teamService.setTeamMembers(rcTeam, rcMembers);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    @Test
    void joinTeamMultipleAdd() {

        Team team = getTeam();
        RcTeam rcTeam = asRcTeam(team);

        RcUser rcUser1 = getRcUser();
        RcUser rcUser2 = getRcUser();

        List<RcUser> rcUsers = List.of(rcUser1, rcUser2);
        Set<RcMember> rcMembers = rcUsers.stream()
                .map(rcUser -> new RcMember(rcUser, Set.of(RoomRole.MEMBER, RoomRole.MODERATOR)))
                .collect(Collectors.toSet());

        TeamMembersResult teamMembersResult = new TeamMembersResult();
        teamMembersResult.setSuccess(true);
        teamMembersResult.setCount(1);
        TeamMember teamMember = new TeamMember();
        teamMember.setUser(rcUser1);
        teamMember.setRoles(Set.of());
        teamMembersResult.setMembers(List.of(teamMember));
        when(rocketChatApiClient.execute(new TeamMembersRequest(rcTeam.getId(), null, 0)))
                .thenReturn(teamMembersResult);

        List<RcTeamMember> rcTeamMemberList = List.of(new RcTeamMember(rcUser2.getId(), Set.of(RoomRole.MEMBER)));
        TeamAddMembersRequest teamAddMembersRequest = new TeamAddMembersRequest(rcTeam.getId(), rcTeamMemberList);
        when(rocketChatApiClient.execute(teamAddMembersRequest)).thenReturn(null);

        Room room = mock(Room.class);
        RoomInfoResult roomInfoResult = new RoomInfoResult();
        roomInfoResult.setRoom(room);
        roomInfoResult.setSuccess(true);
        when(rocketChatApiClient.execute(new RoomInfoRequest(rcTeam.getRoomId())))
                .thenReturn(roomInfoResult);

        doNothing().when(roomService).joinRoom(room, rcMembers);

        teamService.joinTeam(rcTeam, rcMembers);

        verifyNoMoreInteractions(rocketChatApiClient, roomService);
    }

    private RcUser getRcUser() {
        RcUser rcUser = new RcUser();
        String username = "user_" + (i++);
        rcUser.setId(username);
        rcUser.setUsername(username);
        return rcUser;
    }

    private Team getTeam() {
        String name = "team" + (i++);
        return new Team(name, name, name, PrivacyType.PRIVATE);
    }

}