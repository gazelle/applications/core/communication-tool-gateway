package net.ihe.gazelle.communicationgateway.rocketchat.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.RoomInfoResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class RoomByNameInfoRequest implements Request<RoomInfoResult> {

    String roomName;

    @Override
    public Class<RoomInfoResult> getResultClass() {
        return RoomInfoResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

    @Override
    public String getSuffix() {
        return "rooms.info";
    }
}
