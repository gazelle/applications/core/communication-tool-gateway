package net.ihe.gazelle.communicationgateway.rocketchat;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.PrivacyType;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelCreateResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelListResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupCreateResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupListResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.Room;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.InvitationLinkResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.MembersResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.RoomRolesResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.RcTeam;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request.AddRoomsRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request.RemoveRoomRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcMember;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;
import net.ihe.gazelle.communicationgateway.rocketchat.internal.ChannelRcTeam;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class RoomServiceTest {

    @Mock
    RocketChatApiClient rocketChatApiClient;

    @InjectMocks
    RoomService roomService;

    static int i = 0;

    @Test
    void getRooms() {
        roomService.rocketchatUrl = "rocketchatUrl";
        roomService.roomsCache.invalidateAll();

        Team team = mock(Team.class);
        RcTeam rcTeam = mock(RcTeam.class);
        when(rcTeam.getId()).thenReturn("rcTeamId");

        Room r1 = new Room();
        r1.setCustomFields(Map.of("id", "a"));
        r1.setName("nameA");
        r1.setTopic("topicA");
        r1.setTeamId("rcTeamId");
        r1.setType("c");

        Room r2 = new Room();
        r2.setCustomFields(Map.of("id", "b"));
        r2.setTeamId("rcTeamId2");

        Room r3 = new Room();
        r3.setCustomFields(Map.of("id", "c"));
        r3.setTeamId(null);

        Room r4 = new Room();
        r4.setCustomFields(Map.of("id", "d"));
        r4.setName("nameD");
        r4.setTopic("topicD");
        r4.setTeamId("rcTeamId");
        r4.setType("p");

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setSuccess(true);
        channelListResult.setChannels(List.of(r1, r2));
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);

        GroupListResult groupListResult = new GroupListResult();
        groupListResult.setSuccess(true);
        groupListResult.setGroups(List.of(r3, r4));
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        assertEquals(Set.of(
                new Channel("a", team, "nameA", "topicA", PrivacyType.PUBLIC),
                new Channel("d", team, "nameD", "topicD", PrivacyType.PRIVATE)
        ), roomService.getRooms(team, rcTeam));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void checkRoomsCreateNoTeamPublic() {
        roomService.rocketchatUrl = "rocketchatUrl";
        roomService.roomsCache.invalidateAll();

        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setSuccess(true);
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);

        GroupListResult groupListResult = new GroupListResult();
        groupListResult.setSuccess(true);
        groupListResult.setGroups(List.of());
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        Room room = asRoom(channel);

        ChannelCreateResult channelCreateResult = new ChannelCreateResult();
        channelCreateResult.setSuccess(true);
        channelCreateResult.setChannel(room);
        when(rocketChatApiClient.execute(new ChannelCreateRequest(channel.getName(), Map.of("id", channel.getId())))).thenReturn(channelCreateResult);
        when(rocketChatApiClient.execute(new ChangeRoomTopicRequest(room.getId(), channel.getTopic()))).thenReturn(null);
        when(rocketChatApiClient.execute(new ChannelCloseRequest(room.getId()))).thenReturn(null);

        roomService.checkRooms(Set.of(channel), false);

        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void checkRoomsCreateTeamPrivate() {
        roomService.rocketchatUrl = "rocketchatUrl";
        roomService.roomsCache.invalidateAll();

        RcTeam rcTeam = new RcTeam();
        String name = "t" + (i++);
        rcTeam.setId(name);
        rcTeam.setName(name);
        ChannelRcTeam channel = getChannel(rcTeam, PrivacyType.PRIVATE);
        channel.setTopic(null);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setSuccess(true);
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);

        GroupListResult groupListResult = new GroupListResult();
        groupListResult.setSuccess(true);
        groupListResult.setGroups(List.of());
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        Room room = asRoom(channel);

        GroupCreateResult groupCreateResult = new GroupCreateResult();
        groupCreateResult.setSuccess(true);
        groupCreateResult.setGroup(room);
        when(rocketChatApiClient.execute(new GroupCreateRequest(channel.getName(), Map.of("id", channel.getId())))).thenReturn(groupCreateResult);
        when(rocketChatApiClient.execute(new GroupCloseRequest(room.getId()))).thenReturn(null);
        when(rocketChatApiClient.execute(new AddRoomsRequest(rcTeam.getName(), List.of(room.getId())))).thenReturn(null);
        roomService.checkRooms(Set.of(channel), false);

        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void checkRoomsCreateCheck1() {
        checkRoomsCreateCheckGeneric(false, false, false);
    }

    @Test
    void checkRoomsCreateCheck2() {
        checkRoomsCreateCheckGeneric(false, true, false);
    }


    @Test
    void checkRoomsCreateCheck3() {
        checkRoomsCreateCheckGeneric(true, false, false);
    }

    @Test
    void checkRoomsCreateCheck4() {
        checkRoomsCreateCheckGeneric(false, false, true);
    }

    private void checkRoomsCreateCheckGeneric(boolean roomTeamIdNull, boolean roomTeamIdOk, boolean archived) {
        roomService.rocketchatUrl = "rocketchatUrl";
        roomService.roomsCache.invalidateAll();

        RcTeam rcTeam = new RcTeam();
        String name = "t" + (i++);
        rcTeam.setId(name);
        rcTeam.setName(name);
        ChannelRcTeam channel = getChannel(rcTeam, PrivacyType.PRIVATE);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setSuccess(true);
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);

        GroupListResult groupListResult = new GroupListResult();
        groupListResult.setSuccess(true);

        Room room = asRoom(channel);
        room.setType("c");
        room.setName("toto");
        room.setTopic("toto");
        if (roomTeamIdNull) {
            room.setTeamId(null);
        } else {
            if (!roomTeamIdOk) {
                room.setTeamId("gergreg");
            }
        }
        if (archived) {
            room.setArchived(true);
        }

        Room room2 = asRoom(getChannel(rcTeam, PrivacyType.PRIVATE));
        room2.setCustomFields(null);

        Room room3 = asRoom(getChannel(rcTeam, PrivacyType.PRIVATE));
        room3.setCustomFields(Map.of());

        groupListResult.setGroups(List.of(room, room2, room3));

        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        when(rocketChatApiClient.execute(new ChangeRoomTypeRequest(room.getId(), "p"))).thenReturn(null);
        when(rocketChatApiClient.execute(new ChangeRoomNameRequest(room.getId(), channel.getName()))).thenReturn(null);
        when(rocketChatApiClient.execute(new ChangeRoomTopicRequest(room.getId(), channel.getTopic()))).thenReturn(null);
        if (!roomTeamIdNull) {
            if (!roomTeamIdOk) {
                when(rocketChatApiClient.execute(new RemoveRoomRequest("gergreg", room.getId()))).thenReturn(null);
            }
        }
        if (!roomTeamIdOk) {
            when(rocketChatApiClient.execute(new AddRoomsRequest(rcTeam.getName(), List.of(room.getId())))).thenReturn(null);
        }
        if (archived) {
            when(rocketChatApiClient.execute(new RoomChangeArchivationStatusRequest(room.getId(), "unarchive"))).thenReturn(null);
        }

        roomService.checkRooms(Set.of(channel), true);

        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void joinChannelSingleAlready() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);
        Room room = asRoom(channel);

        RcUser user = getRcUser();

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of(new RcMember(user, Set.of(RoomRole.MODERATOR, RoomRole.LEADER, RoomRole.OWNER))));
        when(rocketChatApiClient.execute(new ChannelRolesRequest(room.getId())))
                .thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of(user, getRcUser()));
        when(rocketChatApiClient.execute(new ChannelMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new ChannelRemoveLeader(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new ChannelRemoveModerator(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new ChannelRemoveOwner(room.getId(), user.getId())))
                .thenReturn(null);

        roomService.joinRoom(room, Set.of(new RcMember(user, Set.of(RoomRole.MEMBER))));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void joinChannelSingleAlready2() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);
        Room room = asRoom(channel);

        RcUser user = getRcUser();

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of(new RcMember(user, Set.of(RoomRole.LEADER, RoomRole.OWNER))));
        when(rocketChatApiClient.execute(new ChannelRolesRequest(room.getId())))
                .thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of());
        when(rocketChatApiClient.execute(new ChannelMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new ChannelRemoveLeader(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new ChannelAddModerator(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new ChannelRemoveOwner(room.getId(), user.getId())))
                .thenReturn(null);

        roomService.joinRoom(room, Set.of(new RcMember(user, Set.of(RoomRole.MODERATOR))));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void joinChannelSingleNope() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);
        Room room = asRoom(channel);

        RcUser user = getRcUser();

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of());
        when(rocketChatApiClient.execute(new ChannelRolesRequest(room.getId())))
                .thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of());
        when(rocketChatApiClient.execute(new ChannelMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new ChannelInviteRequest(room.getId(), List.of(user.getId()))))
                .thenReturn(null);

        when(rocketChatApiClient.execute(new ChannelAddLeader(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new ChannelAddModerator(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new ChannelAddOwner(room.getId(), user.getId())))
                .thenReturn(null);

        roomService.joinRoom(room, Set.of(new RcMember(user, Set.of(
                RoomRole.MODERATOR, RoomRole.LEADER, RoomRole.OWNER
        ))));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void joinGroupNone() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PRIVATE);
        Room room = asRoom(channel);

        roomService.joinRoom(room, Set.of());
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void joinGroupSingleAlready() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PRIVATE);
        Room room = asRoom(channel);

        RcUser user = getRcUser();

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(
                List.of(new RcMember(user, Set.of(RoomRole.MODERATOR, RoomRole.LEADER, RoomRole.OWNER)))
        );
        when(rocketChatApiClient.execute(new GroupRolesRequest(room.getId())))
                .thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of());
        when(rocketChatApiClient.execute(new GroupMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new GroupRemoveLeader(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new GroupRemoveModerator(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new GroupRemoveOwner(room.getId(), user.getId())))
                .thenReturn(null);

        roomService.joinRoom(room, Set.of(new RcMember(user, Set.of(RoomRole.MEMBER))));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void joinGroupSingleNope() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PRIVATE);
        Room room = asRoom(channel);

        RcUser user = getRcUser();

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of());
        when(rocketChatApiClient.execute(new GroupRolesRequest(room.getId())))
                .thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of());
        when(rocketChatApiClient.execute(new GroupMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new GroupInviteRequest(room.getId(), List.of(user.getId()))))
                .thenReturn(null);

        when(rocketChatApiClient.execute(new GroupAddLeader(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new GroupAddModerator(room.getId(), user.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new GroupAddOwner(room.getId(), user.getId())))
                .thenReturn(null);

        roomService.joinRoom(room, Set.of(new RcMember(user, Set.of(
                RoomRole.MODERATOR, RoomRole.LEADER, RoomRole.OWNER
        ))));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void joinChannelMultiple() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);
        Room room = asRoom(channel);

        RcUser user1 = getRcUser();
        RcUser user2 = getRcUser();

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of(new RcMember(user1, Set.of(RoomRole.MEMBER))));
        when(rocketChatApiClient.execute(new ChannelRolesRequest(room.getId())))
                .thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of());
        when(rocketChatApiClient.execute(new ChannelMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new ChannelInviteRequest(room.getId(), List.of(user2.getId()))))
                .thenReturn(null);

        roomService.joinRoom(room, Set.of(
                new RcMember(user1, Set.of(RoomRole.MEMBER)),
                new RcMember(user2, Set.of(RoomRole.MEMBER))
        ));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void joinGroupMultiple() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PRIVATE);
        Room room = asRoom(channel);

        RcUser user1 = getRcUser();
        RcUser user2 = getRcUser();

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of(new RcMember(user1, Set.of(RoomRole.MODERATOR))));
        when(rocketChatApiClient.execute(new GroupRolesRequest(room.getId())))
                .thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of());
        when(rocketChatApiClient.execute(new GroupMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new GroupInviteRequest(room.getId(), List.of(user2.getId()))))
                .thenReturn(null);

        when(rocketChatApiClient.execute(new GroupAddModerator(room.getId(), user2.getId())))
                .thenReturn(null);

        roomService.joinRoom(room, Set.of(
                new RcMember(user1, Set.of(RoomRole.MODERATOR)),
                new RcMember(user2, Set.of(RoomRole.MODERATOR))
        ));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void setRoomMembers() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);
        Room room = asRoom(channel);

        RcUser user1 = getRcUser();
        RcUser user2 = getRcUser();
        RcUser user3 = getRcUser();
        RcUser userGazelle = getRcUser();
        userGazelle.setUsername("gazelle");

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of(
                new RcMember(user1, Set.of(RoomRole.MEMBER)),
                new RcMember(userGazelle, Set.of(RoomRole.OWNER, RoomRole.MODERATOR))
        ));
        when(rocketChatApiClient.execute(new ChannelRolesRequest(room.getId())))
                .thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of(user3));
        when(rocketChatApiClient.execute(new ChannelMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new ChannelInviteRequest(room.getId(), List.of(user2.getId()))))
                .thenReturn(null);

        when(rocketChatApiClient.execute(new ChannelInviteRequest(room.getId(), List.of(user2.getId()))))
                .thenReturn(null);

        when(rocketChatApiClient.execute(new ChannelKickRequest(room.getId(), user3.getId())))
                .thenReturn(null);

        roomService.setRoomMembers(room, Set.of(
                new RcMember(user1, Set.of(RoomRole.MEMBER)),
                new RcMember(user2, Set.of(RoomRole.MEMBER))
        ));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void setRoomAdmins() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);
        Room room = asRoom(channel);

        RcUser user1 = getRcUser();
        RcUser user2 = getRcUser();
        RcUser user3 = getRcUser();
        RcUser userGazelle = getRcUser();
        userGazelle.setUsername("gazelle");

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of(
                new RcMember(user3, Set.of(RoomRole.MODERATOR)),
                new RcMember(userGazelle, Set.of(RoomRole.OWNER, RoomRole.MODERATOR))
        ));
        when(rocketChatApiClient.execute(new ChannelRolesRequest(room.getId())))
                .thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of(user1));
        when(rocketChatApiClient.execute(new ChannelMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new ChannelInviteRequest(room.getId(), List.of(user2.getId()))))
                .thenReturn(null);

        when(rocketChatApiClient.execute(new ChannelAddModerator(room.getId(), user1.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new ChannelAddModerator(room.getId(), user2.getId())))
                .thenReturn(null);
        when(rocketChatApiClient.execute(new ChannelRemoveModerator(room.getId(), user3.getId())))
                .thenReturn(null);

        roomService.setRoomAdmins(room, Set.of(
                user1, user2
        ));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void leaveRoomEmpty() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);
        Room room = asRoom(channel);
        roomService.leaveRoom(room, Set.of());
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void leaveChannel() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);
        Room room = asRoom(channel);

        RcUser user = getRcUser();
        RcUser user2 = getRcUser();
        RcUser user3 = getRcUser();

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of(new RcMember(user, Set.of(RoomRole.MODERATOR, RoomRole.LEADER, RoomRole.OWNER))));
        when(rocketChatApiClient.execute(new ChannelRolesRequest(room.getId()))).thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of(user2));
        when(rocketChatApiClient.execute(new ChannelMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new ChannelKickRequest(room.getId(), user.getId()))).thenReturn(null);

        roomService.leaveRoom(room, Set.of(user, user3));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void leaveGroup() {
        ChannelRcTeam channel = getChannel(null, PrivacyType.PRIVATE);
        Room room = asRoom(channel);

        RcUser user = getRcUser();
        RcUser user2 = getRcUser();
        RcUser user3 = getRcUser();

        RoomRolesResult roomRolesResult = new RoomRolesResult();
        roomRolesResult.setSuccess(true);
        roomRolesResult.setRoles(List.of(new RcMember(user, Set.of(RoomRole.MODERATOR, RoomRole.LEADER, RoomRole.OWNER))));
        when(rocketChatApiClient.execute(new GroupRolesRequest(room.getId()))).thenReturn(roomRolesResult);

        MembersResult membersResult = new MembersResult();
        membersResult.setMembers(List.of(user2));
        when(rocketChatApiClient.execute(new GroupMembersRequest(room.getId(), null, 0)))
                .thenReturn(membersResult);

        when(rocketChatApiClient.execute(new GroupKickRequest(room.getId(), user.getId()))).thenReturn(null);

        roomService.leaveRoom(room, Set.of(user, user3));
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @NotNull
    private RcUser getRcUser() {
        RcUser user = new RcUser();
        String username = "u" + (i++);
        user.setId(username);
        user.setUsername(username);
        return user;
    }

    @Test
    void archive() {
        roomService.rocketchatUrl = "rocketchatUrl";
        roomService.roomsCache.invalidateAll();

        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setSuccess(true);
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);

        Room room = asRoom(channel);

        GroupListResult groupListResult = new GroupListResult();
        groupListResult.setSuccess(true);
        groupListResult.setGroups(List.of(room));
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        when(rocketChatApiClient.execute(new RoomChangeArchivationStatusRequest(room.getId(), "archive"))).thenReturn(null);

        roomService.archive(channel);
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void unarchive() {
        roomService.rocketchatUrl = "rocketchatUrl";
        roomService.roomsCache.invalidateAll();

        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setSuccess(true);
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);

        Room room = asRoom(channel);

        GroupListResult groupListResult = new GroupListResult();
        groupListResult.setSuccess(true);
        groupListResult.setGroups(List.of(room));
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        when(rocketChatApiClient.execute(new RoomChangeArchivationStatusRequest(room.getId(), "unarchive"))).thenReturn(null);

        roomService.unarchive(channel);
        verifyNoMoreInteractions(rocketChatApiClient);
    }

    @Test
    void getInvitationLink() {
        roomService.rocketchatUrl = "rocketchatUrl";
        roomService.roomsCache.invalidateAll();

        ChannelRcTeam channel = getChannel(null, PrivacyType.PUBLIC);

        ChannelListResult channelListResult = new ChannelListResult();
        channelListResult.setSuccess(true);
        channelListResult.setChannels(List.of());
        when(rocketChatApiClient.execute(new ChannelListRequest(0))).thenReturn(channelListResult);

        Room room = asRoom(channel);
        room.setTeamId("gergreg");

        GroupListResult groupListResult = new GroupListResult();
        groupListResult.setSuccess(true);
        groupListResult.setGroups(List.of(room));
        when(rocketChatApiClient.execute(new GroupListRequest(0))).thenReturn(groupListResult);

        when(rocketChatApiClient.execute(new RemoveRoomRequest("gergreg", room.getId()))).thenReturn(null);

        String rid = "r" + channel.getId();
        FindOrCreateInviteRequest findOrCreateInviteRequest = new FindOrCreateInviteRequest(
                rid, 0, 0
        );
        InvitationLinkResult result = new InvitationLinkResult();
        result.setSuccess(true);
        result.setId("linkid");
        when(rocketChatApiClient.execute(findOrCreateInviteRequest)).thenReturn(result);

        String invitationLink = roomService.getInvitationLink(channel);
        assertEquals("rocketchatUrl/invite/linkid", invitationLink);

        verifyNoMoreInteractions(rocketChatApiClient);
    }

    private Room asRoom(ChannelRcTeam channel) {
        Room room = new Room();
        room.setId("r" + channel.getId());
        room.setName(channel.getName());
        if (channel.getPrivacyType() == PrivacyType.PRIVATE) {
            room.setType("p");
        } else {
            room.setType("c");
        }
        room.setCustomFields(Map.of("id", channel.getId()));
        if (channel.getRcTeam() != null) {
            room.setTeamId(channel.getRcTeam().getId());
        }
        room.setArchived(false);
        room.setTopic(channel.getTopic());
        return room;
    }

    private ChannelRcTeam getChannel(RcTeam team, PrivacyType type) {
        String name = "channel_" + (i++);
        return new ChannelRcTeam(team, "id_" + name, "name_" + name, "topic_" + name, type);
    }
}