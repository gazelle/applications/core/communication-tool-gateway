package net.ihe.gazelle.communicationgateway.rocketchat;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.PrivacyType;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelCreateResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupCreateResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.Room;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.InvitationLinkResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.MembersResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.RoomRolesResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.RcTeam;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request.AddRoomsRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request.RemoveRoomRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcMember;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;
import net.ihe.gazelle.communicationgateway.rocketchat.internal.ChannelRcTeam;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Duration;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Singleton
@NoArgsConstructor
@AllArgsConstructor
public class RoomService {

    public static final String CACHE_KEY = "";

    private final LoadingCache<ChannelRcTeam, String> invitationLinks = Caffeine.newBuilder()
            .expireAfterAccess(Duration.ofHours(1))
            .build(this::loadInvitationLink);

    // all rooms cache
    // map of roomId/room
    final LoadingCache<String, Map<String, Room>> roomsCache = Caffeine.newBuilder()
            .expireAfterAccess(Duration.ofMinutes(1))
            .build(this::loadRooms);

    @Inject
    @ConfigProperty(name = "rocketchat.url")
    String rocketchatUrl;

    @Inject
    RocketChatApiClient rocketChatApiClient;

    public Set<Channel> getRooms(Team team, RcTeam rcTeam) {
        HashMap<String, Room> rooms = new HashMap<>(roomsCache.get(CACHE_KEY));
        return rooms.entrySet().stream()
                .filter(e -> Objects.equals(e.getValue().getTeamId(), rcTeam.getId()))
                .map(e -> new Channel(
                        e.getKey(),
                        team,
                        e.getValue().getName(),
                        e.getValue().getTopic(),
                        getPrivacyType(e.getValue())
                ))
                .collect(Collectors.toSet());
    }

    public synchronized void checkRooms(Set<ChannelRcTeam> channelRcTeamList, boolean unarchiveIfArchived) {
        Map<String, Room> rooms = roomsCache.get(CACHE_KEY);

        Map<String, ChannelRcTeam> byId = channelRcTeamList.stream().collect(Collectors.toMap(
                ChannelRcTeam::getId,
                Function.identity()
        ));

        Map<String, ChannelRcTeam> missing = new HashMap<>(byId);
        missing.keySet().removeIf(rooms::containsKey);

        Map<String, ChannelRcTeam> existing = new HashMap<>(byId);
        existing.keySet().removeIf(id -> !rooms.containsKey(id));

        if (!missing.isEmpty()) {
            log.info("Creating {} missing rooms", missing.size());
            missing.values().stream().parallel().forEach(c -> rooms.put(c.getId(), createRoom(c)));
        }

        if (!existing.isEmpty()) {
            log.info("Checking {} existing rooms", existing.size());
            existing.values().stream().parallel().forEach(c -> checkRoom(c, rooms.get(c.getId()), unarchiveIfArchived));
        }
    }

    public Room getRoom(ChannelRcTeam channelRcTeam) {
        checkRooms(Set.of(channelRcTeam), false);
        return roomsCache.get(CACHE_KEY).get(channelRcTeam.getId());
    }

    public void joinRoom(Room room, Set<RcMember> rcMembers) {
        log.info("Adding {} users to {}", rcMembers.size(), room.getName());
        if (rcMembers.isEmpty()) {
            return;
        }

        setRoomMembers(room, rcMembers, false);
    }

    public void setRoomMembers(Room room, Set<RcMember> rcMembers) {
        setRoomMembers(room, rcMembers, true);
    }

    public void setRoomAdmins(Room room, Set<RcUser> admins) {
        Set<String> adminsUsernames = admins.stream().map(RcUser::getUsername).collect(Collectors.toSet());
        Set<RcMember> adminMembers = admins.stream().map(user -> new RcMember(user, Set.of(RoomRole.MODERATOR))).collect(Collectors.toSet());
        // add/set users as admins
        joinRoom(room, adminMembers);

        Map<String, RcMember> currentRoomMembers = getMembers(room);
        // is room member is not in admin list, ask to remove all roles to this member
        Set<RcMember> notAdmins = currentRoomMembers.values().stream()
                .map(RcMember::getUser)
                .filter(u -> !adminsUsernames.contains(u.getUsername()))
                .filter(u -> !u.getUsername().equals(UserService.GAZELLE_USERNAME))
                .map(u -> new RcMember(u, Set.of(RoomRole.MEMBER)))
                .collect(Collectors.toSet());
        updateRolesToUsersInRoom(room, notAdmins, currentRoomMembers);
    }

    public void leaveRoom(Room room, Set<RcUser> users) {
        log.info("Removing {} users from {}", users.size(), room.getName());
        if (users.isEmpty()) {
            return;
        }

        Map<String, RcMember> existingMembers = getMembers(room);

        log.info("Already {} users in {}", existingMembers.keySet(), room.getName());

        users.stream()
                .parallel()
                .filter(u -> existingMembers.containsKey(u.getUsername()))
                .forEach(user -> leaveRoom(room, user));

        // update cache
        users.forEach(u -> existingMembers.remove(u.getUsername()));
    }

    public void archive(ChannelRcTeam channelRcTeam) {
        Room room = getRoom(channelRcTeam);
        RoomChangeArchivationStatusRequest roomChangeArchivationStatusRequest = new RoomChangeArchivationStatusRequest(room.getId(), "archive");
        rocketChatApiClient.execute(roomChangeArchivationStatusRequest);
    }

    public void unarchive(ChannelRcTeam channelRcTeam) {
        Room room = getRoom(channelRcTeam);
        unarchive(room);
    }

    protected void unarchive(Room room) {
        RoomChangeArchivationStatusRequest roomChangeArchivationStatusRequest = new RoomChangeArchivationStatusRequest(room.getId(), "unarchive");
        rocketChatApiClient.execute(roomChangeArchivationStatusRequest);
    }

    public String getInvitationLink(ChannelRcTeam channel) {
        return invitationLinks.get(channel);
    }

    protected Map<String, Room> loadRooms(String unused) {
        Set<Room> allRoomsSet =
                new HashSet<>(
                        rocketChatApiClient.execute(new ChannelListRequest(0)).getChannels()
                );
        allRoomsSet.addAll(
                rocketChatApiClient.execute(new GroupListRequest(0)).getGroups()
        );

        // channel id is stored in customFields.id
        Map<String, Room> rooms = allRoomsSet.stream()
                .filter(r -> r.getCustomFields() != null)
                .filter(r -> r.getCustomFields().get("id") != null)
                .collect(Collectors.toMap(
                        r -> r.getCustomFields().get("id"),
                        Function.identity()
                ));
        return Collections.synchronizedMap(rooms);
    }

    protected void checkRoom(ChannelRcTeam channelRcTeam, Room room, boolean unarchiveIfArchived) {
        if (unarchiveIfArchived && Boolean.TRUE.equals(room.getArchived())) {
            unarchive(room);
        }

        String roomType = getRoomType(channelRcTeam.getPrivacyType());
        if (!room.getType().equals(roomType)) {
            setRoomType(room, roomType);
        }
        if (!channelRcTeam.getName().equals(room.getName())) {
            setRoomName(room, channelRcTeam.getName());
        }
        if (!Objects.equals(channelRcTeam.getTopic(), room.getTopic())) {
            setRoomTopic(room, channelRcTeam.getTopic());
        }
        RcTeam rcTeam = channelRcTeam.getRcTeam();
        String roomTeamId = room.getTeamId();
        if (rcTeam != null) {
            String rcTeamId = rcTeam.getId();
            if (!Objects.equals(rcTeamId, roomTeamId)) {
                if (roomTeamId != null) {
                    removeRoomFromTeam(room);
                }
                addRoomToTeam(rcTeam, room);
            }
        } else if (roomTeamId != null) {
            removeRoomFromTeam(room);
        }
    }

    protected Room createRoom(ChannelRcTeam channelRcTeam) {
        String name = channelRcTeam.getName();
        log.info("Creating room {}", name);
        Room room;
        Map<String, String> customFields = Map.of("id", channelRcTeam.getId());
        if (channelRcTeam.getPrivacyType() == PrivacyType.PUBLIC) {
            ChannelCreateResult channelCreateResult = rocketChatApiClient.execute(new ChannelCreateRequest(name, customFields));
            room = channelCreateResult.getChannel();
            // close channel for API user
            rocketChatApiClient.execute(new ChannelCloseRequest(room.getId()));
        } else {
            GroupCreateResult groupCreateResult = rocketChatApiClient.execute(new GroupCreateRequest(name, customFields));
            room = groupCreateResult.getGroup();
            // close group for API user
            rocketChatApiClient.execute(new GroupCloseRequest(room.getId()));
        }
        setRoomTopic(room, channelRcTeam.getTopic());
        RcTeam rcTeam = channelRcTeam.getRcTeam();
        if (rcTeam != null) {
            addRoomToTeam(rcTeam, room);
        }
        return room;
    }

    private void setRoomTopic(Room room, String topic) {
        if (topic != null) {
            rocketChatApiClient.execute(new ChangeRoomTopicRequest(room.getId(), topic));
            room.setTopic(topic);
        }
    }

    protected void setRoomName(Room room, String name) {
        rocketChatApiClient.execute(new ChangeRoomNameRequest(room.getId(), name));
        room.setName(name);
    }

    protected String getRoomType(PrivacyType privacyType) {
        if (privacyType == PrivacyType.PUBLIC) {
            return "c";
        } else {
            return "p";
        }
    }

    protected PrivacyType getPrivacyType(Room room) {
        if (room.getType().equals("c")) {
            return PrivacyType.PUBLIC;
        } else {
            return PrivacyType.PRIVATE;
        }
    }

    protected void setRoomType(Room room, String roomType) {
        rocketChatApiClient.execute(new ChangeRoomTypeRequest(room.getId(), roomType));
        room.setType(roomType);
    }

    protected void addRoomToTeam(RcTeam rcTeam, Room room) {
        AddRoomsRequest addRoomsRequest = new AddRoomsRequest(rcTeam.getName(), List.of(room.getId()));
        rocketChatApiClient.execute(addRoomsRequest);
        room.setTeamId(rcTeam.getId());
    }

    protected void removeRoomFromTeam(Room room) {
        RemoveRoomRequest removeRoomRequest = new RemoveRoomRequest(room.getTeamId(), room.getId());
        rocketChatApiClient.execute(removeRoomRequest);
        room.setTeamId(null);
    }

    protected String loadInvitationLink(ChannelRcTeam channel) {
        Room room = getRoom(channel);
        FindOrCreateInviteRequest findOrCreateInviteRequest = new FindOrCreateInviteRequest(room.getId(), 0, 0);
        InvitationLinkResult invitationLinkResult = rocketChatApiClient.execute(findOrCreateInviteRequest);
        return rocketchatUrl + "/invite/" + invitationLinkResult.getId();
    }

    private Map<String, RcMember> getMembers(Room room) {
        Request<RoomRolesResult> requestRoles;
        Request<MembersResult> requestMembers;
        if (room.getType().equals("c")) {
            requestRoles = new ChannelRolesRequest(room.getId());
            requestMembers = new ChannelMembersRequest(room.getId(), null, 0);
        } else {
            requestRoles = new GroupRolesRequest(room.getId());
            requestMembers = new GroupMembersRequest(room.getId(), null, 0);
        }

        RoomRolesResult roomRolesResult = rocketChatApiClient.execute(requestRoles);
        Map<String, RcMember> existingMembers = roomRolesResult.getRoles().stream()
                .collect(Collectors.toMap(r -> r.getUser().getUsername(), Function.identity()));

        MembersResult membersResult = rocketChatApiClient.execute(requestMembers);
        membersResult.getMembers()
                .stream()
                .filter(rcUser -> !existingMembers.containsKey(rcUser.getUsername()))
                .map(rcUser -> new RcMember(rcUser, Set.of()))
                .forEach(r -> existingMembers.put(r.getUser().getUsername(), r));

        return existingMembers;
    }

    protected void addUsersToRoom(Room room, Set<RcMember> members) {
        List<String> userIds = members.stream().map(m -> m.getUser().getId()).collect(Collectors.toList());
        List<String> userNames = members.stream().map(m -> m.getUser().getUsername()).collect(Collectors.toList());
        log.info("Adding {} to room {}", userNames, room.getName());
        if (room.getType().equals("c")) {
            rocketChatApiClient.execute(new ChannelInviteRequest(room.getId(), userIds));
        } else {
            rocketChatApiClient.execute(new GroupInviteRequest(room.getId(), userIds));
        }
    }

    protected void updateRolesToUsersInRoom(Room room, Set<RcMember> rcMembers, Map<String, RcMember> currentRoomMembers) {
        updateRoleToUsersInRoom(room, rcMembers, currentRoomMembers, RoomRole.MODERATOR, setModeratorConsumer(room), removeModeratorConsumer(room));
        updateRoleToUsersInRoom(room, rcMembers, currentRoomMembers, RoomRole.OWNER, setOwnerConsumer(room), removeOwnerConsumer(room));
        updateRoleToUsersInRoom(room, rcMembers, currentRoomMembers, RoomRole.LEADER, setLeaderConsumer(room), removeLeaderConsumer(room));
    }

    protected void updateRoleToUsersInRoom(Room room,
                                           Set<RcMember> members,
                                           Map<String, RcMember> currentRoomMembers,
                                           RoomRole role,
                                           Consumer<String> setRole,
                                           Consumer<String> removeRole) {
        members.stream()
                .parallel()
                .forEach(m -> {
                    boolean shouldHaveRole = m.getRoles().contains(role);
                    boolean hasRole = currentRoomMembers.get(m.getUser().getUsername()).getRoles().contains(role);
                    if (shouldHaveRole && !hasRole) {
                        log.info("Adding role {} to {} in room {}", role, m.getUser().getUsername(), room.getName());
                        setRole.accept(m.getUser().getId());
                    } else if (!shouldHaveRole && hasRole) {
                        log.info("Removing role {} to {} in room {}", role, m.getUser().getUsername(), room.getName());
                        removeRole.accept(m.getUser().getId());
                    }
                });
    }

    protected Consumer<String> setModeratorConsumer(Room room) {
        if (room.getType().equals("c")) {
            return userId -> rocketChatApiClient.execute(new ChannelAddModerator(room.getId(), userId));
        } else {
            return userId -> rocketChatApiClient.execute(new GroupAddModerator(room.getId(), userId));
        }
    }

    protected Consumer<String> setOwnerConsumer(Room room) {
        if (room.getType().equals("c")) {
            return userId -> rocketChatApiClient.execute(new ChannelAddOwner(room.getId(), userId));
        } else {
            return userId -> rocketChatApiClient.execute(new GroupAddOwner(room.getId(), userId));
        }
    }

    protected Consumer<String> setLeaderConsumer(Room room) {
        if (room.getType().equals("c")) {
            return userId -> rocketChatApiClient.execute(new ChannelAddLeader(room.getId(), userId));
        } else {
            return userId -> rocketChatApiClient.execute(new GroupAddLeader(room.getId(), userId));
        }
    }

    protected Consumer<String> removeModeratorConsumer(Room room) {
        if (room.getType().equals("c")) {
            return userId -> rocketChatApiClient.execute(new ChannelRemoveModerator(room.getId(), userId));
        } else {
            return userId -> rocketChatApiClient.execute(new GroupRemoveModerator(room.getId(), userId));
        }
    }

    protected Consumer<String> removeOwnerConsumer(Room room) {
        if (room.getType().equals("c")) {
            return userId -> rocketChatApiClient.execute(new ChannelRemoveOwner(room.getId(), userId));
        } else {
            return userId -> rocketChatApiClient.execute(new GroupRemoveOwner(room.getId(), userId));
        }
    }

    protected Consumer<String> removeLeaderConsumer(Room room) {
        if (room.getType().equals("c")) {
            return userId -> rocketChatApiClient.execute(new ChannelRemoveLeader(room.getId(), userId));
        } else {
            return userId -> rocketChatApiClient.execute(new GroupRemoveLeader(room.getId(), userId));
        }
    }

    protected void leaveRoom(Room room, RcUser user) {
        log.info("Removing {} from room {}", user.getUsername(), room.getName());
        if (room.getType().equals("c")) {
            rocketChatApiClient.execute(new ChannelKickRequest(room.getId(), user.getId()));
        } else {
            rocketChatApiClient.execute(new GroupKickRequest(room.getId(), user.getId()));
        }
    }

    protected void setRoomMembers(Room room, Set<RcMember> rcMembers, boolean strict) {
        Map<String, RcMember> currentRoomMembers = getMembers(room);

        log.info("Already {} users in {}", currentRoomMembers.size(), room.getName());

        // add new members
        Set<RcMember> missingMembers = rcMembers.stream()
                .filter(m -> !currentRoomMembers.containsKey(m.getUser().getUsername()))
                .collect(Collectors.toSet());
        if (!missingMembers.isEmpty()) {
            log.info("Adding {} users to {}", missingMembers.size(), room.getName());
            addUsersToRoom(room, missingMembers);
            // update room members with no role yet
            missingMembers.forEach(m -> currentRoomMembers.put(m.getUser().getUsername(), new RcMember(m.getUser(), Set.of(RoomRole.MEMBER))));
        }

        // update roles
        updateRolesToUsersInRoom(room, rcMembers, currentRoomMembers);

        if (strict) {
            Set<String> requiredMembersUsernames = rcMembers.stream()
                    .map(RcMember::getUser)
                    .map(RcUser::getUsername)
                    .collect(Collectors.toSet());
            Set<RcUser> usersToRemove = currentRoomMembers
                    .values().stream()
                    .map(RcMember::getUser)
                    .filter(user -> !requiredMembersUsernames.contains(user.getUsername()))
                    .filter(user -> !user.getUsername().equals(UserService.GAZELLE_USERNAME))
                    .collect(Collectors.toSet());
            leaveRoom(room, usersToRemove);
        }
    }

}
