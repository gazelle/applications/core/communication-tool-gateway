package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class TeamRemoveMemberRequest implements Request<Result> {
    String teamId;
    String userId;

    @Override
    public Class<Result> getResultClass() {
        return Result.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

    @Override
    public String getSuffix() {
        return "teams.removeMember";
    }
}
