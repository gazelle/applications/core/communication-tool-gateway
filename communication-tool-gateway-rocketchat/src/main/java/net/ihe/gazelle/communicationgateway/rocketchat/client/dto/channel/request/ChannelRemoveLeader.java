package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomUserRequest;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ChannelRemoveLeader extends RoomUserRequest {

    public ChannelRemoveLeader() {
        super();
    }

    public ChannelRemoveLeader(String roomId, String userId) {
        super(roomId, userId);
    }

    @Override
    public String getSuffix() {
        return "channels.removeLeader";
    }
}
