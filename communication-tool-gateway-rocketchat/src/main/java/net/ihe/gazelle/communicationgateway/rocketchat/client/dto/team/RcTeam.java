package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RcTeam {

    @JsonProperty("_id")
    String id;

    String name;

    int type;

    String roomId;

    String roomCustomFieldsId;

    String roomName;

    String roomTopic;

}
