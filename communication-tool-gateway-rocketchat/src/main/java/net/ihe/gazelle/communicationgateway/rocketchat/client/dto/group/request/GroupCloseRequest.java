package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomCloseRequest;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GroupCloseRequest extends RoomCloseRequest {

    public GroupCloseRequest() {
        super();
    }
    public GroupCloseRequest(String roomId) {
        super(roomId);
    }

    @Override
    public String getSuffix() {
        return "groups.close";
    }
}
