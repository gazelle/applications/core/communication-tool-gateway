package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.Room;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ChannelListResult extends Result {
    List<Room> channels;
    int count;
}
