package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomUserRequest;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ChannelAddOwner extends RoomUserRequest {

    public ChannelAddOwner() {
        super();
    }

    public ChannelAddOwner(String roomId, String userId) {
        super(roomId, userId);
    }

    @Override
    public String getSuffix() {
        return "channels.addOwner";
    }
}
