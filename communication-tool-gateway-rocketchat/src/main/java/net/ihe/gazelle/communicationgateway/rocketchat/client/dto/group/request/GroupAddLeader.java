package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomUserRequest;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GroupAddLeader extends RoomUserRequest {

    public GroupAddLeader() {
        super();
    }
    public GroupAddLeader(String roomId, String userId) {
        super(roomId, userId);
    }

    @Override
    public String getSuffix() {
        return "groups.addLeader";
    }
}
