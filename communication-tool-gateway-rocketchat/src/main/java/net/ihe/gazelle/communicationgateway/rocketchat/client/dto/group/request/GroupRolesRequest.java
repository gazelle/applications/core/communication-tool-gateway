package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomRolesRequest;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GroupRolesRequest extends RoomRolesRequest {
    public GroupRolesRequest() {
        super();
    }

    public GroupRolesRequest(String roomId) {
        super(roomId);
    }

    @Override
    public String getSuffix() {
        return "groups.roles";
    }
}
