package net.ihe.gazelle.communicationgateway.rocketchat;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import net.ihe.gazelle.communicationgateway.connector.dto.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatException;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.chat.request.PostMessageRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.Room;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.RcTeam;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcMember;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;
import net.ihe.gazelle.communicationgateway.rocketchat.internal.ChannelRcTeam;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Singleton
@NoArgsConstructor
@AllArgsConstructor
public class RocketChatConnector implements CommunicationToolConnector {

    @Inject
    RocketChatApiClient rocketChatApiClient;

    @Inject
    RoomService roomService;

    @Inject
    UserService userService;

    @Inject
    TeamService teamService;

    @Override
    public String getTarget() {
        return "rocketchat";
    }

    @Override
    public void healthCheck() {
        rocketChatApiClient.healthCheck();
    }

    @Override
    public void checkUsers(Set<User> users) {
        userService.getUsers(users);
    }

    @Override
    public void doJoinTeam(Team team, Set<Member> members) {
        RcTeam rcTeam = teamService.getTeam(team);
        Set<RcMember> rcMembers = asRcMembers(members);
        teamService.joinTeam(rcTeam, rcMembers);
    }

    @Override
    public void doSetTeamMembers(Team team, Set<Member> members) {
        RcTeam rcTeam = teamService.getTeam(team);
        Set<RcMember> rcMembers = asRcMembers(members);
        teamService.setTeamMembers(rcTeam, rcMembers);
    }

    @Override
    public void setTeamAdmins(Team team, Set<User> admins) {
        RcTeam rcTeam = teamService.getTeam(team);
        Set<RcUser> rcAdmins = userService.getUsers(admins);
        teamService.setTeamAdmins(rcTeam, rcAdmins);
    }

    @Override
    public Set<Channel> getChannels(Team team) {
        RcTeam rcTeam = teamService.getTeam(team);
        return roomService.getRooms(team, rcTeam);
    }

    @Override
    public void checkChannels(Set<Channel> channels) {
        Set<ChannelRcTeam> channelRcTeamList = channels.stream().map(this::asChannelRcTeam).collect(Collectors.toSet());
        roomService.checkRooms(channelRcTeamList, true);
    }

    @Override
    public void doJoinChannel(Channel channel, Set<Member> members) {
        Room room = roomService.getRoom(asChannelRcTeam(channel));
        Set<RcMember> rcMembers = asRcMembers(members);
        roomService.joinRoom(room, rcMembers);
    }

    @Override
    public void doSetChannelMembers(Channel channel, Set<Member> members) {
        Room room = roomService.getRoom(asChannelRcTeam(channel));
        Set<RcMember> rcMembers = asRcMembers(members);
        roomService.setRoomMembers(room, rcMembers);
    }

    @Override
    public void setChannelAdmins(Channel channel, Set<User> admins) {
        Room room = roomService.getRoom(asChannelRcTeam(channel));
        Set<RcUser> rcAdmins = userService.getUsers(admins);
        roomService.setRoomAdmins(room, rcAdmins);
    }

    @Override
    public void leaveChannel(Channel channel, Set<User> users) {
        Room room = roomService.getRoom(asChannelRcTeam(channel));
        Set<RcUser> rcUsers = userService.getUsers(users);
        roomService.leaveRoom(room, rcUsers);
    }

    @Override
    public String getInvitationLink(Channel channel) {
        return roomService.getInvitationLink(asChannelRcTeam(channel));
    }

    @Override
    public void archiveChannel(Channel channel) {

        roomService.archive(asChannelRcTeam(channel));
    }

    @Override
    public void unarchiveChannel(Channel channel) {

        roomService.unarchive(asChannelRcTeam(channel));
    }

    @Override
    public void sendMessage(Channel channel, String message) {
        Room room = roomService.getRoom(asChannelRcTeam(channel));
        PostMessageRequest postMessageRequest = new PostMessageRequest(room.getId(), message + "\n");
        rocketChatApiClient.execute(postMessageRequest);
    }

    private ChannelRcTeam asChannelRcTeam(Channel channel) {
        RcTeam rcTeam;
        if (channel.getTeam() != null) {
            rcTeam = teamService.getTeam(channel.getTeam());
        } else {
            rcTeam = null;
        }
        return new ChannelRcTeam(rcTeam, channel.getId(), channel.getName(), channel.getTopic(), channel.getPrivacyType());
    }

    private Set<RcMember> asRcMembers(Set<Member> members) {
        Set<User> users = members.stream().map(Member::getUser).collect(Collectors.toSet());
        Map<String, RcUser> rcUsers = userService.getUsers(users).stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(RcUser::getUsername, Function.identity()));

        return members.stream()
                .map(member -> {
                    RoomRole roomRole = asRoomRole(member.getRole());
                    return new RcMember(rcUsers.get(member.getUser().getUsername()), Set.of(roomRole));
                })
                .filter(rcMember -> rcMember.getUser() != null)
                .collect(Collectors.toSet());
    }

    protected RoomRole asRoomRole(Role role) {
        if (role == Role.USER) {
            return RoomRole.MEMBER;
        } else if (role == Role.ADMIN) {
            return RoomRole.MODERATOR;
        }
        throw new RocketChatException("Unsupported member type " + role);
    }

}
