package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class TeamData {

    String name;

    int type;

}
