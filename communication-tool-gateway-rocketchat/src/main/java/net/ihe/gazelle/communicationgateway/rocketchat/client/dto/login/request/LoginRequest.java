package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.login.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {
    String user;
    String password;
}
