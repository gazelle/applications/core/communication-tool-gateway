package net.ihe.gazelle.communicationgateway.rocketchat.client.base;

public enum Method {
    GET,
    POST
}
