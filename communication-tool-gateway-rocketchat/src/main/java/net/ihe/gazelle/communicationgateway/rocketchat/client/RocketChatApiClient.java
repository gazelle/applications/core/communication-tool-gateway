package net.ihe.gazelle.communicationgateway.rocketchat.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.login.request.LoginRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.login.result.Login;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.login.result.LoginResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.result.UserInfoResult;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Map;
import java.util.TreeMap;

import static java.util.stream.Collectors.joining;

@Slf4j
@Singleton
@NoArgsConstructor
@AllArgsConstructor
public class RocketChatApiClient {
    private static final HttpClient HTTP_CLIENT = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .build();

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private static final String FAILED_TO_PERFORM_QUERY = "Failed to perform query";

    private final LoadingCache<String, Login> tokens = Caffeine.newBuilder()
            .expireAfterAccess(Duration.ofMinutes(3))
            .build(this::loadToken);

    @Inject
    @ConfigProperty(name = "rocketchat.url")
    String rocketchatUrl;

    @Inject
    @ConfigProperty(name = "rocketchat.private.url", defaultValue = "none")
    String rocketchatPrivateUrl;

    @Inject
    @ConfigProperty(name = "rocketchat.user")
    String rocketchatUser;

    @Inject
    @ConfigProperty(name = "rocketchat.password")
    String rocketchatPassword;

    public void healthCheck() {
        UserInfoResult userInfoResult = executeGet("users.info", Map.of("username", rocketchatUser), UserInfoResult.class);
        if (!userInfoResult.isSuccess()) {
            log.error("Failed to retrieve user info for {}", rocketchatUser);
            throw new RocketChatException("Failed to retrieve user info for " + rocketchatUser);
        }
    }

    private Login loadToken(String notUsed) {
        LoginResult loginResult = executePost("login", new LoginRequest(rocketchatUser, rocketchatPassword), LoginResult.class);
        return loginResult.getData();
    }

    private String encodeValue(String value) {
        return URLEncoder.encode(value, StandardCharsets.UTF_8);
    }

    public <R extends Result> R execute(Request<R> request) {
        if (request.getMethod() == Method.GET) {
            String json = toJson(request);
            Map<String, String> parameters = readMap(json);
            return executeGet(request.getSuffix(), parameters, request.getResultClass());
        } else {
            return executePost(request.getSuffix(), request, request.getResultClass());
        }
    }

    protected <T extends Result> T executeGet(String suffix, Map<String, String> parameters, Class<T> resultType) {
        HttpRequest.Builder get = HttpRequest.newBuilder().GET();
        String encodedParameters = parameters.entrySet().stream()
                .map(e -> e.getKey() + "=" + encodeValue(e.getValue()))
                .collect(joining("&"));
        return execute("GET", suffix + "?" + encodedParameters, resultType, get, null);
    }

    protected <T extends Result> T executePost(String suffix, Object body, Class<T> resultType) {
        HttpRequest.Builder post = HttpRequest.newBuilder().POST(
                HttpRequest.BodyPublishers.ofString(toJson(body))
        );
        return execute("POST", suffix, resultType, post, body);
    }

    protected String toJson(Object body) {
        try {
            return OBJECT_MAPPER.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            log.error("Failed to serialize body");
            throw new RocketChatException("Failed to serialize body", e);
        }
    }

    protected Map<String, String> readMap(String json) {
        try {
            MapType mapType = TypeFactory.defaultInstance().constructMapType(TreeMap.class, String.class, String.class);
            return OBJECT_MAPPER.readValue(json, mapType);
        } catch (JsonProcessingException e) {
            log.error("Failed to deserialize {}", json);
            throw new RocketChatException("Failed to deserialize", e);
        }
    }

    protected <T> T execute(String method, String suffix, Class<T> resultType, HttpRequest.Builder builder, Object bodyLog) {
        log.info("Executing {} on {} with body {}", method, suffix, bodyLog);

        try {
            URI uri = new URI(getRocketchatApiUrl() + "/api/v1/" + suffix);
            log.info("Executing {} on {}", method, uri);
            builder.uri(uri)
                    .header("Content-Type", "application/json");
            if (!suffix.equals("login")) {
                Login login = tokens.get("token");
                if (login == null) {
                    throw new RocketChatException("Invalid credentials");
                }
                builder.header("X-User-Id", login.getUserId())
                        .header("X-Auth-Token", login.getAuthToken());
            }
            HttpRequest request = builder.build();
            HttpResponse<String> response = HTTP_CLIENT.send(request, HttpResponse.BodyHandlers.ofString());
            int code = response.statusCode();
            if (code != 200 && code != 400) {
                String responseString = response.body();
                log.error("Failed to execute {} on {} ({}) with body {} : {}", method, suffix, code, bodyLog, responseString);
                throw new RocketChatException(FAILED_TO_PERFORM_QUERY);
            }
            String responseString = response.body();
            if (responseString.isEmpty()) {
                throw new RocketChatException("No body content");
            }
            log.info("Result string : {}", responseString);
            T result = OBJECT_MAPPER.readValue(responseString, resultType);
            log.info("Result : {}", result);
            return result;
        } catch (InterruptedException e) {
            log.error("Failed to execute {} on {} with body {}", method, suffix, bodyLog);
            Thread.currentThread().interrupt();
            throw new RocketChatException(FAILED_TO_PERFORM_QUERY, e);
        } catch (Exception e) {
            log.error("Failed to execute {} on {} with body {}", method, suffix, bodyLog);
            throw new RocketChatException(FAILED_TO_PERFORM_QUERY, e);
        }
    }

    private String getRocketchatApiUrl() {
        if (rocketchatPrivateUrl.equals("none")) {
            return rocketchatUrl;
        }
        return rocketchatPrivateUrl;
    }

}
