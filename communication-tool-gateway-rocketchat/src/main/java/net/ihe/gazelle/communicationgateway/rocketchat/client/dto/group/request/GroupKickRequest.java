package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomUserRequest;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GroupKickRequest extends RoomUserRequest {

    public GroupKickRequest() {
        super();
    }

    public GroupKickRequest(String roomId, String userId) {
        super(roomId, userId);
    }

    @Override
    public String getSuffix() {
        return "groups.kick";
    }
}
