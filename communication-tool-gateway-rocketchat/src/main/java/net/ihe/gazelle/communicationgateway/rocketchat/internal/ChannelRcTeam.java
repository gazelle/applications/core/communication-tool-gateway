package net.ihe.gazelle.communicationgateway.rocketchat.internal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.connector.dto.PrivacyType;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.RcTeam;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class ChannelRcTeam {
    RcTeam rcTeam;
    String id;
    String name;
    String topic;
    PrivacyType privacyType;
}
