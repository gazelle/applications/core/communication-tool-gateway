package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomInviteRequest;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ChannelInviteRequest extends RoomInviteRequest {

    public ChannelInviteRequest() {
        super();
    }

    public ChannelInviteRequest(String roomId, List<String> userIds) {
        super(roomId, userIds);
    }

    @Override
    public String getSuffix() {
        return "channels.invite";
    }
}
