package net.ihe.gazelle.communicationgateway.rocketchat.client.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Result {
    boolean success;
}
