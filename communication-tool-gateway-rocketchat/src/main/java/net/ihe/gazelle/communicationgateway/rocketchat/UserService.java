package net.ihe.gazelle.communicationgateway.rocketchat;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.connector.dto.User;
import net.ihe.gazelle.communicationgateway.dto.OrganizationDTO;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request.UserCreateRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request.UserListRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.result.UserInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.result.UserListResult;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
@Singleton
@NoArgsConstructor
@AllArgsConstructor
public class UserService {

    public static final String GAZELLE_USERNAME = "gazelle";

    protected static final String CACHE_KEY = "";

    // all users cache
    // map of username/user
    final LoadingCache<String, Map<String, RcUser>> usersCache = Caffeine.newBuilder()
            .expireAfterAccess(Duration.ofMinutes(1))
            .build(this::loadAllUsers);

    private final Random random = new SecureRandom();

    @Inject
    RocketChatApiClient rocketChatApiClient;

    public Set<RcUser> getUsers(Set<User> users) {
        if (users.isEmpty()) {
            return Set.of();
        } else {
            return getUsersMultiple(users);
        }
    }


    protected RcUser createUser(User user) {
        log.info("creating user : {}", user.getUsername());
        RcUser rcUser;
        UserCreateRequest userCreateRequest = new UserCreateRequest(
                user.getUsername(),
                generateSecureRandomPassword(),
                user.getEmail(),
                user.getLastname(),
                true,
                true,
                false
        );
        UserInfoResult userInfoResult = rocketChatApiClient.execute(userCreateRequest);
        rcUser = userInfoResult.getUser();
        if (rcUser == null) {
            log.info("Failed to create {}", user.getUsername());
            // return RcUser with null id/username
            return new RcUser();
        }
        return rcUser;
    }

    protected synchronized Set<RcUser> getUsersMultiple(Set<User> users) {
        log.info("getting {} users", users.size());

        Map<String, RcUser> allUsers = usersCache.get(CACHE_KEY);

        Set<User> missing = users.stream()
                .filter(u -> !allUsers.containsKey(u.getUsername()))
                .collect(Collectors.toSet());

        if (!missing.isEmpty()) {
            log.info("Missing users : {}", missing.size());
            missing.stream()
                    .parallel()
                    .forEach(user -> allUsers.put(user.getUsername(), createUser(user)));
        } else {
            log.info("All users in cache");
        }

        return users.stream()
                .map(u -> allUsers.get(u.getUsername()))
                .filter(Objects::nonNull)
                .filter(rcUser -> rcUser.getId() != null)
                .collect(Collectors.toSet());
    }

    protected Map<String, RcUser> loadAllUsers(String unused) {
        Map<String, RcUser> allUsers;
        UserListResult userListResult = rocketChatApiClient.execute(new UserListRequest(0));
        allUsers = userListResult.getUsers().stream().collect(Collectors.toMap(RcUser::getUsername, Function.identity()));
        log.info("total users : {}", allUsers.size());
        return allUsers;
    }

    protected String generateSecureRandomPassword() {
        Stream<Character> pwdStream = Stream.concat(getRandomNumbers(), Stream.concat(getRandomSpecialChars(), Stream.concat(getRandomAlphabets(2, true), getRandomAlphabets(4, false))));
        List<Character> charList = pwdStream.collect(Collectors.toList());
        Collections.shuffle(charList);
        return charList.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }

    protected Stream<Character> getRandomAlphabets(int count, boolean upperCase) {
        IntStream characters;
        if (upperCase) {
            characters = random.ints(count, 65, 90);
        } else {
            characters = random.ints(count, 97, 122);
        }
        return characters.mapToObj(this::toChar);
    }

    protected Stream<Character> getRandomNumbers() {
        IntStream numbers = random.ints(2, 48, 57);
        return numbers.mapToObj(this::toChar);
    }

    protected Stream<Character> getRandomSpecialChars() {
        IntStream specialChars = random.ints(2, 33, 45);
        return specialChars.mapToObj(this::toChar);
    }

    private char toChar(int i) {
        return (char) i;
    }

}
