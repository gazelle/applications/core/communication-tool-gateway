package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RcUser {
    @JsonProperty("_id")
    String id;
    String username;
}
