package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.InvitationLinkResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class FindOrCreateInviteRequest implements Request<InvitationLinkResult> {

    String rid;

    int days;

    int maxUses;

    @Override
    public Class<InvitationLinkResult> getResultClass() {
        return InvitationLinkResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

    @Override
    public String getSuffix() {
        return "findOrCreateInvite";
    }
}
