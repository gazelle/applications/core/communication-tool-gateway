package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.result.TeamCreateResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class TeamCreateRequest implements Request<TeamCreateResult> {

    String name;

    int type;

    @Override
    public Class<TeamCreateResult> getResultClass() {
        return TeamCreateResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

    @Override
    public String getSuffix() {
        return "teams.create";
    }
}
