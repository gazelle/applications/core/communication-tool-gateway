package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomRolesRequest;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ChannelRolesRequest extends RoomRolesRequest {
    public ChannelRolesRequest() {
        super();
    }
    public ChannelRolesRequest(String roomId) {
        super(roomId);
    }

    @Override
    public String getSuffix() {
        return "channels.roles";
    }
}
