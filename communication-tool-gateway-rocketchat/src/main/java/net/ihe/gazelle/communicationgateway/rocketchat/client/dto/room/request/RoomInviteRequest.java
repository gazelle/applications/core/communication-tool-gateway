package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;

import java.util.List;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public abstract class RoomInviteRequest implements Request<Result> {
    String roomId;
    List<String> userIds;

    @Override
    public Class<Result> getResultClass() {
        return Result.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

}
