package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;

import java.util.Set;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class RcTeamMember {
    String userId;
    Set<RoomRole> roles;
}
