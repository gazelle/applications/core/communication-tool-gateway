package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team;

import lombok.Data;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;

import java.util.Set;

@Data
public class TeamMember {
    RcUser user;
    Set<RoomRole> roles;
}
