package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomMembersRequest;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GroupMembersRequest extends RoomMembersRequest {

    public GroupMembersRequest() {
        super();
    }
    public GroupMembersRequest(String roomId, String filter, Integer count) {
        super(roomId, filter, count);
    }

    @Override
    public String getSuffix() {
        return "groups.members";
    }
}
