package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class InvitationLinkResult extends Result {
    @JsonProperty("_id")
    String id;
}
