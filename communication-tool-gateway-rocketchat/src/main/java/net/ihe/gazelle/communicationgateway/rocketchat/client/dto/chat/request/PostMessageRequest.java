package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.chat.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostMessageRequest implements Request<Result> {

    String roomId;

    String text;

    @Override
    public Class<Result> getResultClass() {
        return Result.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

    @Override
    public String getSuffix() {
        return "chat.postMessage";
    }
}
