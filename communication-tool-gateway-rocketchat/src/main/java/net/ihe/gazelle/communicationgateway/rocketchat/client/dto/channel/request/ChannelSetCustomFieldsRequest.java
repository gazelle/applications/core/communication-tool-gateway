package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomSetCustomFieldsRequest;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ChannelSetCustomFieldsRequest extends RoomSetCustomFieldsRequest {
    public ChannelSetCustomFieldsRequest() {
        super();
    }

    public ChannelSetCustomFieldsRequest(String roomId, Map<String, String> customFields) {
        super(roomId, customFields);
    }

    @Override
    public String getSuffix() {
        return "channels.setCustomFields";
    }
}
