package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomInviteRequest;

import java.util.List;


@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GroupInviteRequest extends RoomInviteRequest {

    public GroupInviteRequest() {
        super();
    }
    public GroupInviteRequest(String roomId, List<String> userIds) {
        super(roomId, userIds);
    }

    @Override
    public String getSuffix() {
        return "groups.invite";
    }
}
