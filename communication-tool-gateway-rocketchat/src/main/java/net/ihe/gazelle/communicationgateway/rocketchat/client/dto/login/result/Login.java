package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.login.result;

import lombok.Data;

@Data
public class Login {
    String authToken;
    String userId;
}
