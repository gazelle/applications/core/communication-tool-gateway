package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.result.TeamListResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class TeamListRequest implements Request<TeamListResult> {

    int count;

    @Override
    public Class<TeamListResult> getResultClass() {
        return TeamListResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

    @Override
    public String getSuffix() {
        return "teams.list";
    }
}
