package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomMembersRequest;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ChannelMembersRequest extends RoomMembersRequest {

    public ChannelMembersRequest() {
        super();
    }
    public ChannelMembersRequest(String roomId, String filter, Integer count) {
        super(roomId, filter, count);
    }

    @Override
    public String getSuffix() {
        return "channels.members";
    }
}
