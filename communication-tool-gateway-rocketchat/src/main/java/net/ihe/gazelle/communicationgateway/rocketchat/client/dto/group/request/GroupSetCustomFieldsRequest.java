package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomSetCustomFieldsRequest;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GroupSetCustomFieldsRequest extends RoomSetCustomFieldsRequest {
    public GroupSetCustomFieldsRequest() {
        super();
    }

    public GroupSetCustomFieldsRequest(String roomId, Map<String, String> customFields) {
        super(roomId, customFields);
    }

    @Override
    public String getSuffix() {
        return "groups.setCustomFields";
    }
}
