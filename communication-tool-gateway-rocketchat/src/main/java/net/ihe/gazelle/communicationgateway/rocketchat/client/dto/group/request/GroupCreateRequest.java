package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupCreateResult;

import java.util.Map;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class GroupCreateRequest implements Request<GroupCreateResult> {

    String name;

    Map<String, String> customFields;

    @Override
    public Class<GroupCreateResult> getResultClass() {
        return GroupCreateResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

    @Override
    public String getSuffix() {
        return "groups.create";
    }
}
