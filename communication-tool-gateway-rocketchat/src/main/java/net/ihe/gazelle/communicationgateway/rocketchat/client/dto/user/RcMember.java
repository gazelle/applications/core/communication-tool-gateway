package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;

import java.util.Set;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class RcMember {

    @JsonProperty("u")
    RcUser user;

    Set<RoomRole> roles;

}
