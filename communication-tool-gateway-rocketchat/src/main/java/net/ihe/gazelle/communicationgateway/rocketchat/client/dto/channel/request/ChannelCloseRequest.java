package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomCloseRequest;


@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ChannelCloseRequest extends RoomCloseRequest {

    public ChannelCloseRequest() {
        super();
    }
    public ChannelCloseRequest(String roomId) {
        super(roomId);
    }

    @Override
    public String getSuffix() {
        return "channels.close";
    }
}
