package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelCreateResult;

import java.util.Map;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class ChannelCreateRequest implements Request<ChannelCreateResult> {

    String name;

    Map<String, String> customFields;

    @Override
    public Class<ChannelCreateResult> getResultClass() {
        return ChannelCreateResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

    @Override
    public String getSuffix() {
        return "channels.create";
    }
}
