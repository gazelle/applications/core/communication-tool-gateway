package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class Room {
    @JsonProperty("_id")
    String id;

    String name;

    String topic;

    @JsonProperty("t")
    String type;

    String teamId;

    Boolean archived;

    Map<String, String> customFields;
}
