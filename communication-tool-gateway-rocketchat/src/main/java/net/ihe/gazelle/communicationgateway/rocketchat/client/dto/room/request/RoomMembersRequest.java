package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.MembersResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public abstract class RoomMembersRequest implements Request<MembersResult> {

    String roomId;

    String filter;

    Integer count;

    @Override
    public Class<MembersResult> getResultClass() {
        return MembersResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

}
