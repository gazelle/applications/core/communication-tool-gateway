package net.ihe.gazelle.communicationgateway.rocketchat;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.connector.dto.PrivacyType;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.rocketchat.client.RocketChatApiClient;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.ChannelInfoRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.ChannelListRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.request.ChannelSetCustomFieldsRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.channel.result.ChannelInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupInfoRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupListRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.request.GroupSetCustomFieldsRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.group.result.GroupInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.Room;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.RoomRole;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.ChangeRoomNameRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.ChangeRoomTopicRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request.RoomInfoRequest;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.RoomInfoResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.RcTeam;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.TeamMember;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.request.*;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.result.TeamCreateResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.team.result.TeamMembersResult;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcMember;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcUser;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Duration;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Singleton
@NoArgsConstructor
@AllArgsConstructor
public class TeamService {

    public static final String CACHE_KEY = "";
    @Inject
    RocketChatApiClient rocketChatApiClient;
    @Inject
    RoomService roomService;

    // all teams cache
    // map of teamId/team
    final LoadingCache<String, Map<String, RcTeam>> teamsCache = Caffeine.newBuilder()
            .expireAfterAccess(Duration.ofMinutes(1))
            .build(this::loadTeams);

    public synchronized RcTeam getTeam(Team team) {
        Map<String, RcTeam> teams = teamsCache.get(CACHE_KEY);
        String id = team.getId();
        RcTeam rcTeam = teams.get(id);
        if (rcTeam == null) {
            rcTeam = createTeam(team);
            teams.put(id, rcTeam);
        } else {
            checkTeam(team, rcTeam);
        }
        return rcTeam;
    }

    public void joinTeam(RcTeam team, Set<RcMember> members) {
        if (members.isEmpty()) {
            return;
        }
        setTeamMembers(team, members, false);
    }

    public void setTeamMembers(RcTeam team, Set<RcMember> members) {
        setTeamMembers(team, members, true);
    }

    public void setTeamAdmins(RcTeam team, Set<RcUser> admins) {
        Room room = findRoom(team.getRoomId());
        roomService.setRoomAdmins(room, admins);
    }

    protected Map<String, RcTeam> loadTeams(String unused) {
        // retrieve all teams
        Set<RcTeam> allTeams =
                new HashSet<>(
                        rocketChatApiClient.execute(new TeamListRequest(0)).getTeams()
                );

        // retrieve all rooms
        Set<Room> allRoomsSet =
                new HashSet<>(
                        rocketChatApiClient.execute(new ChannelListRequest(0)).getChannels()
                );
        allRoomsSet.addAll(
                rocketChatApiClient.execute(new GroupListRequest(0)).getGroups()
        );
        // group rooms by id
        Map<String, Room> roomById = allRoomsSet.stream().collect(Collectors.toMap(Room::getId, Function.identity()));
        allTeams.forEach(rcTeam -> {
            // get team main room
            Room teamRoom = roomById.get(rcTeam.getRoomId());
            // team id is stored in teamRoom.customFields.id
            Map<String, String> customFields = teamRoom.getCustomFields();
            if (customFields != null) {
                rcTeam.setRoomCustomFieldsId(customFields.get("id"));
            }
            rcTeam.setRoomName(teamRoom.getName());
            rcTeam.setRoomTopic(teamRoom.getTopic());
        });

        Map<String, RcTeam> teamsMap = allTeams.stream()
                .filter(r -> r.getRoomCustomFieldsId() != null)
                .collect(Collectors.toMap(
                        RcTeam::getRoomCustomFieldsId,
                        Function.identity()
                ));
        return Collections.synchronizedMap(teamsMap);
    }

    protected void checkTeam(Team team, RcTeam rcTeam) {
        log.info("Checking team {}", team.getName());
        int privacyType = getPrivacyType(team.getPrivacyType());
        if (!Objects.equals(rcTeam.getName(), team.getName()) || rcTeam.getType() != privacyType) {
            UpdateTeamRequest updateTeamRequest = new UpdateTeamRequest(rcTeam.getId(), new TeamData(team.getName(), privacyType));
            rocketChatApiClient.execute(updateTeamRequest);
            rcTeam.setType(privacyType);
            rcTeam.setName(team.getName());
        }
        if (!team.getName().equals(rcTeam.getRoomName())) {
            changeTeamRoomName(team, rcTeam);
        }
        if (!Objects.equals(team.getTopic(), rcTeam.getRoomTopic())) {
            changeTeamRoomTopic(team, rcTeam);
        }
    }

    protected RcTeam createTeam(Team team) {
        log.info("Creating team {}", team.getName());
        TeamCreateRequest teamCreateRequest = new TeamCreateRequest(team.getName(), getPrivacyType(team.getPrivacyType()));
        TeamCreateResult teamCreateResult = rocketChatApiClient.execute(teamCreateRequest);
        RcTeam rcTeam = teamCreateResult.getTeam();

        Room room = findRoom(rcTeam.getRoomId());
        if (room.getType().equals("c")) {
            rocketChatApiClient.execute(new ChannelSetCustomFieldsRequest(room.getId(), Map.of("id", team.getId())));
        } else {
            rocketChatApiClient.execute(new GroupSetCustomFieldsRequest(room.getId(), Map.of("id", team.getId())));
        }

        rcTeam.setRoomCustomFieldsId(team.getId());
        rcTeam.setRoomName(room.getName());
        changeTeamRoomTopic(team, rcTeam);

        return rcTeam;
    }

    protected void changeTeamRoomName(Team team, RcTeam rcTeam) {
        ChangeRoomNameRequest changeRoomNameRequest = new ChangeRoomNameRequest(rcTeam.getRoomId(), team.getName());
        rocketChatApiClient.execute(changeRoomNameRequest);
        rcTeam.setRoomName(team.getName());
    }

    protected void changeTeamRoomTopic(Team team, RcTeam rcTeam) {
        if (team.getTopic() != null) {
            ChangeRoomTopicRequest changeRoomTopicRequest = new ChangeRoomTopicRequest(rcTeam.getRoomId(), team.getTopic());
            rocketChatApiClient.execute(changeRoomTopicRequest);
            rcTeam.setRoomTopic(team.getTopic());
        }
    }

    protected Room findRoom(String roomId) {
        RoomInfoResult roomInfoResult = rocketChatApiClient.execute(new RoomInfoRequest(roomId));

        Room room = roomInfoResult.getRoom();

        if (room == null) {
            GroupInfoResult groupInfoResult = rocketChatApiClient.execute(new GroupInfoRequest(roomId));
            room = groupInfoResult.getGroup();
        }

        if (room == null) {
            ChannelInfoResult channelInfoResult = rocketChatApiClient.execute(new ChannelInfoRequest(roomId));
            room = channelInfoResult.getChannel();
        }
        return room;
    }

    protected int getPrivacyType(PrivacyType type) {
        if (type == PrivacyType.PUBLIC) {
            return 0;
        }
        return 1;
    }

    protected void setTeamMembers(RcTeam team, Set<RcMember> members, boolean strict) {
        log.info("Adding {} members to {}", members.size(), team.getName());

        TeamMembersResult teamMembersResult = rocketChatApiClient.execute(new TeamMembersRequest(team.getId(), null, 0));
        Map<String, TeamMember> existingMembers = teamMembersResult.getMembers().stream()
                .collect(Collectors.toMap(m -> m.getUser().getUsername(), Function.identity()));
        log.info("Already {} members in {}", existingMembers.size(), team.getName());

        List<RcTeamMember> newMembersList = members.stream()
                .filter(member -> !existingMembers.containsKey(member.getUser().getUsername()))
                .map(member -> new RcTeamMember(member.getUser().getId(), Set.of(RoomRole.MEMBER)))
                .collect(Collectors.toList());
        if (!newMembersList.isEmpty()) {
            log.info("Adding {} users to {}", newMembersList.size(), team.getName());
            TeamAddMembersRequest teamAddMembersRequest = new TeamAddMembersRequest(team.getId(), newMembersList);
            rocketChatApiClient.execute(teamAddMembersRequest);
        }

        if (strict) {
            // remove team members
            Set<String> requiredMembersUsernames = members.stream()
                    .map(RcMember::getUser)
                    .map(RcUser::getUsername)
                    .collect(Collectors.toSet());
            Set<TeamRemoveMemberRequest> removeMemberRequests = existingMembers.values().stream()
                    .map(TeamMember::getUser)
                    .filter(rcUser -> !rcUser.getUsername().equals(UserService.GAZELLE_USERNAME))
                    .filter(user -> !requiredMembersUsernames.contains(user.getUsername()))
                    .map(RcUser::getId)
                    .map(userId -> new TeamRemoveMemberRequest(team.getId(), userId))
                    .collect(Collectors.toSet());
            log.info("Removing {} users from {}", removeMemberRequests.size(), team.getName());
            removeMemberRequests
                    .stream().parallel()
                    .forEach(rocketChatApiClient::execute);
        }

        Room room = findRoom(team.getRoomId());
        if (strict) {
            roomService.setRoomMembers(room, members);
        } else {
            roomService.joinRoom(room, members);
        }
    }

}
