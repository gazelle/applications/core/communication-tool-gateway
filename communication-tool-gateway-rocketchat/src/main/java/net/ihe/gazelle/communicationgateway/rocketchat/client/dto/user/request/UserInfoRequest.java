package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.result.UserInfoResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UserInfoRequest implements Request<UserInfoResult> {
    String username;

    @Override
    public Class<UserInfoResult> getResultClass() {
        return UserInfoResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

    @Override
    public String getSuffix() {
        return "users.info";
    }
}
