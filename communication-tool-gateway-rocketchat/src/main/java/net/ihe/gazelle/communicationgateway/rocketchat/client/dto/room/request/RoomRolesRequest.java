package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result.RoomRolesResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public abstract class RoomRolesRequest implements Request<RoomRolesResult> {
    String roomId;

    @Override
    public Class<RoomRolesResult> getResultClass() {
        return RoomRolesResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

}
