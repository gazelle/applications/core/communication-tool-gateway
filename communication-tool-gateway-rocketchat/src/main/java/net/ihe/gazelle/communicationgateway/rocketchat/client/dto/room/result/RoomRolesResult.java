package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.result;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.RcMember;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class RoomRolesResult extends Result {
    List<RcMember> roles;
}
