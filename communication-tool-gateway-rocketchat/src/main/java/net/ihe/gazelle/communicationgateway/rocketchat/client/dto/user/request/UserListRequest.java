package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.result.UserListResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UserListRequest implements Request<UserListResult> {
    int count;

    @Override
    public Class<UserListResult> getResultClass() {
        return UserListResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.GET;
    }

    @Override
    public String getSuffix() {
        return "users.list";
    }
}
