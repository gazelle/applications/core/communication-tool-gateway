package net.ihe.gazelle.communicationgateway.rocketchat.client.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface Request<R extends Result> {
    @JsonIgnore
    Class<R> getResultClass();

    @JsonIgnore
    Method getMethod();

    @JsonIgnore
    String getSuffix();
}
