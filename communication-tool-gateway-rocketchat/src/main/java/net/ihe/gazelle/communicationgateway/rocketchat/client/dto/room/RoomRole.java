package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum RoomRole {

    @JsonProperty("leader")
    LEADER,

    @JsonProperty("owner")
    OWNER,

    @JsonProperty("member")
    MEMBER,

    @JsonProperty("moderator")
    MODERATOR;

}
