package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.room.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Result;

@Data
@RequiredArgsConstructor
public class ChangeRoomTopicRequest implements Request<Result> {
    String rid;
    String roomTopic;

    public ChangeRoomTopicRequest(String rid, String roomTopic) {
        this.rid = rid;
        if (roomTopic == null) {
            this.roomTopic = "";
        } else {
            this.roomTopic = roomTopic;
        }
    }

    @Override
    public Class<Result> getResultClass() {
        return Result.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

    @Override
    public String getSuffix() {
        return "rooms.saveRoomSettings";
    }
}
