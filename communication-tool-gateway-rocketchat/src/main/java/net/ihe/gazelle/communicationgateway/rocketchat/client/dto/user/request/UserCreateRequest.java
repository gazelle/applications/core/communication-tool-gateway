package net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Method;
import net.ihe.gazelle.communicationgateway.rocketchat.client.base.Request;
import net.ihe.gazelle.communicationgateway.rocketchat.client.dto.user.result.UserInfoResult;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UserCreateRequest implements Request<UserInfoResult> {

    String username;
    String password;
    String email;
    String name;
    boolean active;
    boolean verified;
    boolean sendWelcomeEmail;

    @Override
    public Class<UserInfoResult> getResultClass() {
        return UserInfoResult.class;
    }

    @Override
    public Method getMethod() {
        return Method.POST;
    }

    @Override
    public String getSuffix() {
        return "users.create";
    }
}
