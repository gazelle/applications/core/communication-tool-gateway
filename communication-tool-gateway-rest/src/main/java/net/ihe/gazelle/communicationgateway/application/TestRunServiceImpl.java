package net.ihe.gazelle.communicationgateway.application;

import net.ihe.gazelle.communicationgateway.application.command.JoinChannel;
import net.ihe.gazelle.communicationgateway.application.command.Update;
import net.ihe.gazelle.communicationgateway.application.service.*;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;
import net.ihe.gazelle.communicationgateway.connector.dto.Role;
import net.ihe.gazelle.communicationgateway.connector.dto.User;
import net.ihe.gazelle.communicationgateway.dto.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.MessageFormat;
import java.util.*;

@Singleton
public class TestRunServiceImpl implements TestRunService {

    private final ResourceBundle messages = ResourceBundle.getBundle("commGateway", Locale.US);

    @Inject
    ChannelService channelService;

    @Inject
    CommunicationToolConnectorLocator communicationToolConnectorLocator;

    @Inject
    EntityCommandService entityCommandService;

    @Inject
    TestRunLinkService testRunLinkService;

    @Inject
    CommandService commandService;

    protected CommunicationToolConnector getConnector() {
        return communicationToolConnectorLocator.getConnector();
    }

    @Override
    public void createTestRunChannel(TestingSessionDTO testingSession,
                                     TestRunDTO testRun,
                                     UserDTO creatorDTO) {

        prepareTestRun(testingSession, testRun, creatorDTO);
        sendMessages(testingSession, testRun);
    }

    @Override
    public void joinTestRunChannel(TestingSessionDTO testingSession,
                                   TestRunDTO testRun,
                                   UserDTO userDTO,
                                   TestRunRoleDTO roleDTO) {
        prepareTestRun(testingSession, testRun, null);

        Channel channel = channelService.getTestRunChannel(testingSession, testRun);
        Role role;
        if (roleDTO == TestRunRoleDTO.ADMIN) {
            role = Role.ADMIN;
        } else {
            role = Role.USER;
        }
        Member member = new Member(entityCommandService.asUser(userDTO), role);
        getConnector().joinChannel(channel, Set.of(member));
    }

    @Override
    public void leaveTestRunChannel(TestingSessionDTO testingSession, TestRunDTO testRun, UserDTO userDTO) {
        prepareTestRun(testingSession, testRun, null);
        Channel channel = channelService.getTestRunChannel(testingSession, testRun);
        getConnector().leaveChannel(channel, Set.of(entityCommandService.asUser(userDTO)));
    }

    @Override
    public String getInvitationLink(TestingSessionDTO testingSession,
                                    TestRunDTO testRun) {
        prepareTestRun(testingSession, testRun);

        Channel channel = channelService.getTestRunChannel(testingSession, testRun);
        return getConnector().getInvitationLink(channel);
    }

    @Override
    public void archiveTestRunChannel(TestingSessionDTO testingSession,
                                      TestRunDTO testRun,
                                      String testStatus,
                                      String lastTestStatus) {
        prepareTestRun(testingSession, testRun);

        Channel channel = channelService.getTestRunChannel(testingSession, testRun);
        getConnector().archiveChannel(channel);
    }

    @Override
    public void unarchiveTestRun(TestingSessionDTO testingSession,
                                 TestRunDTO testRun,
                                 String testStatus,
                                 String lastTestStatus) {
        prepareTestRun(testingSession, testRun);

        Channel channel = channelService.getTestRunChannel(testingSession, testRun);
        getConnector().unarchiveChannel(channel);
    }

    @Override
    public void sendTestRunMessage(TestingSessionDTO testingSession,
                                   TestRunDTO testRun,
                                   String message) {
        prepareTestRun(testingSession, testRun);

        Channel channel = channelService.getTestRunChannel(testingSession, testRun);
        getConnector().sendMessage(channel, message);
    }

    private void prepareTestRun(TestingSessionDTO testingSession, TestRunDTO testRun) {
        prepareTestRun(testingSession, testRun, null);
    }

    private void prepareTestRun(TestingSessionDTO testingSession, TestRunDTO testRun, UserDTO creatorDTO) {
        Set<Update> updates = new HashSet<>(entityCommandService.getTestingSessionUpdates(testingSession));
        updates.addAll(entityCommandService.getTestRunUpdates(testingSession, testRun));

        if (creatorDTO != null) {
            User user = entityCommandService.asUser(creatorDTO);
            updates.addAll(entityCommandService.getUserUpdates(user));
            Channel testRunChannel = channelService.getTestRunChannel(testingSession, testRun);
            updates.add(new JoinChannel(testRunChannel, new Member(user, Role.ADMIN)));
        }

        Set<SystemDTO> systems = testRun.getSystems();
        if (systems != null) {
            systems.stream()
                    .map(SystemDTO::getOrganizations)
                    .filter(Objects::nonNull)
                    .flatMap(Set::stream)
                    .distinct()
                    .flatMap(o -> entityCommandService.getOrganizationUpdates(testingSession, o).stream())
                    .forEach(updates::add);
        }

        commandService.perform(updates);
    }

    private void sendMessages(TestingSessionDTO testingSession, TestRunDTO testRun) {
        TestDTO test = testRun.getTest();
        String testName = test.getKeyword();
        Set<SystemDTO> systemParticipants = testRun.getSystems();

        Channel testRunChannel = channelService.getTestRunChannel(testingSession, testRun);
        String testRunChannelName = testRunChannel.getName();
        String invitationLink = getConnector().getInvitationLink(testRunChannel);
        String tmTestRunUrl = testRunLinkService.getTmTestRunUrl(testRun);

        Map<OrganizationDTO, Set<String>> systemsPerOrganization = new HashMap<>();
        for (SystemDTO system : systemParticipants) {
            for (OrganizationDTO organization : system.getOrganizations()) {
                Set<String> systems = systemsPerOrganization.computeIfAbsent(organization, o -> new TreeSet<>());
                systems.add(system.getName());
            }
        }

        for (Map.Entry<OrganizationDTO, Set<String>> entry : systemsPerOrganization.entrySet()) {
            OrganizationDTO organization = entry.getKey();
            Set<String> systemKeywords = entry.getValue();
            String systems = String.join(", ", systemKeywords);

            Channel organizationPrivateChannel = channelService.getOrganizationPrivateChannel(testingSession, organization);
            String notificationContent = MessageFormat.format(
                    this.messages.getString("test.run.notification.organisation"),
                    testRunChannelName,
                    tmTestRunUrl,
                    invitationLink,
                    testName,
                    systems
            );
            getConnector().sendMessage(organizationPrivateChannel, notificationContent);

            Channel organizationPublicChannel = channelService.getOrganizationPublicChannel(testingSession, organization);
            String organizationPublicChannelLink = getConnector().getInvitationLink(organizationPublicChannel);
            String organizationChannelLinkMessage = MessageFormat.format(
                    this.messages.getString("test.run.notification.testRun"),
                    organization.getName(),
                    systems,
                    organizationPublicChannelLink
            );
            getConnector().sendMessage(testRunChannel, organizationChannelLinkMessage);
        }
    }

}
