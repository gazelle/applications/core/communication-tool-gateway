package net.ihe.gazelle.communicationgateway.interlay;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.application.TestRunService;
import net.ihe.gazelle.communicationgateway.client.TestRunClient;
import net.ihe.gazelle.communicationgateway.dto.payload.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Path;

@Path("testRun") // same as in TestRunClient
@Singleton
@Slf4j
public class TestRunController implements TestRunClient {

    @Inject
    TestRunService testRunService;

    @Override
    public void createTestRunChannel(CreateTestRunChannel createTestRunChannel) {
        testRunService.createTestRunChannel(createTestRunChannel.getTestingSession(),
                createTestRunChannel.getTestRun(),
                createTestRunChannel.getCreator());
    }

    @Override
    public void joinTestRunChannel(JoinTestRunChannel joinTestRunChannel) {
        testRunService.joinTestRunChannel(joinTestRunChannel.getTestingSession(),
                joinTestRunChannel.getTestRun(),
                joinTestRunChannel.getUser(),
                joinTestRunChannel.getRole());
    }

    @Override
    public void leaveTestRunChannel(LeaveTestRunChannel leaveTestRunChannel) {
        testRunService.leaveTestRunChannel(leaveTestRunChannel.getTestingSession(),
                leaveTestRunChannel.getTestRun(),
                leaveTestRunChannel.getUser());
    }

    @Override
    public String getInvitationLink(TestRunInvitationLink testRunInvitationLink) {
        return testRunService.getInvitationLink(testRunInvitationLink.getTestingSession(),
                testRunInvitationLink.getTestRun());
    }

    @Override
    public void archiveTestRunChannel(ArchiveTestRunChannel archiveTestRunChannel) {
        testRunService.archiveTestRunChannel(archiveTestRunChannel.getTestingSession(),
                archiveTestRunChannel.getTestRun(),
                archiveTestRunChannel.getTestStatus(),
                archiveTestRunChannel.getLastTestStatus());
    }

    @Override
    public void unarchiveTestRun(UnarchiveTestRunChannel unarchiveTestRunChannel) {
        testRunService.unarchiveTestRun(unarchiveTestRunChannel.getTestingSession(),
                unarchiveTestRunChannel.getTestRun(),
                unarchiveTestRunChannel.getTestStatus(),
                unarchiveTestRunChannel.getLastTestStatus());
    }

    @Override
    public void sendTestRunMessage(TestRunMessage testRunMessage) {
        testRunService.sendTestRunMessage(testRunMessage.getTestingSession(),
                testRunMessage.getTestRun(),
                testRunMessage.getMessage());
    }

}
