package net.ihe.gazelle.communicationgateway.application.command;

import lombok.Value;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;

import java.util.Set;

@Value
public class SetChannelMembers implements Update {
    Channel channel;
    Set<Member> members;
}
