package net.ihe.gazelle.communicationgateway.application;

import net.ihe.gazelle.communicationgateway.dto.OrganizationDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;

import java.util.List;

public interface ProvisioningService {
    void updateTestingSessions(List<TestingSessionDTO> testingSessions);

    void updateOrganizations(TestingSessionDTO testingSession,
                             List<OrganizationDTO> organizations);
}
