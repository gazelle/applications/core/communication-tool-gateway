package net.ihe.gazelle.communicationgateway.application.health;

import net.ihe.gazelle.communicationgateway.application.service.CommunicationToolConnectorLocator;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class HealthServiceImpl implements HealthService {
    @Inject
    CommunicationToolConnectorLocator communicationToolConnectorLocator;

    @Override
    public void healthCheck() {
        communicationToolConnectorLocator.getConnector().healthCheck();
    }
}
