package net.ihe.gazelle.communicationgateway.application;

public class CommunicationToolGatewayException extends RuntimeException {
    public CommunicationToolGatewayException(String message) {
        super(message);
    }
}
