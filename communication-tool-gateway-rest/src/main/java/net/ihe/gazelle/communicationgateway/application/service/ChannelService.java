package net.ihe.gazelle.communicationgateway.application.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.PrivacyType;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.dto.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Set;

@Singleton
@NoArgsConstructor
@AllArgsConstructor
public class ChannelService {

    public static final String ORG_PREFIX = "org_";
    public static final String PROFILE_PREFIX = "prof_";
    @Inject
    TestRunLinkService testRunLinkService;

    public Team getTeam(TestingSessionDTO testingSession) {
        String id = "ts_" + testingSession.getId();
        String name = testingSession.getKeyword();
        String topic = "Team for \"" + testingSession.getDescription() + "\"";
        return new Team(id,
                name,
                topic,
                PrivacyType.PRIVATE);
    }

    public Channel getTestRunChannel(TestingSessionDTO testingSession, TestRunDTO testRun) {
        Team team = getTeam(testingSession);
        String id = "tr_" + testRun.getId();
        String channelName = "TR " + testRun.getId() + " " + testRun.getTest().getKeyword();
        String topic;
        Set<SystemDTO> systems = testRun.getSystems();
        if (systems != null) {
            topic = "Private topic for test run " + testRun.getId()
                    + " - " + testRun.getTest().getName()
                    + " - " + testRunLinkService.getTmTestRunUrl(testRun);
        } else {
            topic = null;
        }
        return new Channel(id,
                team,
                channelName,
                topic,
                PrivacyType.PRIVATE);
    }

    public Channel getOrganizationPublicChannel(TestingSessionDTO testingSession, OrganizationDTO organization) {
        Team team = getTeam(testingSession);
        String id = "org_" + organization.getId() + "_public_" + testingSession.getId();
        String channelName = organization.getKeyword() + " - Public - " + team.getName();
        String topic = "Public channel for organization " + organization.getName();
        return new Channel(id,
                team,
                channelName,
                topic,
                PrivacyType.PUBLIC);
    }

    public Channel getOrganizationPrivateChannel(TestingSessionDTO testingSession, OrganizationDTO organization) {
        Team team = getTeam(testingSession);
        String id = "org_" + organization.getId() + "_private_" + testingSession.getId();
        String channelName = organization.getKeyword() + " - Private - " + team.getName();
        String topic = "Private channel for organization " + organization.getName();
        return new Channel(id,
                team,
                channelName,
                topic,
                PrivacyType.PRIVATE);
    }

    public boolean isOrganizationChannel(Channel channel) {
        return channel.getId() != null && channel.getId().startsWith(ORG_PREFIX);
    }

    public Channel getMonitorsChannel(TestingSessionDTO testingSession) {
        Team team = getTeam(testingSession);
        String id = "m_" + testingSession.getId();
        String channelName = "Monitors - " + team.getName();
        String topic = "Private channel for monitors";
        return new Channel(id,
                team,
                channelName,
                topic,
                PrivacyType.PRIVATE);
    }

    public Channel getProfileChannel(TestingSessionDTO testingSession, ProfileDTO profile) {
        Team team = getTeam(testingSession);
        String id = PROFILE_PREFIX + profile.getId() + "_" + testingSession.getId();
        String channelName = "Profile " + profile.getKeyword() + " - " + team.getName();
        String topic = "Public channel for profile " + profile.getName();
        return new Channel(id,
                team,
                channelName,
                topic,
                PrivacyType.PUBLIC);
    }

    public boolean isProfileChannel(Channel channel) {
        return channel.getId() != null && channel.getId().startsWith(PROFILE_PREFIX);
    }
}
