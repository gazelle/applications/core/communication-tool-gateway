package net.ihe.gazelle.communicationgateway.application.command;

import lombok.Value;
import net.ihe.gazelle.communicationgateway.connector.dto.User;

@Value
public class CheckUser implements Update {
    User user;
}
