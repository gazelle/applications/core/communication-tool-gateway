package net.ihe.gazelle.communicationgateway.application.service;

import net.ihe.gazelle.communicationgateway.application.command.*;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import net.ihe.gazelle.communicationgateway.connector.dto.*;
import net.ihe.gazelle.communicationgateway.dto.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashSet;
import java.util.Set;

@Singleton
public class EntityCommandService {

    @Inject
    ChannelService channelService;

    @Inject
    CommunicationToolConnectorLocator communicationToolConnectorLocator;

    protected CommunicationToolConnector getConnector() {
        return communicationToolConnectorLocator.getConnector();
    }

    public Set<Update> getTestingSessionUpdates(TestingSessionDTO testingSession) {
        Set<Update> updates = new HashSet<>();

        Team team = channelService.getTeam(testingSession);
        // get all channels with a GCT id
        Set<Channel> channels = getConnector().getChannels(team);

        // channels that should be archived
        Set<Channel> toArchiveChannels = new HashSet<>();
        for (Channel channel : channels) {
            if (channelService.isOrganizationChannel(channel) || channelService.isProfileChannel(channel)) {
                // all organization and profile channels are candidate
                // we do not include all channels (test runs, ...)
                toArchiveChannels.add(channel);
            }
        }

        if (testingSession.getOrganizations() != null) {
            for (OrganizationDTO organization : testingSession.getOrganizations()) {
                updates.addAll(getOrganizationUpdates(testingSession, organization));
            }
        }

        Channel monitorsChannel = channelService.getMonitorsChannel(testingSession);
        updates.add(new CheckChannel(monitorsChannel));

        if (testingSession.getUsers() != null) {
            getTestingSessionUsersUpdates(testingSession, updates, monitorsChannel);
        }

        if (testingSession.getProfiles() != null) {
            for (ProfileDTO profile : testingSession.getProfiles()) {
                Channel profileChannel = channelService.getProfileChannel(testingSession, profile);
                updates.add(new CheckChannel(profileChannel));
            }
        }

        for (Update update : updates) {
            if (update instanceof CheckChannel) {
                // do not archive channel
                toArchiveChannels.remove(((CheckChannel) update).getChannel());
            }
        }
        for (Channel toArchiveChannel : toArchiveChannels) {
            updates.add(new ArchiveChannel(toArchiveChannel));
        }

        return updates;
    }

    private void getTestingSessionUsersUpdates(TestingSessionDTO testingSession, Set<Update> updates, Channel monitorsChannel) {
        Team team = channelService.getTeam(testingSession);
        Set<Member> teamMembers = new HashSet<>();
        Set<User> teamAdmins = new HashSet<>();
        Set<Member> monitorMembers = new HashSet<>();
        for (TestingSessionUserDTO testingSessionUser : testingSession.getUsers()) {
            UserDTO userDTO = testingSessionUser.getUser();
            User user = asUser(userDTO);
            updates.addAll(getUserUpdates(user));
            if (testingSessionUser.getRoles().contains(TestingSessionRoleDTO.MONITOR)) {
                monitorMembers.add(new Member(user, Role.USER));
            }
            if (testingSessionUser.getRoles().contains(TestingSessionRoleDTO.TESTING_SESSION_ADMIN)) {
                teamMembers.add(new Member(user, Role.ADMIN));
                teamAdmins.add(user);
                monitorMembers.add(new Member(user, Role.ADMIN));
            } else {
                teamMembers.add(new Member(user, Role.USER));
            }
        }
        // add new users to team
        // do not remove users (not using SetTeamMembers)
        for (Member member : teamMembers) {
            updates.add(new JoinTeam(team, member));
        }
        // force list of admins of team (remove moderator role to unauthorized users)
        updates.add(new SetTeamAdmins(team, teamAdmins));
        // force monitors channel members, remove unauthorized users
        updates.add(new SetChannelMembers(monitorsChannel, monitorMembers));
    }

    public Set<Update> getOrganizationUpdates(TestingSessionDTO testingSession, OrganizationDTO organization) {
        Set<Update> updates = new HashSet<>();

        Channel privateOrganizationChannel = channelService.getOrganizationPrivateChannel(testingSession, organization);
        Channel publicOrganizationChannel = channelService.getOrganizationPublicChannel(testingSession, organization);
        updates.add(new CheckChannel(privateOrganizationChannel));
        updates.add(new CheckChannel(publicOrganizationChannel));

        Set<OrganizationUserDTO> users = organization.getUsers();
        if (users != null) {
            Set<Member> members = new HashSet<>();
            Set<User> admins = new HashSet<>();
            for (OrganizationUserDTO organizationUser : users) {
                UserDTO userDTO = organizationUser.getUser();
                User user = asUser(userDTO);
                updates.addAll(getUserUpdates(user));
                if (organizationUser.getRoles().contains(OrganizationRoleDTO.VENDOR_ADMIN)) {
                    admins.add(user);
                    members.add(new Member(user, Role.ADMIN));
                } else {
                    members.add(new Member(user, Role.USER));
                }
            }
            // private channel should contain only specified users
            updates.add(new SetChannelMembers(privateOrganizationChannel, members));
            for (Member member : members) {
                // add each member to public channel, can contain other users
                updates.add(new JoinChannel(publicOrganizationChannel, member));
            }
            // set public channel admins (remove moderator role to users not org admin anymore)
            updates.add(new SetChannelAdmins(publicOrganizationChannel, admins));
        }
        return updates;
    }

    public Set<Update> getUserUpdates(User user) {
        return Set.of(new CheckUser(user));
    }

    public User asUser(UserDTO userDTO) {
        return new User(
                userDTO.getUsername(),
                userDTO.getFirstname(),
                userDTO.getLastname(),
                userDTO.getEmail()
        );
    }

    public Set<Update> getTestRunUpdates(TestingSessionDTO testingSession, TestRunDTO testRun) {
        Channel testRunChannel = channelService.getTestRunChannel(testingSession, testRun);
        return Set.of(new CheckChannel(testRunChannel));
    }
}
