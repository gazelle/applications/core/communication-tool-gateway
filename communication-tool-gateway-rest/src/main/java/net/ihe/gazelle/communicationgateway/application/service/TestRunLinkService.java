package net.ihe.gazelle.communicationgateway.application.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.ihe.gazelle.communicationgateway.dto.TestRunDTO;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@NoArgsConstructor
@AllArgsConstructor
public class TestRunLinkService {

    @Inject
    @ConfigProperty(name = "tm.url")
    String tmUrl;

    public String getTmTestRunUrl(TestRunDTO testRun) {
        return tmUrl + "/testing/test/test/TestInstance.seam?id=" + testRun.getId();
    }

}
