package net.ihe.gazelle.communicationgateway.application.command;

import lombok.Value;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;

@Value
public class JoinTeam implements Update {
    Team team;
    Member user;
}
