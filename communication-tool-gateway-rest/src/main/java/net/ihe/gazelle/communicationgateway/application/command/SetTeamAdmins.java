package net.ihe.gazelle.communicationgateway.application.command;

import lombok.Value;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.connector.dto.User;

import java.util.Set;

@Value
public class SetTeamAdmins implements Update {
    Team team;
    Set<User> users;
}
