package net.ihe.gazelle.communicationgateway.application;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.application.command.Update;
import net.ihe.gazelle.communicationgateway.application.service.CommandService;
import net.ihe.gazelle.communicationgateway.application.service.EntityCommandService;
import net.ihe.gazelle.communicationgateway.dto.OrganizationDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Singleton
public class ProvisioningServiceImpl implements ProvisioningService {

    @Inject
    EntityCommandService entityCommandService;

    @Inject
    CommandService commandService;

    @Override
    public void updateTestingSessions(List<TestingSessionDTO> testingSessions) {
        Set<Update> updates = new HashSet<>();
        for (TestingSessionDTO testingSession : testingSessions) {
            updates.addAll(entityCommandService.getTestingSessionUpdates(testingSession));
        }
        commandService.perform(updates);
    }

    @Override
    public void updateOrganizations(TestingSessionDTO testingSession,
                                    List<OrganizationDTO> organizations) {
        Set<Update> updates = new HashSet<>(entityCommandService.getTestingSessionUpdates(testingSession));
        for (OrganizationDTO organization : organizations) {
            updates.addAll(entityCommandService.getOrganizationUpdates(testingSession, organization));
        }
        commandService.perform(updates);
    }

}
