package net.ihe.gazelle.communicationgateway.application.command;

import lombok.Value;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;

@Value
public class JoinChannel implements Update {
    Channel channel;
    Member user;
}
