package net.ihe.gazelle.communicationgateway.interlay;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;


@ApplicationPath("/")
public class CommToolGMRestApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        // resources
        classes.add(HealthController.class);
        classes.add(TestRunController.class);
        classes.add(ProvisioningController.class);
        return classes;
    }
}
