package net.ihe.gazelle.communicationgateway.application.service;

import net.ihe.gazelle.communicationgateway.application.CommunicationToolGatewayException;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class CommunicationToolConnectorLocator {

    @Inject
    Instance<CommunicationToolConnector> connectors;

    @Inject
    @ConfigProperty(name = "connector")
    String connector;

    CommunicationToolConnector communicationToolConnector;

    @PostConstruct
    public void init() {
        for (CommunicationToolConnector candidateCommunicationToolConnector : connectors) {
            if (candidateCommunicationToolConnector.getTarget().equals(connector)) {
                this.communicationToolConnector = candidateCommunicationToolConnector;
                return;
            }
        }
        throw new CommunicationToolGatewayException("No CommunicationToolConnector with target " + connector);
    }

    public CommunicationToolConnector getConnector() {
        return communicationToolConnector;
    }

}
