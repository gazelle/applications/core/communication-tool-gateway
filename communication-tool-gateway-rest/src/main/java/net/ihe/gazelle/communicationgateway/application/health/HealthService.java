package net.ihe.gazelle.communicationgateway.application.health;

public interface HealthService {

    void healthCheck();

}
