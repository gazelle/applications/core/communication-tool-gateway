package net.ihe.gazelle.communicationgateway.application.command;

import lombok.Value;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.User;

import java.util.Set;

@Value
public class SetChannelAdmins implements Update {
    Channel channel;
    Set<User> users;
}
