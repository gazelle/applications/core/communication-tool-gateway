package net.ihe.gazelle.communicationgateway.interlay;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.application.ProvisioningService;
import net.ihe.gazelle.communicationgateway.client.ProvisioningClient;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;
import net.ihe.gazelle.communicationgateway.dto.payload.UpdateOrganizations;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Path;
import java.util.List;

@Path("provisioning") // same as in ProvisioningClient
@Singleton
@Slf4j
public class ProvisioningController implements ProvisioningClient {
    @Inject
    ProvisioningService provisioningService;

    @Override
    public void updateTestingSessions(List<TestingSessionDTO> testingSessions) {
        provisioningService.updateTestingSessions(testingSessions);
    }

    @Override
    public void updateOrganizations(UpdateOrganizations updateOrganizations) {
        provisioningService.updateOrganizations(updateOrganizations.getTestingSession(),
                updateOrganizations.getOrganizations());
    }

}
