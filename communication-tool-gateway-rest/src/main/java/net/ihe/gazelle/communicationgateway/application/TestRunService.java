package net.ihe.gazelle.communicationgateway.application;

import net.ihe.gazelle.communicationgateway.dto.TestRunDTO;
import net.ihe.gazelle.communicationgateway.dto.TestRunRoleDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;
import net.ihe.gazelle.communicationgateway.dto.UserDTO;

public interface TestRunService {
    void createTestRunChannel(TestingSessionDTO testingSession,
                              TestRunDTO testRun,
                              UserDTO creatorDTO);

    void joinTestRunChannel(TestingSessionDTO testingSession,
                            TestRunDTO testRun,
                            UserDTO user,
                            TestRunRoleDTO role);

    void leaveTestRunChannel(TestingSessionDTO testingSession,
                             TestRunDTO testRun,
                             UserDTO user);

    String getInvitationLink(TestingSessionDTO testingSession,
                             TestRunDTO testRun);

    void archiveTestRunChannel(TestingSessionDTO testingSession,
                               TestRunDTO testRun,
                               String testStatus,
                               String lastTestStatus);

    void unarchiveTestRun(TestingSessionDTO testingSession,
                          TestRunDTO testRun,
                          String testStatus,
                          String lastTestStatus);

    void sendTestRunMessage(TestingSessionDTO testingSession,
                            TestRunDTO testRun,
                            String message);
}
