package net.ihe.gazelle.communicationgateway.application.service;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.application.command.*;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.connector.dto.User;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Singleton
public class CommandService {

    @Inject
    CommunicationToolConnectorLocator communicationToolConnectorLocator;

    protected CommunicationToolConnector getConnector() {
        return communicationToolConnectorLocator.getConnector();
    }

    public void perform(Set<Update> updates) {
        Map<? extends Class<? extends Update>, List<Update>> collect = updates.stream().collect(Collectors.groupingBy(Update::getClass));

        List<Update> checkUsers = collect.get(CheckUser.class);
        if (checkUsers != null) {
            List<User> users = checkUsers.stream().map(CheckUser.class::cast).map(CheckUser::getUser).collect(Collectors.toList());
            log.info("Checking {} users", users.size());
            getConnector().checkUsers(new HashSet<>(users));
        }

        List<Update> joinTeams = collect.get(JoinTeam.class);
        if (joinTeams != null) {
            Map<Team, Set<Member>> usersPerTeam = joinTeams.stream().map(JoinTeam.class::cast)
                    .collect(
                            Collectors.groupingBy(
                                    JoinTeam::getTeam,
                                    Collectors.mapping(JoinTeam::getUser, Collectors.toSet())
                            )
                    );
            usersPerTeam.entrySet().stream().parallel().forEach(entry -> {
                Team team = entry.getKey();
                Set<Member> members = entry.getValue();
                log.info("Add {} members to team {}", members.size(), team.getName());
                getConnector().joinTeam(team, members);
            });
        }

        List<Update> setTeamMembersList = collect.get(SetTeamMembers.class);
        if (setTeamMembersList != null) {
            setTeamMembersList.stream().map(SetTeamMembers.class::cast).parallel().forEach(setTeamMembers -> {
                Team team = setTeamMembers.getTeam();
                Set<Member> members = setTeamMembers.getMembers();
                log.info("Set {} members to team {}", members.size(), team.getName());
                getConnector().setTeamMembers(team, members);
            });
        }

        List<Update> setTeamAdminsList = collect.get(SetTeamAdmins.class);
        if (setTeamAdminsList != null) {
            setTeamAdminsList.stream().map(SetTeamAdmins.class::cast).parallel().forEach(setTeamAdmins -> {
                Team team = setTeamAdmins.getTeam();
                Set<User> users = setTeamAdmins.getUsers();
                log.info("Set {} admins on team {}", users.size(), team.getName());
                getConnector().setTeamAdmins(team, users);
            });
        }

        List<Update> checkChannels = collect.get(CheckChannel.class);
        if (checkChannels != null) {
            List<Channel> channelList = checkChannels.stream().map(CheckChannel.class::cast).map(CheckChannel::getChannel).collect(Collectors.toList());
            log.info("Checking {} channels", channelList.size());
            getConnector().checkChannels(new HashSet<>(channelList));
        }

        List<Update> archiveChannels = collect.get(ArchiveChannel.class);
        if (archiveChannels != null) {
            List<Channel> channelList = archiveChannels.stream().map(ArchiveChannel.class::cast).map(ArchiveChannel::getChannel).collect(Collectors.toList());
            log.info("Archiving {} channels", channelList.size());
            channelList.stream().parallel().forEach(channel -> getConnector().archiveChannel(channel));
        }

        List<Update> joinChannels = collect.get(JoinChannel.class);
        if (joinChannels != null) {
            Map<Channel, Set<Member>> joins = joinChannels.stream().map(JoinChannel.class::cast)
                    .collect(
                            Collectors.groupingBy(
                                    JoinChannel::getChannel,
                                    Collectors.mapping(JoinChannel::getUser, Collectors.toSet())
                            )
                    );
            joins.entrySet().stream().parallel().forEach(entry -> {
                Channel channel = entry.getKey();
                Set<Member> members = entry.getValue();
                log.info("Adding {} members to channel {}", members.size(), channel.getName());
                getConnector().joinChannel(channel, members);
            });
        }

        List<Update> setChannelMembersList = collect.get(SetChannelMembers.class);
        if (setChannelMembersList != null) {
            setChannelMembersList.stream().map(SetChannelMembers.class::cast).parallel().forEach(setChannelMembers -> {
                Channel channel = setChannelMembers.getChannel();
                Set<Member> members = setChannelMembers.getMembers();
                log.info("Set {} members to channel {}", members.size(), channel.getName());
                getConnector().setChannelMembers(channel, members);
            });
        }

        List<Update> setChannelAdminsList = collect.get(SetChannelAdmins.class);
        if (setChannelAdminsList != null) {
            setChannelAdminsList.stream().map(SetChannelAdmins.class::cast).parallel().forEach(setChannelAdmins -> {
                Channel channel = setChannelAdmins.getChannel();
                Set<User> users = setChannelAdmins.getUsers();
                log.info("Set {} admins on channel {}", users.size(), channel.getName());
                getConnector().setChannelAdmins(channel, users);
            });
        }

    }

}
