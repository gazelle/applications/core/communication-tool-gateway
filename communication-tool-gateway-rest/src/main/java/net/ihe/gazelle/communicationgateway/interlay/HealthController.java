package net.ihe.gazelle.communicationgateway.interlay;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.communicationgateway.application.health.HealthService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("health")
@Singleton
@Slf4j
public class HealthController {

    @Inject
    HealthService healthService;

    @GET
    public String healthCheck() {
        healthService.healthCheck();
        return "OK";
    }

}
