package net.ihe.gazelle.communicationgateway.application.command;

import lombok.Value;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;

@Value
public class CheckChannel implements Update {
    Channel channel;
}
