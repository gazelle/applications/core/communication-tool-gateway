package net.ihe.gazelle.communicationgateway.application.command;

import lombok.Value;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;

import java.util.Set;

@Value
public class SetTeamMembers implements Update {
    Team team;
    Set<Member> members;
}
