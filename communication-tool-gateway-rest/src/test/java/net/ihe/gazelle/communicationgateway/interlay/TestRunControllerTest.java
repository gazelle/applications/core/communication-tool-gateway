package net.ihe.gazelle.communicationgateway.interlay;

import net.ihe.gazelle.communicationgateway.application.TestRunService;
import net.ihe.gazelle.communicationgateway.dto.TestRunDTO;
import net.ihe.gazelle.communicationgateway.dto.TestRunRoleDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;
import net.ihe.gazelle.communicationgateway.dto.UserDTO;
import net.ihe.gazelle.communicationgateway.dto.payload.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class TestRunControllerTest {

    @Mock
    TestRunService testRunService;

    @InjectMocks
    TestRunController testRunController;

    @Test
    void createTestRunChannel() {
        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);
        UserDTO user = mock(UserDTO.class);
        CreateTestRunChannel param = new CreateTestRunChannel(
                ts,
                tr,
                user
        );
        doNothing().when(testRunService).createTestRunChannel(ts, tr, user);
        testRunController.createTestRunChannel(param);
        verifyNoMoreInteractions(testRunService);
    }

    @Test
    void joinTestRunChannel() {
        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);
        UserDTO user = mock(UserDTO.class);
        JoinTestRunChannel param = new JoinTestRunChannel(
                ts,
                tr,
                user,
                TestRunRoleDTO.ADMIN
        );
        doNothing().when(testRunService).joinTestRunChannel(ts, tr, user, TestRunRoleDTO.ADMIN);
        testRunController.joinTestRunChannel(param);
        verifyNoMoreInteractions(testRunService);
    }

    @Test
    void leaveTestRunChannel() {
        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);
        UserDTO user = mock(UserDTO.class);
        LeaveTestRunChannel param = new LeaveTestRunChannel(
                ts,
                tr,
                user
        );
        doNothing().when(testRunService).leaveTestRunChannel(ts, tr, user);
        testRunController.leaveTestRunChannel(param);
        verifyNoMoreInteractions(testRunService);
    }

    @Test
    void getInvitationLink() {
        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);
        TestRunInvitationLink param = new TestRunInvitationLink(
                ts, tr
        );
        when(testRunService.getInvitationLink(ts, tr)).thenReturn("urlValue");
        String result = testRunController.getInvitationLink(param);
        assertEquals("urlValue", result);
        verifyNoMoreInteractions(testRunService);
    }

    @Test
    void archiveTestRunChannel() {
        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);
        ArchiveTestRunChannel param = new ArchiveTestRunChannel(
                ts, tr, null, null
        );
        doNothing().when(testRunService).archiveTestRunChannel(ts, tr, null, null);
        testRunController.archiveTestRunChannel(param);
        verifyNoMoreInteractions(testRunService);
    }

    @Test
    void unarchiveTestRun() {
        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);
        UnarchiveTestRunChannel param = new UnarchiveTestRunChannel(
                ts, tr, null, null
        );
        doNothing().when(testRunService).unarchiveTestRun(ts, tr, null, null);
        testRunController.unarchiveTestRun(param);
        verifyNoMoreInteractions(testRunService);
    }

    @Test
    void sendTestRunMessage() {
        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);
        String message = "coucou";
        TestRunMessage param = new TestRunMessage(
                ts, tr, message
        );
        doNothing().when(testRunService).sendTestRunMessage(ts, tr, message);
        testRunController.sendTestRunMessage(param);
        verifyNoMoreInteractions(testRunService);
    }
}