package net.ihe.gazelle.communicationgateway.interlay;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CommToolGMRestApplicationTest {

    @Test
    void getClasses() {
        CommToolGMRestApplication commToolGMRestApplication = new CommToolGMRestApplication();
        Set<Class<?>> classes = commToolGMRestApplication.getClasses();
        assertEquals(3, classes.size());
        assertTrue(classes.containsAll(
                Set.of(HealthController.class, ProvisioningController.class, TestRunController.class)
        ));
    }

}