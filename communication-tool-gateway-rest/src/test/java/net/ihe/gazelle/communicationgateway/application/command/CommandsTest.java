package net.ihe.gazelle.communicationgateway.application.command;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

class CommandsTest {

    @Test
    void beans() {
        EqualsVerifier.simple()
                .forPackage("net.ihe.gazelle.communicationgateway.application.command", true)
                .verify();
    }
}
