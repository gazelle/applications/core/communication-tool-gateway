package net.ihe.gazelle.communicationgateway.application.service;

import net.ihe.gazelle.communicationgateway.application.command.*;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import net.ihe.gazelle.communicationgateway.connector.dto.*;
import net.ihe.gazelle.communicationgateway.dto.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class EntityCommandServiceTest {

    @Mock
    CommunicationToolConnector connector;

    @Mock
    CommunicationToolConnectorLocator communicationToolConnectorLocator;

    @Mock
    ChannelService channelService;

    @InjectMocks
    EntityCommandService entityCommandService;

    TestingSessionDTO simpleTestingSession = new TestingSessionDTO(
            "123",
            "EU-CAT 2022",
            "Europe CAT for 2022",
            null,
            null,
            null
    );

    @Test
    void getTestingSessionUpdates1() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);
        Team team = mock(Team.class);
        when(channelService.getTeam(simpleTestingSession)).thenReturn(team);
        when(connector.getChannels(team)).thenReturn(Set.of());

        Channel monitors = getChannel("m_123");
        when(channelService.getMonitorsChannel(simpleTestingSession))
                .thenReturn(monitors);
        Set<Update> testingSessionUpdates = entityCommandService.getTestingSessionUpdates(simpleTestingSession);
        assertEquals(
                Set.of(new CheckChannel(monitors)),
                testingSessionUpdates
        );

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, connector);
    }

    @Test
    void getTestingSessionUpdates2() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);
        Team team = new Team("team_0", null, null, null);
        Channel channel1 = new Channel("a", null, null, null, null);
        when(channelService.isOrganizationChannel(channel1)).thenReturn(true);
        when(connector.getChannels(team)).thenReturn(Set.of(channel1));

        TestingSessionDTO testingSession = new TestingSessionDTO();
        testingSession.setId("456");
        testingSession.setKeyword("EU-CAT 2023");
        testingSession.setDescription("Europe CAT 2023");

        UserDTO user1DTO = new UserDTO("b", "b", "b", "b", "b");
        User user1 = entityCommandService.asUser(user1DTO);
        Member memberUser1 = new Member(user1, Role.USER);

        UserDTO monitor1DTO = new UserDTO("c", "c", "c", "c", "c");
        User monitor1 = entityCommandService.asUser(monitor1DTO);
        Member memberMonitor1 = new Member(monitor1, Role.USER);

        UserDTO admin1DTO = new UserDTO("d", "d", "d", "d", "d");
        User admin1 = entityCommandService.asUser(admin1DTO);
        Member memberAdmin1 = new Member(admin1, Role.ADMIN);

        ProfileDTO profile1 = new ProfileDTO("d", "d", "d");

        OrganizationDTO orga1 = new OrganizationDTO("a", "a", "a",
                Set.of(
                        new OrganizationUserDTO(user1DTO, Set.of(OrganizationRoleDTO.VENDOR_ADMIN)),
                        new OrganizationUserDTO(monitor1DTO, Set.of())
                )
        );

        testingSession.setOrganizations(
                Set.of(orga1)
        );
        testingSession.setUsers(
                Set.of(
                        new TestingSessionUserDTO(user1DTO, Set.of()),
                        new TestingSessionUserDTO(monitor1DTO, Set.of(TestingSessionRoleDTO.MONITOR)),
                        new TestingSessionUserDTO(admin1DTO, Set.of(TestingSessionRoleDTO.TESTING_SESSION_ADMIN))
                )
        );
        testingSession.setProfiles(
                Set.of(profile1)
        );

        when(channelService.getTeam(testingSession))
                .thenReturn(team);

        Channel monitorsChannel = getChannel("m_123");
        when(channelService.getMonitorsChannel(testingSession))
                .thenReturn(monitorsChannel);
        Channel orga1PublicChannel = getChannel("orga_public");
        when(channelService.getOrganizationPublicChannel(testingSession, orga1))
                .thenReturn(orga1PublicChannel);
        Channel orga1PrivateChannel = getChannel("orga_private");
        when(channelService.getOrganizationPrivateChannel(testingSession, orga1))
                .thenReturn(orga1PrivateChannel);
        Channel profile1Channel = getChannel("profile");
        when(channelService.getProfileChannel(testingSession, profile1))
                .thenReturn(profile1Channel);

        Set<Update> testingSessionUpdates = entityCommandService.getTestingSessionUpdates(testingSession);
        Set<Update> expected = Set.of(
                new CheckChannel(monitorsChannel),
                new CheckChannel(orga1PublicChannel),
                new CheckChannel(orga1PrivateChannel),
                new CheckChannel(profile1Channel),
                new CheckUser(user1),
                new CheckUser(monitor1),
                new CheckUser(admin1),
                new JoinTeam(team, memberUser1),
                new JoinTeam(team, memberMonitor1),
                new JoinTeam(team, memberAdmin1),
                new SetTeamAdmins(team, Set.of(admin1)),
                new SetChannelMembers(monitorsChannel, Set.of(memberMonitor1, memberAdmin1)),
                new SetChannelMembers(orga1PrivateChannel, Set.of(new Member(user1, Role.ADMIN), new Member(monitor1, Role.USER))),
                new JoinChannel(orga1PublicChannel, new Member(user1, Role.ADMIN)),
                new JoinChannel(orga1PublicChannel, new Member(monitor1, Role.USER)),
                new SetChannelAdmins(orga1PublicChannel, Set.of(user1)),
                new ArchiveChannel(channel1)
        );
        for (Update update : expected) {
            assertTrue(testingSessionUpdates.contains(update));
        }
        assertEquals(expected.size(), testingSessionUpdates.size());
        assertEquals(expected, testingSessionUpdates);

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, connector);
    }

    @Test
    void getTestingSessionUpdates3() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);
        Team team = mock(Team.class);
        when(channelService.getTeam(simpleTestingSession)).thenReturn(team);
        Channel channel1 = new Channel("a", null, null, null, null);
        Channel channel2 = new Channel("b", null, null, null, null);
        Channel channel3 = new Channel("c", null, null, null, null);
        when(connector.getChannels(team)).thenReturn(Set.of(channel1, channel2, channel3));

        when(channelService.isOrganizationChannel(channel1)).thenReturn(false);
        when(channelService.isProfileChannel(channel1)).thenReturn(false);

        when(channelService.isOrganizationChannel(channel2)).thenReturn(true);

        when(channelService.isOrganizationChannel(channel3)).thenReturn(false);
        when(channelService.isProfileChannel(channel3)).thenReturn(true);

        Channel monitors = getChannel("m_123");
        when(channelService.getMonitorsChannel(simpleTestingSession))
                .thenReturn(monitors);
        Set<Update> testingSessionUpdates = entityCommandService.getTestingSessionUpdates(simpleTestingSession);
        assertEquals(
                Set.of(new CheckChannel(monitors), new ArchiveChannel(channel2), new ArchiveChannel(channel3)),
                testingSessionUpdates
        );

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, connector);
    }

    private Channel getChannel(String id) {
        return new Channel(id, null, null, null, null);
    }

    @Test
    void getOrganizationUpdates() {

        UserDTO user1DTO = new UserDTO("b", "b", "b", "b", "b");
        User user1 = entityCommandService.asUser(user1DTO);
        Member member1 = new Member(user1, Role.USER);

        UserDTO admin1DTO = new UserDTO("b", "b", "b", "b", "b");
        User admin1 = entityCommandService.asUser(admin1DTO);
        Member member2 = new Member(admin1, Role.ADMIN);

        OrganizationDTO orga1 = new OrganizationDTO("a", "a", "a",
                Set.of(
                        new OrganizationUserDTO(user1DTO, Set.of()),
                        new OrganizationUserDTO(admin1DTO, Set.of(OrganizationRoleDTO.VENDOR_ADMIN))
                )
        );

        Channel orga1PublicChannel = getChannel("orga_public");
        Channel orga1PrivateChannel = getChannel("orga_private");
        when(channelService.getOrganizationPrivateChannel(simpleTestingSession, orga1))
                .thenReturn(orga1PrivateChannel);
        when(channelService.getOrganizationPublicChannel(simpleTestingSession, orga1))
                .thenReturn(orga1PublicChannel);

        Set<Update> updates = entityCommandService.getOrganizationUpdates(simpleTestingSession, orga1);
        assertEquals(
                Set.of(
                        new CheckChannel(orga1PublicChannel),
                        new CheckChannel(orga1PrivateChannel),
                        new CheckUser(user1),
                        new JoinChannel(orga1PublicChannel, member1),
                        new JoinChannel(orga1PublicChannel, member2),
                        new SetChannelAdmins(orga1PublicChannel, Set.of(admin1)),
                        new SetChannelMembers(orga1PrivateChannel, Set.of(member1, member2))
                ),
                updates
        );
        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, connector);
    }

    @Test
    void getOrganizationUpdates2() {
        OrganizationDTO orga1 = new OrganizationDTO("a", "a", "a", null);

        Channel orga1PublicChannel = getChannel("orga_public");
        Channel orga1PrivateChannel = getChannel("orga_private");
        when(channelService.getOrganizationPrivateChannel(simpleTestingSession, orga1))
                .thenReturn(orga1PrivateChannel);
        when(channelService.getOrganizationPublicChannel(simpleTestingSession, orga1))
                .thenReturn(orga1PublicChannel);

        Set<Update> updates = entityCommandService.getOrganizationUpdates(simpleTestingSession, orga1);
        assertEquals(
                Set.of(
                        new CheckChannel(orga1PublicChannel),
                        new CheckChannel(orga1PrivateChannel)
                ),
                updates
        );
        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, connector);
    }

    @Test
    void getUserUpdates() {
        UserDTO user1DTO = new UserDTO("b", "b", "b", "b", "b");
        User user1 = entityCommandService.asUser(user1DTO);

        Set<Update> updates = entityCommandService.getUserUpdates(user1);
        assertEquals(
                Set.of(
                        new CheckUser(user1)
                ),
                updates
        );
        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, connector);
    }

    @Test
    void asUser() {
        UserDTO user1DTO = new UserDTO("b", "username", "firstname", "lastname", "email");
        User user1 = entityCommandService.asUser(user1DTO);
        assertEquals(user1DTO.getUsername(), user1.getUsername());
        assertEquals(user1DTO.getFirstname(), user1.getFirstname());
        assertEquals(user1DTO.getLastname(), user1.getLastname());
        assertEquals(user1DTO.getEmail(), user1.getEmail());
        verifyNoInteractions(channelService);
    }

    @Test
    void getTestRunUpdates() {

        TestRunDTO testRun = new TestRunDTO("abcdf", new TestDTO("uio", "TEST-0", "Test 0"), null);

        Channel trChannel = getChannel("tr_abcdf");
        when(channelService.getTestRunChannel(simpleTestingSession, testRun))
                .thenReturn(trChannel);

        Set<Update> updates = entityCommandService.getTestRunUpdates(simpleTestingSession, testRun);
        assertEquals(
                Set.of(
                        new CheckChannel(trChannel)
                ),
                updates
        );
        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, connector);
    }
}