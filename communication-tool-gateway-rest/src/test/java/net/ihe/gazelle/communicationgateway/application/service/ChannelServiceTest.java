package net.ihe.gazelle.communicationgateway.application.service;

import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.PrivacyType;
import net.ihe.gazelle.communicationgateway.connector.dto.Team;
import net.ihe.gazelle.communicationgateway.dto.*;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ChannelServiceTest {

    TestRunLinkService testRunLinkService = new TestRunLinkService("https://gazelle");

    ChannelService channelService = new ChannelService(testRunLinkService);

    TestingSessionDTO testingSession = new TestingSessionDTO(
            "123",
            "EU-CAT 2022",
            "Europe CAT for 2022",
            null,
            null,
            null
    );

    @Test
    void getTeam() {
        Team team = channelService.getTeam(testingSession);
        checkTeam(team);
    }

    private void checkTeam(Team team) {
        assertEquals("ts_123", team.getId());
        assertEquals("EU-CAT 2022", team.getName());
        assertEquals("Team for \"Europe CAT for 2022\"", team.getTopic());
        assertEquals(PrivacyType.PRIVATE, team.getPrivacyType());
    }

    @Test
    void getTestRunChannel() {
        TestingSessionDTO testingSession = new TestingSessionDTO(
                "123",
                "EU-CAT 2022",
                "Europe CAT for 2022",
                null,
                null,
                null
        );
        TestRunDTO testRun = new TestRunDTO(
                "456",
                new TestDTO(
                        "789",
                        "TEST-0",
                        "TEST 0 is awesome"
                ),
                Set.of(
                        new SystemDTO(
                                "s1id", "s1keyword", "s1name",
                                Set.of(
                                        new OrganizationDTO(
                                                "o1id", "o1keyword", "o1name", null
                                        ),
                                        new OrganizationDTO(
                                                "o2id", "o2keyword", "o2name", null
                                        )
                                )
                        ),
                        new SystemDTO(
                                "s2id", "s2keyword", "s2name",
                                Set.of(
                                        new OrganizationDTO(
                                                "o1id", "o1keyword", "o1name", null
                                        ),
                                        new OrganizationDTO(
                                                "o3id", "o3keyword", "o3name", null
                                        )
                                )
                        )
                )
        );
        Channel testRunChannel = channelService.getTestRunChannel(testingSession, testRun);
        assertEquals("tr_456", testRunChannel.getId());
        assertEquals("TR 456 TEST-0", testRunChannel.getName());
        assertEquals("Private topic for test run 456 - TEST 0 is awesome - https://gazelle/testing/test/test/TestInstance.seam?id=456", testRunChannel.getTopic());
        checkTeam(testRunChannel.getTeam());
        assertEquals(PrivacyType.PRIVATE, testRunChannel.getPrivacyType());
    }

    @Test
    void getTestRunChannel2() {
        TestingSessionDTO testingSession = new TestingSessionDTO(
                "123",
                "EU-CAT 2022",
                "Europe CAT for 2022",
                null,
                null,
                null
        );
        TestRunDTO testRun = new TestRunDTO(
                "456",
                new TestDTO(
                        "789",
                        "TEST-0",
                        "TEST 0 is awesome"
                ),
                null
        );
        Channel testRunChannel = channelService.getTestRunChannel(testingSession, testRun);
        assertEquals("tr_456", testRunChannel.getId());
        assertEquals("TR 456 TEST-0", testRunChannel.getName());
        assertNull(testRunChannel.getTopic());
        checkTeam(testRunChannel.getTeam());
        assertEquals(PrivacyType.PRIVATE, testRunChannel.getPrivacyType());
    }

    @Test
    void getOrganizationPrivateChannel() {
        OrganizationDTO organization = new OrganizationDTO(
                "abc",
                "ORGA0",
                "Organization 0",
                null
        );
        Channel organizationChannel = channelService.getOrganizationPrivateChannel(testingSession, organization);

        assertEquals("org_abc_private_123", organizationChannel.getId());
        assertEquals("ORGA0 - Private - EU-CAT 2022", organizationChannel.getName());
        assertEquals("Private channel for organization Organization 0", organizationChannel.getTopic());
        checkTeam(organizationChannel.getTeam());
        assertEquals(PrivacyType.PRIVATE, organizationChannel.getPrivacyType());
    }

    @Test
    void getOrganizationPublicChannel() {
        OrganizationDTO organization = new OrganizationDTO(
                "abc",
                "ORGA0",
                "Organization 0",
                null
        );
        Channel organizationChannel = channelService.getOrganizationPublicChannel(testingSession, organization);

        assertEquals("org_abc_public_123", organizationChannel.getId());
        assertEquals("ORGA0 - Public - EU-CAT 2022", organizationChannel.getName());
        assertEquals("Public channel for organization Organization 0", organizationChannel.getTopic());
        checkTeam(organizationChannel.getTeam());
        assertEquals(PrivacyType.PUBLIC, organizationChannel.getPrivacyType());
    }

    @Test
    void getMonitorsChannel() {
        Channel monitorsChannel = channelService.getMonitorsChannel(testingSession);
        assertEquals("m_123", monitorsChannel.getId());
        assertEquals("Monitors - EU-CAT 2022", monitorsChannel.getName());
        assertEquals("Private channel for monitors", monitorsChannel.getTopic());
        checkTeam(monitorsChannel.getTeam());
        assertEquals(PrivacyType.PRIVATE, monitorsChannel.getPrivacyType());
    }

    @Test
    void getProfileChannel() {
        ProfileDTO profile = new ProfileDTO(
                "def",
                "PROF-0",
                "Profile 0"
        );
        Channel profileChannel = channelService.getProfileChannel(testingSession, profile);
        assertEquals("prof_def_123", profileChannel.getId());
        assertEquals("Profile PROF-0 - EU-CAT 2022", profileChannel.getName());
        assertEquals("Public channel for profile Profile 0", profileChannel.getTopic());
        checkTeam(profileChannel.getTeam());
        assertEquals(PrivacyType.PUBLIC, profileChannel.getPrivacyType());
    }

    @Test
    void isOrganizationChannel() {
        assertFalse(channelService.isOrganizationChannel(new Channel(null, null, null, null, null)));
        assertFalse(channelService.isOrganizationChannel(new Channel("ergreerg", null, null, null, null)));
        assertTrue(channelService.isOrganizationChannel(new Channel("org_", null, null, null, null)));
        assertTrue(channelService.isOrganizationChannel(new Channel("org_zezeezfezf", null, null, null, null)));
    }

    @Test
    void isProfileChannel() {
        assertFalse(channelService.isProfileChannel(new Channel(null, null, null, null, null)));
        assertFalse(channelService.isProfileChannel(new Channel("ergreerg", null, null, null, null)));
        assertTrue(channelService.isProfileChannel(new Channel("prof_", null, null, null, null)));
        assertTrue(channelService.isProfileChannel(new Channel("prof_zezeezfezf", null, null, null, null)));
    }
}