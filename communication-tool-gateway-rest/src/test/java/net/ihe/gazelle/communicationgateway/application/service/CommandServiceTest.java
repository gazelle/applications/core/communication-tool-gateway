package net.ihe.gazelle.communicationgateway.application.service;

import net.ihe.gazelle.communicationgateway.application.command.*;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import net.ihe.gazelle.communicationgateway.connector.dto.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class CommandServiceTest {

    @Mock
    CommunicationToolConnector connector;

    @Mock
    CommunicationToolConnectorLocator communicationToolConnectorLocator;

    @InjectMocks
    CommandService commandService;

    @Test
    void performNone() {
        Set<Update> updates = new HashSet<>();
        commandService.perform(updates);
        verifyNoInteractions(communicationToolConnectorLocator);
    }

    @Test
    void perform() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);
        User user = new User("abcd", null, null, null);
        User user2 = new User("efgh", null, null, null);
        Member member1 = new Member(user, Role.USER);
        Member member2 = new Member(user, Role.ADMIN);
        Member member3 = new Member(user2, Role.ADMIN);
        Team team1 = new Team("123", null, null, null);
        Team team2 = new Team("456", null, null, null);
        Channel channel1 = new Channel("efg", null, null, null, null);
        Channel channel2 = new Channel("hij", null, null, null, null);
        Channel channel3 = new Channel("klo", null, null, null, null);

        doNothing().when(connector).checkUsers(Set.of(user));
        doNothing().when(connector).joinTeam(team1, Set.of(member1, member2, member3));
        doNothing().when(connector).joinTeam(team2, Set.of(member1));
        doNothing().when(connector).checkChannels(Set.of(channel1));
        doNothing().when(connector).joinChannel(channel1, Set.of(member1, member2, member3));
        doNothing().when(connector).joinChannel(channel2, Set.of(member1));
        doNothing().when(connector).setChannelMembers(channel2, Set.of(member1, member2, member3));
        doNothing().when(connector).setTeamMembers(team1, Set.of(member1, member2, member3));
        doNothing().when(connector).setChannelAdmins(channel1, Set.of(user));
        doNothing().when(connector).setTeamAdmins(team1, Set.of(user));
        doNothing().when(connector).archiveChannel(channel3);

        Set<Update> updates = new HashSet<>();
        updates.add(new CheckUser(user));
        updates.add(new JoinTeam(team1, member1));
        updates.add(new JoinTeam(team1, member2));
        updates.add(new JoinTeam(team1, member3));
        updates.add(new JoinTeam(team2, member1));
        updates.add(new SetTeamMembers(team1, Set.of(member1, member2, member3)));
        updates.add(new SetTeamAdmins(team1, Set.of(user)));
        updates.add(new CheckChannel(channel1));
        updates.add(new JoinChannel(channel1, member1));
        updates.add(new JoinChannel(channel1, member2));
        updates.add(new JoinChannel(channel1, member3));
        updates.add(new JoinChannel(channel2, member1));
        updates.add(new SetChannelMembers(channel2, Set.of(member1, member2, member3)));
        updates.add(new SetChannelAdmins(channel1, Set.of(user)));
        updates.add(new ArchiveChannel(channel3));
        commandService.perform(updates);

        verifyNoMoreInteractions(connector);
    }
}