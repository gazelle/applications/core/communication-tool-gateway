package net.ihe.gazelle.communicationgateway.application;

import net.ihe.gazelle.communicationgateway.application.command.CheckChannel;
import net.ihe.gazelle.communicationgateway.application.command.CheckUser;
import net.ihe.gazelle.communicationgateway.application.command.Update;
import net.ihe.gazelle.communicationgateway.application.service.CommandService;
import net.ihe.gazelle.communicationgateway.application.service.EntityCommandService;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.User;
import net.ihe.gazelle.communicationgateway.dto.OrganizationDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class ProvisioningServiceImplTest {

    @Mock
    EntityCommandService entityCommandService;

    @Mock
    CommandService commandService;

    @InjectMocks
    ProvisioningServiceImpl provisioningService;

    @Test
    void updateTestingSessions() {
        TestingSessionDTO ts1 = mock(TestingSessionDTO.class);
        TestingSessionDTO ts2 = mock(TestingSessionDTO.class);
        TestingSessionDTO ts3 = mock(TestingSessionDTO.class);
        Set<Update> set = Set.of(new CheckUser(new User()));
        when(entityCommandService.getTestingSessionUpdates(any())).thenReturn(set);
        doNothing().when(commandService).perform(set);
        provisioningService.updateTestingSessions(List.of(ts1, ts2, ts3));
        verifyNoMoreInteractions(entityCommandService);
        verifyNoMoreInteractions(commandService);
    }

    @Test
    void updateOrganizations() {
        TestingSessionDTO ts1 = mock(TestingSessionDTO.class);

        Set<Update> set1 = Set.of(new CheckUser(new User()));
        Set<Update> set2 = Set.of(new CheckChannel(new Channel()));
        Set<Update> all = new HashSet<>(set1);
        all.addAll(set2);

        when(entityCommandService.getTestingSessionUpdates(any())).thenReturn(set1);
        when(entityCommandService.getOrganizationUpdates(any(), any())).thenReturn(set2);
        doNothing().when(commandService).perform(all);

        OrganizationDTO o1 = mock(OrganizationDTO.class);
        OrganizationDTO o2 = mock(OrganizationDTO.class);
        OrganizationDTO o3 = mock(OrganizationDTO.class);
        provisioningService.updateOrganizations(ts1, List.of(o1, o2, o3));

        verifyNoMoreInteractions(entityCommandService);
        verifyNoMoreInteractions(commandService);
    }
}