package net.ihe.gazelle.communicationgateway.interlay;

import net.ihe.gazelle.communicationgateway.application.ProvisioningService;
import net.ihe.gazelle.communicationgateway.dto.OrganizationDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;
import net.ihe.gazelle.communicationgateway.dto.payload.UpdateOrganizations;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class ProvisioningControllerTest {

    @Mock
    ProvisioningService provisioningService;

    @InjectMocks
    ProvisioningController provisioningController;

    @Test
    void updateTestingSessions() {
        TestingSessionDTO ts1 = mock(TestingSessionDTO.class);
        TestingSessionDTO ts2 = mock(TestingSessionDTO.class);
        TestingSessionDTO ts3 = mock(TestingSessionDTO.class);
        List<TestingSessionDTO> tsList = List.of(ts1, ts2, ts3);
        doNothing().when(provisioningService).updateTestingSessions(tsList);
        provisioningController.updateTestingSessions(tsList);
        verifyNoMoreInteractions(provisioningService);
    }

    @Test
    void updateOrganizations() {
        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        List<OrganizationDTO> oList = List.of(mock(OrganizationDTO.class), mock(OrganizationDTO.class), mock(OrganizationDTO.class));
        doNothing().when(provisioningService).updateOrganizations(ts, oList);
        UpdateOrganizations updateOrganizations = new UpdateOrganizations(ts, oList);
        provisioningController.updateOrganizations(updateOrganizations);
        verifyNoMoreInteractions(provisioningService);
    }
}