package net.ihe.gazelle.communicationgateway.application.service;

import net.ihe.gazelle.communicationgateway.application.CommunicationToolGatewayException;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CommunicationToolConnectorLocatorTest {

    @Test
    void getConnectorKo() {
        CommunicationToolConnectorLocator communicationToolConnectorLocator = new CommunicationToolConnectorLocator();
        communicationToolConnectorLocator.connector = "abcd";
        communicationToolConnectorLocator.connectors = new InstanceStub<>(
                List.of(
                        new CommunicationToolConnectorStub("1234"),
                        new CommunicationToolConnectorStub("5678")
                )
        );
        assertThrows(CommunicationToolGatewayException.class, communicationToolConnectorLocator::init);
    }

    @Test
    void getConnectorOk() {
        CommunicationToolConnectorLocator communicationToolConnectorLocator = new CommunicationToolConnectorLocator();
        communicationToolConnectorLocator.connector = "abcd";
        communicationToolConnectorLocator.connectors = new InstanceStub<>(
                List.of(
                        new CommunicationToolConnectorStub("abcd"),
                        new CommunicationToolConnectorStub("5678")
                )
        );
        communicationToolConnectorLocator.init();
        CommunicationToolConnector connector = communicationToolConnectorLocator.getConnector();
        assertEquals("abcd", connector.getTarget());
    }
}