package net.ihe.gazelle.communicationgateway.interlay;

import net.ihe.gazelle.communicationgateway.application.health.HealthService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verifyNoMoreInteractions;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class HealthControllerTest {

    @Mock
    HealthService healthService;

    @InjectMocks
    HealthController healthController;

    @Test
    void healthCheck() {
        doNothing().when(healthService).healthCheck();
        String response = healthController.healthCheck();
        assertEquals("OK", response);
        verifyNoMoreInteractions(healthService);
    }
}