package net.ihe.gazelle.communicationgateway.application.health;

import net.ihe.gazelle.communicationgateway.application.service.CommunicationToolConnectorLocator;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class HealthServiceImplTest {

    @Mock
    CommunicationToolConnector connector;

    @Mock
    CommunicationToolConnectorLocator communicationToolConnectorLocator;

    @InjectMocks
    HealthServiceImpl healthService;

    @Test
    void healthCheckOk() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);
        doNothing().when(connector).healthCheck();
        assertDoesNotThrow(() -> healthService.healthCheck());
        verifyNoMoreInteractions(communicationToolConnectorLocator);
        verifyNoMoreInteractions(connector);
    }

    @Test
    void healthCheckNotOk() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);
        doThrow(new RuntimeException()).when(connector).healthCheck();
        assertThrows(RuntimeException.class, () -> healthService.healthCheck());
        verifyNoMoreInteractions(communicationToolConnectorLocator);
        verifyNoMoreInteractions(connector);
    }
}