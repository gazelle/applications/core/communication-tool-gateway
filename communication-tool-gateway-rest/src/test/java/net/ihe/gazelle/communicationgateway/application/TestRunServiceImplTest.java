package net.ihe.gazelle.communicationgateway.application;

import net.ihe.gazelle.communicationgateway.application.command.Update;
import net.ihe.gazelle.communicationgateway.application.service.*;
import net.ihe.gazelle.communicationgateway.connector.CommunicationToolConnector;
import net.ihe.gazelle.communicationgateway.connector.dto.Channel;
import net.ihe.gazelle.communicationgateway.connector.dto.Member;
import net.ihe.gazelle.communicationgateway.connector.dto.Role;
import net.ihe.gazelle.communicationgateway.connector.dto.User;
import net.ihe.gazelle.communicationgateway.dto.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class TestRunServiceImplTest {

    @Mock
    ChannelService channelService;

    @Mock
    CommunicationToolConnector connector;

    @Mock
    CommunicationToolConnectorLocator communicationToolConnectorLocator;

    @Mock
    EntityCommandService entityCommandService;

    @Mock
    CommandService commandService;

    @Mock
    TestRunLinkService testRunLinkService;

    @InjectMocks
    TestRunServiceImpl testRunService;

    @Test
    void createTestRunChannel() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);

        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);
        TestDTO t = mock(TestDTO.class);
        when(t.getKeyword()).thenReturn("TEST");
        when(tr.getTest()).thenReturn(t);
        SystemDTO s1 = mock(SystemDTO.class);
        OrganizationDTO o1 = mock(OrganizationDTO.class);
        when(s1.getName()).thenReturn("System b1");
        when(s1.getOrganizations()).thenReturn(Set.of(o1));
        SystemDTO s2 = mock(SystemDTO.class);
        OrganizationDTO o2 = mock(OrganizationDTO.class);
        when(s2.getOrganizations()).thenReturn(Set.of(o2));
        when(s2.getName()).thenReturn("System a2");
        when(tr.getSystems()).thenReturn(Set.of(s1, s2));
        UserDTO creator = mock(UserDTO.class);

        prepareTestRun(ts, tr, creator);

        Channel oc1 = mock(Channel.class);
        when(channelService.getOrganizationPrivateChannel(ts, o1)).thenReturn(oc1);
        doNothing().when(connector).sendMessage(eq(oc1), any());

        Channel oc2 = mock(Channel.class);
        when(channelService.getOrganizationPrivateChannel(ts, o2)).thenReturn(oc2);
        doNothing().when(connector).sendMessage(eq(oc2), any());

        Channel ocp1 = mock(Channel.class);
        when(channelService.getOrganizationPublicChannel(ts, o1)).thenReturn(ocp1);

        Channel ocp2 = mock(Channel.class);
        when(channelService.getOrganizationPublicChannel(ts, o2)).thenReturn(ocp2);

        Channel trc = mock(Channel.class);
        when(channelService.getTestRunChannel(ts, tr)).thenReturn(trc);
        when(connector.getInvitationLink(trc)).thenReturn("trcLink");
        doNothing().when(connector).sendMessage(eq(trc), any());

        testRunService.createTestRunChannel(ts, tr, creator);

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, entityCommandService, commandService);
    }

    @Test
    void joinTestRunChannelAdmin() {
        joinTestRunChannelGeneric(TestRunRoleDTO.ADMIN, Role.ADMIN);
    }

    @Test
    void joinTestRunChannelParticipant() {
        joinTestRunChannelGeneric(TestRunRoleDTO.PARTICIPANT, Role.USER);
    }

    private void joinTestRunChannelGeneric(TestRunRoleDTO testRunRoleDTO, Role role) {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);

        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);

        UserDTO user = mock(UserDTO.class);
        User u = mock(User.class);
        when(entityCommandService.asUser(user)).thenReturn(u);

        prepareTestRun(ts, tr, null);

        Channel trc = mock(Channel.class);
        when(channelService.getTestRunChannel(ts, tr)).thenReturn(trc);

        Member member = new Member(u, role);
        doNothing().when(connector).joinChannel(trc, Set.of(member));

        testRunService.joinTestRunChannel(ts, tr, user, testRunRoleDTO);

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, entityCommandService, commandService);
    }

    @Test
    void leaveTestRunChannelGeneric() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);

        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);

        UserDTO user = mock(UserDTO.class);
        User u = mock(User.class);
        when(entityCommandService.asUser(user)).thenReturn(u);

        prepareTestRun(ts, tr, null);

        Channel trc = mock(Channel.class);
        when(channelService.getTestRunChannel(ts, tr)).thenReturn(trc);

        doNothing().when(connector).leaveChannel(trc, Set.of(u));

        testRunService.leaveTestRunChannel(ts, tr, user);

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, entityCommandService, commandService);
    }

    private void prepareTestRun(TestingSessionDTO ts, TestRunDTO tr, UserDTO creator) {
        Update update1 = mock(Update.class);
        Update update2 = mock(Update.class);
        when(entityCommandService.getTestingSessionUpdates(ts)).thenReturn(Set.of(update1));
        when(entityCommandService.getTestRunUpdates(ts, tr)).thenReturn(Set.of(update2));

        if (creator != null) {
            User u = mock(User.class);
            when(entityCommandService.asUser(creator)).thenReturn(u);
            Update update3 = mock(Update.class);
            when(entityCommandService.getUserUpdates(u)).thenReturn(Set.of(update3));
            Channel c = mock(Channel.class);
            when(channelService.getTestRunChannel(ts, tr)).thenReturn(c);
        }

        Set<SystemDTO> systems = tr.getSystems();
        if (systems != null) {
            for (SystemDTO system : systems) {
                if (system.getOrganizations() != null) {
                    for (OrganizationDTO o : system.getOrganizations()) {
                        Update update = mock(Update.class);
                        when(entityCommandService.getOrganizationUpdates(ts, o)).thenReturn(Set.of(update));
                    }
                }
            }
        }

        doNothing().when(commandService).perform(any());
    }

    @Test
    void getInvitationLink() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);

        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);
        when(tr.getSystems()).thenReturn(null);

        prepareTestRun(ts, tr, null);

        Channel trc = mock(Channel.class);
        when(channelService.getTestRunChannel(ts, tr)).thenReturn(trc);
        when(connector.getInvitationLink(trc)).thenReturn("trcLink");

        String invitationLink = testRunService.getInvitationLink(ts, tr);
        assertEquals("trcLink", invitationLink);

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, entityCommandService, commandService);
    }

    @Test
    void archiveTestRunChannel() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);

        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);

        prepareTestRun(ts, tr, null);

        Channel trc = mock(Channel.class);
        when(channelService.getTestRunChannel(ts, tr)).thenReturn(trc);
        doNothing().when(connector).archiveChannel(trc);

        testRunService.archiveTestRunChannel(ts, tr, null, null);

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, entityCommandService, commandService);

    }

    @Test
    void unarchiveTestRun() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);

        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);

        prepareTestRun(ts, tr, null);

        Channel trc = mock(Channel.class);
        when(channelService.getTestRunChannel(ts, tr)).thenReturn(trc);
        doNothing().when(connector).unarchiveChannel(trc);

        testRunService.unarchiveTestRun(ts, tr, null, null);

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, entityCommandService, commandService);

    }

    @Test
    void sendTestRunMessage() {
        when(communicationToolConnectorLocator.getConnector()).thenReturn(connector);

        TestingSessionDTO ts = mock(TestingSessionDTO.class);
        TestRunDTO tr = mock(TestRunDTO.class);

        prepareTestRun(ts, tr, null);

        Channel trc = mock(Channel.class);
        when(channelService.getTestRunChannel(ts, tr)).thenReturn(trc);
        doNothing().when(connector).sendMessage(trc, "hello");

        testRunService.sendTestRunMessage(ts, tr, "hello");

        verifyNoMoreInteractions(channelService, communicationToolConnectorLocator, entityCommandService, commandService);

    }
}