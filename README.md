# Wildfly Local start

```bash
mvn clean install
cd communication-tool-gateway-war
mvn org.wildfly.plugins:wildfly-maven-plugin:run
```

# Quarkus Local start

```bash
mvn clean install
cd communication-tool-gateway-quarkus
mvn quarkus:dev
```
