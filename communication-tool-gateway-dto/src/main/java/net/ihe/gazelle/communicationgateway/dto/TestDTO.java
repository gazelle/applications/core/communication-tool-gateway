package net.ihe.gazelle.communicationgateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class TestDTO {
    String id;
    String keyword;
    String name;
}
