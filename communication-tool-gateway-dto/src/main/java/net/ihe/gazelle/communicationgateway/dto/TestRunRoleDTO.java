package net.ihe.gazelle.communicationgateway.dto;

public enum TestRunRoleDTO {
    PARTICIPANT,
    ADMIN
}
