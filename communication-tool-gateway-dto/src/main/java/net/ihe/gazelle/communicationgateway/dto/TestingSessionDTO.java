package net.ihe.gazelle.communicationgateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Set;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class TestingSessionDTO {
    String id;
    String keyword;
    String description;
    Set<OrganizationDTO> organizations;
    Set<TestingSessionUserDTO> users;
    Set<ProfileDTO> profiles;
}
