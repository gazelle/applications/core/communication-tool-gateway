package net.ihe.gazelle.communicationgateway.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.dto.TestRunDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class TestRunInvitationLink {
    TestingSessionDTO testingSession;
    TestRunDTO testRun;
}
