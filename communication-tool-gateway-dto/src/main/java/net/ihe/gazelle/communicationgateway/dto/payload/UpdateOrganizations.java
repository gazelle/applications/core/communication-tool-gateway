package net.ihe.gazelle.communicationgateway.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.dto.OrganizationDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;

import java.util.List;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UpdateOrganizations {
    TestingSessionDTO testingSession;
    List<OrganizationDTO> organizations;
}
