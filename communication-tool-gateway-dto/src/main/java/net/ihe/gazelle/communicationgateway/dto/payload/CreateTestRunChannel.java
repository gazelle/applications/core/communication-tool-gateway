package net.ihe.gazelle.communicationgateway.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.ihe.gazelle.communicationgateway.dto.TestRunDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;
import net.ihe.gazelle.communicationgateway.dto.UserDTO;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class CreateTestRunChannel {
    TestingSessionDTO testingSession;

    TestRunDTO testRun;

    UserDTO creator;

}
