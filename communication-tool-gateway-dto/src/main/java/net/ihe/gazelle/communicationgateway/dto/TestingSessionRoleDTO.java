package net.ihe.gazelle.communicationgateway.dto;

public enum TestingSessionRoleDTO {
    MONITOR,
    TESTING_SESSION_ADMIN
}
