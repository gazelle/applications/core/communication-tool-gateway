package net.ihe.gazelle.communicationgateway.client;

import net.ihe.gazelle.communicationgateway.dto.payload.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("testRun")
public interface TestRunClient {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    void createTestRunChannel(CreateTestRunChannel createTestRunChannel);

    @POST
    @Path("join")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    void joinTestRunChannel(JoinTestRunChannel joinTestRunChannel);

    @POST
    @Path("leave")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    void leaveTestRunChannel(LeaveTestRunChannel leaveTestRunChannel);

    @POST
    @Path("link")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    String getInvitationLink(TestRunInvitationLink testRunInvitationLink);

    @POST
    @Path("archive")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    void archiveTestRunChannel(ArchiveTestRunChannel archiveTestRunChannel);

    @POST
    @Path("message")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    void sendTestRunMessage(TestRunMessage testRunMessage);

    @POST
    @Path("unarchive")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    void unarchiveTestRun(UnarchiveTestRunChannel unarchiveTestRunChannel);
}
