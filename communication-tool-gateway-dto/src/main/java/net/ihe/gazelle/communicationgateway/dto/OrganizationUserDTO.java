package net.ihe.gazelle.communicationgateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Set;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class OrganizationUserDTO {
    UserDTO user;
    Set<OrganizationRoleDTO> roles;
}
