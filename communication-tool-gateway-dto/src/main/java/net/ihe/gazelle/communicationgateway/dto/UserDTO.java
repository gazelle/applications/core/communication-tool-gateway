package net.ihe.gazelle.communicationgateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UserDTO {
    String id;
    String username;
    String firstname;
    String lastname;
    String email;
}
