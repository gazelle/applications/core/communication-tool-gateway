package net.ihe.gazelle.communicationgateway.client;

import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;
import net.ihe.gazelle.communicationgateway.dto.payload.UpdateOrganizations;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("provisioning")
public interface ProvisioningClient {
    @POST
    @Path("testingSessions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    void updateTestingSessions(List<TestingSessionDTO> testingSessions);

    @POST
    @Path("organizations")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    void updateOrganizations(UpdateOrganizations updateOrganizations);
}
