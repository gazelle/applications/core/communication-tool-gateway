package net.ihe.gazelle.communicationgateway.dto;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

class DTOTest {

    @Test
    void beans() {
        OrganizationDTO o1 = new OrganizationDTO("a", "a", "a", null);
        OrganizationDTO o2 = new OrganizationDTO("b", "b", "b", null);
        EqualsVerifier.simple()
                .forPackage("net.ihe.gazelle.communicationgateway.dto", true)
                .withPrefabValues(OrganizationDTO.class, o1, o2)
                .verify();
    }

}
